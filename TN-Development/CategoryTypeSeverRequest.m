//
//  CategoryTypeSeverRequest.m
//  TinyNews
//
//  Created by jay kumar on 11/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CategoryTypeSeverRequest.h"
#import "QNavigatorAppDelegate.h"
#import "NSString+URLEncoding.h"

@implementation CategoryTypeSeverRequest
SEL callBackSelector;
id callBackTarget;
QNavigatorAppDelegate *l_appDelegate;

-(void)getStoriesInRange:(SEL)tempSelector tempTarget:(id)target CustomId:(NSString*)CustID  page:(int)page number:(int)num type:(NSString*)categoryType
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	
 	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=target;
	if(l_appDelegate.m_internetWorking==0)
    {
        responseData=[[NSMutableData alloc] init];
        
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
        NSString * latString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_latitude]];
		NSString * lngString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_longitude]];
		NSLog(@"NewRegistration:lat=[%@], lng=[%@]",latString, lngString);
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,CustID,[categoryType URLEncodedString],num,page];
        
        temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
        [theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		m_connection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[m_connection start];
		
		if(m_connection)
		{
			NSLog(@"Request sent to get data");
		}
        
    }
    else
	{
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
    
    
}

-(void)searchStories:(SEL)tempSelector tempTarget:(id)target seratchText:(NSString*)searchText startIndex:(int)startIndex endIndex:(int)endIndex
{
    l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	
 	[l_appDelegate CheckInternetConnection];
	callBackSelector=tempSelector;
	callBackTarget=target;
	if(l_appDelegate.m_internetWorking==0)
    {
        responseData=[[NSMutableData alloc] init];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
        temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=searchStories&searchtxt=%@&startindex=%d&endindex=%d",kServerUrl,searchText,startIndex,endIndex];
        
        temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@",temp_url);
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
        [theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		m_connection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[m_connection start];
		
		if(m_connection)
		{
			NSLog(@"Request sent to get data");
		}
        
    }
    else
	{
		[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
	}
    
    
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSLog(@"ShopbeeAPIs:didReceiveResponse");
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    m_responseCode= [httpResponse statusCode];
	NSLog(@"%d",m_responseCode);
	
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	NSLog(@"ShopbeeAPIs:didReceiveData");
	[responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSLog(@"ShopbeeAPIs:connectionDidFinishLoading");
	//[l_appDelegate.tabBarController.tabBar setUserInteractionEnabled:TRUE];
	NSString *str=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:m_responseCode] withObject:responseData];
    [str release];
	str=nil;
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	NSLog(@"ShopbeeAPIs:didFailWithError %@", [error localizedDescription]);
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[callBackTarget performSelector:callBackSelector withObject:[NSNumber numberWithInt:-1] withObject:nil];
    
}
-(void)dealloc
{
    if (responseData!=nil)
    {
        [responseData release];
        responseData=nil;
    }
    [super dealloc];
}
@end
