//
//  CategoryRequest.h
//  TinyNews
//
//  Created by Nava Carmon on 28/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryRequest : NSOperation
{
    NSDictionary *data;
}

@property (nonatomic, retain) NSDictionary *data;

- (id) initWithData:(NSDictionary *) aData;
- (void) performMain;

@end
