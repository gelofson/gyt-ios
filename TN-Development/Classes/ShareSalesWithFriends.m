//
//  ShareSalesWithFriends.m
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ShareSalesWithFriends.h"
#import <QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"QNavigatorAppDelegate.h"
#import"ViewAFriendViewController.h"
#import"CategoryModelClass.h"
#import"BigSalesModelClass.h"
#import "AuditingClass.h"
#import"AsyncImageView.h"


@implementation ShareSalesWithFriends


@synthesize m_searchBar;
@synthesize m_friendsArray;
@synthesize m_shareView;
@synthesize m_email;
@synthesize m_table;
@synthesize m_catObjModel;
@synthesize m_bigSalesModel;

QNavigatorAppDelegate *l_appDelegate;
//CategoryModelClass *l_objCatModel;




#pragma mark Searchbar Delegates 


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar 
{
 	[m_searchBar setShowsCancelButton:NO animated:YES];
	
	[m_searchBar resignFirstResponder];
	m_backup=[m_friendsArray retain];
 	
	
	[m_searchBar resignFirstResponder];
	NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"firstname CONTAINS[cd] %@",m_searchBar.text];
	NSArray *filteredNameList = [m_backup filteredArrayUsingPredicate:bPredicate];
	m_backup=[filteredNameList retain];
	[self.m_table reloadData];
	
	
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
	[m_searchBar resignFirstResponder];
	
	[m_searchBar setShowsCancelButton:NO animated:YES];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
	[m_searchBar setShowsCancelButton:YES animated:YES];
	
	return YES;
	
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	if(m_searchBar.text.length==0)
	{
		[self performSelector:@selector(load_tableViewDataSource)];
		
		[self.m_table reloadData];
		
		[m_searchBar resignFirstResponder];
	}
	[m_searchBar setShowsCancelButton:YES animated:YES];
	
}
#pragma mark -
#pragma mark custom methods

-(IBAction)m_goToBackView // for navigating to the back view
{
	[self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)FilterFriends
{
	
	for (int i=0; i<[m_friendsArray count];i++ ) 
	{
		if ([[[m_friendsArray objectAtIndex:i] valueForKey:@"requestType"] isEqualToString:@"pending"]||
		[[[m_friendsArray objectAtIndex:i] valueForKey:@"requestType"] isEqualToString:@"acceptORreject"]) 
		{
			[m_friendsArray removeObjectAtIndex:i];
		}
	}
	m_backup =[[NSMutableArray  alloc]initWithArray:m_friendsArray] ;
	
}

-(void)load_tableViewDataSource
{
	
	m_backup =[[NSMutableArray  alloc]initWithArray:m_friendsArray] ;
	
}
-(IBAction)btnTellAFriendAction:(id)sender
{
	m_email=[[m_backup objectAtIndex:[sender tag]] valueForKey:@"email"];
	[self performSelector:@selector(showPicker)];
}

-(void)showPicker
{
	// This sample can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self performSelector:@selector(displayComposerSheet)];	
		}
		else
		{
			[self performSelector:@selector(launchMailAppOnDevice)];
		}
	}
	else
	{
		[self performSelector:@selector(launchMailAppOnDevice)];
	}
}

-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
	
	NSMutableArray *toRecipients=[[NSMutableArray alloc]init];
	if (m_email) {
		
		[toRecipients addObject:m_email];
	}
	NSLog(@"values of  (to recipients) is",toRecipients);
	[picker setToRecipients:toRecipients];
	
	
	[picker setSubject:@"Share \"FashionGram\""];
	
	NSMutableString *emailBody = nil;
	if (self.m_catObjModel != nil) {
		emailBody=[NSMutableString stringWithFormat:@"%@",self.m_catObjModel.m_strBrandName];
		[emailBody appendFormat:@"\n"];
		[emailBody appendFormat:@"%@",self.m_catObjModel.m_strTitle];
		[emailBody appendFormat:@"\n"];
		[emailBody appendFormat:@"%@",self.m_catObjModel.m_strDiscount];
		[emailBody appendFormat:@"%@",self.m_catObjModel.m_strAddress];
	} else if (self.m_bigSalesModel != nil) {
		emailBody=[NSMutableString stringWithFormat:@"%@",self.m_bigSalesModel.m_strAdTitle];
		[emailBody appendFormat:@"\n"];
		[emailBody appendFormat:@"%@",self.m_bigSalesModel.m_strDescription1];
		[emailBody appendFormat:@"\n"];
		[emailBody appendFormat:@"%@",self.m_bigSalesModel.m_strDescription2];
		[emailBody appendFormat:@"\n"];
		[emailBody appendFormat:@"%@",self.m_bigSalesModel.m_strAddress];
	}
	
	UIImage *logoPic = [UIImage imageNamed:@"logo_for_share.png"];
	NSData *imageD = UIImageJPEGRepresentation(logoPic, 1);
	[picker addAttachmentData:imageD mimeType:@"image/jpg" fileName:@"logo_for_share.png"];
	

	
	[picker setMessageBody:emailBody isHTML:NO];
	
	[self presentModalViewController:picker animated:NO];
	
    [picker release];
	picker=nil;
	[toRecipients release];
	toRecipients=nil;

}	


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	AuditingClass *temp_objAudit;
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			
			//------------------------------------------------------------
			
			temp_objAudit=[AuditingClass SharedInstance];
			[temp_objAudit initializeMembers];
			NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
			[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
			
			if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
			{
				
				NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
										 @"Tell a Friend about FashionGram",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-2",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
				
				[temp_objAudit.m_arrAuditData addObject:temp_dict];
			}
			
			if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
			{
				[temp_objAudit sendRequestToSubmitAuditData];
				
			}
			
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Your message sent successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			[alert release];
			
					
			break;
		case MFMailComposeResultFailed:
			break;
		default:
			break;
	}
	
	self.navigationItem.rightBarButtonItem.enabled=NO;
	[self dismissModalViewControllerAnimated:YES];
	
}

-(void)launchMailAppOnDevice
{
	NSString *recipients = @"mailto:first@example.com?cc=second@example.com,third@example.com&subject=";
	NSString *body = @"&body=";
	
	NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
	email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


#pragma mark -
#pragma mark tableView methods
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [m_backup count];
	
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *cellidentifier=@"cell";
	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:cellidentifier] autorelease];
		cell.backgroundView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"white_bar_without_arrow.png"]];	
		cell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		UILabel *temp_lblFriendName=[[UILabel alloc]init];
		temp_lblFriendName.frame=CGRectMake(60,12,185,25); 
		temp_lblFriendName.font=[UIFont fontWithName:kFontName size:16];
		temp_lblFriendName.textColor=[UIColor colorWithRed:0.28f green:0.55f blue:0.60f alpha:1.0];
		temp_lblFriendName.tag=1001;
				
		AsyncImageView* asyncImage = [[[AsyncImageView alloc]
									   initWithFrame:CGRectMake(0,6,55,35)] autorelease];
		asyncImage.tag = 999;
		[cell.contentView addSubview:asyncImage];
		[cell.contentView addSubview:temp_lblFriendName];
		[temp_lblFriendName release];
		temp_lblFriendName=nil;
		
	}	 
	//if (![[[m_backup objectAtIndex:indexPath.row] valueForKey:@"requestType"] isEqualToString:@"pending"]&&
//		![[[m_backup objectAtIndex:indexPath.row] valueForKey:@"requestType"] isEqualToString:@"acceptORreject"]) 
//	{
		
		((UILabel*)[cell.contentView viewWithTag:1001]).text=  [NSString stringWithFormat:@"%@ %@",
							   [[m_backup objectAtIndex:indexPath.row] valueForKey:@"firstname"],
								[[m_backup objectAtIndex:indexPath.row] valueForKey:@"lastname"]];
		
		temp_btnShare=[UIButton buttonWithType:UIButtonTypeCustom];
		[temp_btnShare setImage:[UIImage imageNamed:@"share_btn.png"] forState:UIControlStateNormal];
		[temp_btnShare addTarget: self action:@selector(btnTellAFriendAction:) forControlEvents:UIControlEventTouchUpInside];
		temp_btnShare.frame=CGRectMake(230,10,40,30);
		temp_btnShare.tag=indexPath.row;
		temp_btnShare.backgroundColor = [UIColor clearColor];
		[cell.contentView addSubview:temp_btnShare];
		
		NSString *temp_strUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_backup objectAtIndex:indexPath.row] valueForKey:@"thumbImageUrl"]];
		NSURL *tempLoadingUrl = [NSURL URLWithString:temp_strUrl];
		AsyncImageView* asyncImage = (AsyncImageView*)[cell.contentView viewWithTag:999];
		//((UIImageView *)[asyncImage viewWithTag:101]).image=[UIImage imageNamed:@"loading.png"];
		[asyncImage loadImageFromURL:tempLoadingUrl];
		
	//}
	
	return cell;
	//[temp_btnShare release];
	
}


#pragma mark -

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewWillAppear:(BOOL)animated{
	
	
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
		self.m_catObjModel = nil;
		self.m_bigSalesModel = nil;
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"gray_searchbox@2x.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	
	
	[self FilterFriends];
  // [self load_tableViewDataSource];
	//m_searchBar.frame=CGRectMake(0, 87, 200,20);
//	[[m_searchBar.subviews objectAtIndex:3] setFrame:CGRectMake(0, 0, 100, 10)];
	
}

-(void)viewDidAppear:(BOOL)animated
{
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_searchBar = nil;
    //self.m_shareView = nil;
    self.m_table = nil;
}

- (void)dealloc {
    
	[m_searchBar release];
    [m_friendsArray release];
	[m_email release];
	[m_table release];
    //[share_image release];
    //[temp_imageView release];
    //[temp_btnShare release];
    [m_name release];
    [m_personName release];
    [m_arrData release];
    [m_backup release];
    
	//[m_shareView release];
    
    [super dealloc];
}

@end
