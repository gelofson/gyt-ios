//
//  WhereBox.h
//  QNavigator
//
//  Created by softprodigy on 08/03/11.Command /Developer/Applications/SDK 4.2 Beta/Platforms/iPhoneSimulator.platform/Developer/usr/bin/gcc-4.2 failed with exit code 1


//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"
#import "FBConnect.h"
#import "MGTwitterEngine.h"
#import "ShareSalesWithFriends.h"
#import "JSON.h"
#import "PhotosLoadingView.h"

#ifdef OLD_FB_SDK
#import "FBConnect/FBConnect.h"
#import "FBConnect/FBSession.h"
#endif

@class TwitterFriendsViewController;
@interface WhereBox : UIViewController <UITextFieldDelegate, SA_OAuthTwitterControllerDelegate,MFMailComposeViewControllerDelegate>{ 

	UIButton *btnBackObj;
	
	UIView	*m_moreDetailView,*m_shareView,*m_FittingRoomView;
	NSInteger tagg;
	UITableView	*m_tableView;
	UIScrollView *m_scrollView;
	
	NSURLConnection *m_mallMapConnection;
	NSURLConnection *m_theConnection;
	BOOL m_isConnectionActive;
	BOOL m_isMallMapConnectionActive;
	UIActivityIndicatorView	*m_activityIndicator;
	NSMutableData *m_mutCatResponseData;
	int m_intResponseCode;
	SA_OAuthTwitterEngine *_engine;  
	//Facebook *facebook;
	IBOutlet UIButton *m_cancel;
	IBOutlet UIView *popupView;
	MGTwitterEngine *twitterEngine; 
	TwitterFriendsViewController *tw;
	UIButton *m_emailFrnds;
	UIButton *m_facebookFrnds;
	UIButton *m_twitterFrnds;
	UIButton *m_shopBeeFrnds;
	UIButton *m_btnSendBuyOrNot;
	UIButton *m_btnSendIBoughtIt;
	UIButton *m_publicWishlist;
	UIButton *m_privateWishlist;
	UILabel *m_sharing_lable;
	UIImageView *m_imgShareView;
	UIButton *m_btnCancelWhereBox;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	NSString *m_privateOrPublic;
	NSString *m_BoughtItOrNot;
	UIImage *m_productImage;
	UIView *m_DarkGreenBoxView; 
	UIView *popUpforShareInfo; 
	UILabel *m_lblBrandName;
	BOOL ForPictureDownloading;
	BOOL isFromIndyShopView; // In case user is from the indy shop view value of this bool is true.
	PhotosLoadingView *m_ProductImageView;
	
#ifdef OLD_FB_SDK
	FBSession *localFBSession;
	FBLoginDialog* fbLoginDialog;
#endif
}

@property(nonatomic,readwrite) BOOL isFromIndyShopView;
@property (nonatomic,retain) IBOutlet UILabel *m_lblBrandName;
@property (nonatomic,retain) IBOutlet UIView *popUpforShareInfo; 
@property (nonatomic,retain) IBOutlet UIView *m_DarkGreenBoxView;
@property (nonatomic,retain) UIImage *m_productImage;
@property (nonatomic,retain) NSString *m_BoughtItOrNot;
@property (nonatomic,retain) NSString *m_privateOrPublic;
@property (nonatomic,retain) IBOutlet UIButton *m_btnCancelWhereBox;
@property (nonatomic,retain) IBOutlet UIImageView *m_imgShareView;
@property (nonatomic,retain)IBOutlet UILabel *m_sharing_lable;

@property (nonatomic,retain)IBOutlet UIButton *m_emailFrnds;
@property (nonatomic,retain)IBOutlet UIButton *m_facebookFrnds;
@property (nonatomic,retain)IBOutlet UIButton *m_twitterFrnds;
@property (nonatomic,retain)IBOutlet UIButton *m_shopBeeFrnds;

@property (nonatomic,retain)IBOutlet UIButton *m_btnSendBuyOrNot;
@property (nonatomic,retain)IBOutlet UIButton *m_btnSendIBoughtIt;

@property (nonatomic,retain)IBOutlet UIButton *m_publicWishlist;
@property (nonatomic,retain)IBOutlet UIButton *m_privateWishlist;

@property (nonatomic,retain) UIScrollView *m_scrollView;
@property (nonatomic,retain) IBOutlet UITableView *m_tableView;
@property (nonatomic,readwrite) NSInteger tagg;
@property (nonatomic,retain) UIView	*m_moreDetailView;
@property (nonatomic,retain) IBOutlet UIView *m_shareView;

@property(nonatomic,retain)IBOutlet UIButton *btnBackObj;


@property (nonatomic,retain) NSURLConnection *m_theConnection;
@property (nonatomic,retain) NSURLConnection *m_mallMapConnection;
@property BOOL m_isConnectionActive;
@property BOOL m_isMallMapConnectionActive;

@property (nonatomic,retain) IBOutlet UIActivityIndicatorView *m_activityIndicator;
@property int m_intResponseCode;
@property (nonatomic,retain) NSMutableData	*m_mutCatResponseData;
@property (nonatomic,retain) MGTwitterEngine *twitterEngine; 

@property(nonatomic,retain) TwitterFriendsViewController *tw;
@property(nonatomic,retain) IBOutlet UIView *m_FittingRoomView;


-(IBAction)back;
-(IBAction)shareBtnAction;
-(IBAction)btnTellFriendByEmailAction:(id)sender;
-(IBAction)btnCloseOnThreeButtonViewAction;
//-(IBAction)btnTellFriendByTwitterAction:(id)sender;
-(void)sendRequestToCheckMallMap:(NSString *)imageUrl;
-(void)whereBtnPressed:(int)tag;
-(IBAction)btnTellAFriendAction;
-(IBAction)ShopbeeFriends;
-(IBAction)clickButtonTellFriendsByFacebook:(id)sender;
-(IBAction) btnFindTwitterFrnd;
-(IBAction)btnFittingRoomViewAction;
-(IBAction)btnSendIBoughtItAction;
-(IBAction)btnSendBuyOrNotAction;
-(IBAction)btnWishListViewAction:(id)sender;
-(IBAction)btnTellShopBeeFriendsAction:(id)sender;
-(IBAction)btnPublicAction;
-(IBAction)btnPrivateAction;
-(IBAction)btnShareInfoAction:(id)sender;
-(IBAction)RightMovement;
-(IBAction)LeftMovement;
-(void)moveLeft;
-(void)moveRight;

- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;
//-(void)sendRequestToLoadImages:(NSString *)imageUrl;
-(void)sendRequestToLoadImages:(NSString *)imageUrl withContinueToNext:(BOOL) continueToNextFlag;

#ifdef OLD_FB_SDK
// facebook
-(void)getFacebookName;
-(void)postToFacebookWall;
#endif
@end
