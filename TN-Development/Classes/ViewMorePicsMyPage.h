//
//  ViewMorePicsMyPage.h
//  QNavigator
//
//  Created by softprodigy on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<QuartzCore/QuartzCore.h>
#import"MyPagePhotoViewController.h"

@interface ViewMorePicsMyPage : UIViewController {
	
	UILabel *m_nameLabel;
	UILabel *m_lblSale;
	UILabel *m_lblDiscount;


	UIPageControl *m_pageControl;
	
	UIView *m_photoView;
    UIScrollView *m_photoscrollView;
	
//	MyPagePhotoViewController *m_obj;
	
	NSArray *m_pgcntrlphotos;
    NSArray *m_headlineArray;
	NSInteger m_value;
	
	NSString *m_msgTypeBuzzCase;
	
	int pageNO;
	
	NSMutableData *m_mutResponseData;
	
	int m_intTypeOfResponse;
	
	BOOL m_isConnectionFailed;
	
	UIButton *m_leftButton;
	
	UIButton *m_rightButton;
	IBOutlet UILabel *m_headLineLBL;
    IBOutlet UITextView  *whoWhatTextView;
    IBOutlet UITextView *Whowhattitletext;
}
@property(nonatomic,retain) IBOutlet UITextView  *whoWhatTextView;
@property(nonatomic,retain) IBOutlet UITextView *Whowhattitletext;
@property(nonatomic,retain) NSString *m_msgTypeBuzzCase;

@property(nonatomic,retain) IBOutlet UILabel *m_lblSale;

@property(nonatomic,retain) IBOutlet UILabel *m_lblDiscount;

@property(nonatomic,retain) IBOutlet UILabel *m_nameLabel;

@property(nonatomic,retain) IBOutlet UIPageControl *m_pageControl;

@property(nonatomic,retain) IBOutlet UIView *m_photoView;
@property(nonatomic,retain) IBOutlet UIScrollView *m_photoscrollView;

@property(nonatomic,retain)  NSArray *m_pgcntrlphotos;
@property(nonatomic,retain)NSArray *m_headlineArray;
@property(nonatomic,readwrite) NSInteger m_value;

@property(nonatomic,retain) IBOutlet UIButton *m_leftButton;

@property (nonatomic,retain) IBOutlet UIButton *m_rightButton;

@property(nonatomic,retain)IBOutlet UILabel *m_headLineLBL;
-(IBAction) m_goToBackView;

- (IBAction) changePage:(id)sender;

-(IBAction) m_leftButtonAction;

-(IBAction) m_rightButtonAction;

-(void)sendRequestToLoadImages:(NSString *)imageUrl;

-(void)leftButtonAction;
-(void)rightButtonAction;
-(void)changepagecontrolImage;

- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict;
-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle;
-(NSString*)dateReFormated:(NSString*)date;
@end
