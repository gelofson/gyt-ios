//
//  MessageBoardManager.m
//  TinyNews
//
//  Created by Nava Carmon on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageBoardManager.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"
#import "JSON.h"
#import "NewsDataManager.h"
@implementation MessageBoardManager

static MessageBoardManager * manager = nil;

- (id) init 
{
	self = [super init];
	return self;
}


+ (MessageBoardManager *) sharedManager 
{
    @synchronized(self) 
	{
        if (manager == nil) 
		{
            manager = [[self alloc] init]; // assignment not done here
		}
    }
    return manager;
}


 

// sending a buzz request to Q category to create a message board
- (void) sendBuzzRequest
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
	NSData *m_personData=UIImageJPEGRepresentation([UIImage imageNamed:@"no-image"], 0.5);
    
	
	NSUInteger len = [m_personData length];
	const char *raw=[m_personData bytes];
	
	//creates the byte array from image
	NSMutableArray *tempByteArray=[[[NSMutableArray alloc]init] autorelease];
	for (long int i = 0; i < len; i++)
	{
		[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
	} 
	
	
    NSDictionary *dict=[NSDictionary dictionaryWithObjectsAndKeys:customerID,@"custid", @"",@"comments",@"Q",@"type",@"",@"advtlocid",[tempByteArray JSONFragment],@"photo",@"dummy", @"headline", nil];
    //	NSLog(@"%@",dict);
	
    [tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=sendBuzzRequest",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		NSString *temp_strJson=[NSString stringWithFormat:@"buzz=%@",[dict JSONRepresentation]];
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
        
        
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
            [self onGetBuzzResponse:response data:data error:jsonError];
        }];
    }
}

- (void) onGetBuzzResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([self showError:error]) {
        return;
    }
    
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int m_intResponseCode = [httpResponse statusCode];
    if(m_intResponseCode==200)//rquest from buzz use case
	{
        NSLog(@"Sending request to Q category was successful");
        [self getMessageBoardId:nil];
	}
	
}

//http://66.28.216.132/salebynow/json.htm?action=getMyRequestSummaries&custid=99&type=IBoughtIt&num=2&pagenum=1
- (void) getMessageBoardId:(id) delegate
{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *customerID= [prefs objectForKey:@"userName"];
    [self getMessageBoardId:delegate custID:customerID];
}


- (void) getMessageBoardId:(id) delegate custID:(NSString *) custId
{
    NSMutableString *temp_url;
    
    temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getMyRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,custId,@"Q",1,1];
    //    temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllRequestSummaries&custid=%@&type=%@&num=%d&pagenum=%d",kServerUrl,customerID,@"Q",3,1];
    temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",temp_url);
    
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
    
    NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
    
    //[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
    [theRequest setAllHTTPHeaderFields:headerFieldsDict];
    [theRequest setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
        NSString *messageID = [self onGetMessageResponse:response data:data error:jsonError];
        if (delegate) {
            [delegate setMessageId:messageID];
        } else {
            NSString *myMessageID = [[NSUserDefaults standardUserDefaults] objectForKey:@"MessageBoardID"];
            if (!myMessageID || (myMessageID && ![myMessageID isEqualToString:messageID])) {
                [[NSUserDefaults standardUserDefaults] setValue:messageID forKey:@"MessageBoardID"];
            }
        }
    }];
}

- (NSString *) onGetMessageResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([self showError:error]) {
        return nil;
    }
    
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int m_intResponseCode = [httpResponse statusCode];
    if(m_intResponseCode==200)//rquest from buzz use case
	{
        NSString *tempString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];	
        NSArray *responseArray = [tempString JSONValue];
        
        NSArray *fittingRoomSummary = [[responseArray objectAtIndex:0] valueForKey:@"fittingRoomSummaryList"];
        
        NSString *messageID=[[fittingRoomSummary objectAtIndex:0] valueForKey:@"messageId"];
        [tempString release];
        NSLog(@"=============response from Q %@ %@", responseArray, messageID);
        
        return messageID;
	}
	return nil;
}

- (NSArray *) onGetCommentsBoard:(NSURLResponse *)response data:(NSData *)data error:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([self showError:error]) {
        return nil;
    }
	
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int m_intResponseCode = [httpResponse statusCode];
    
    NSArray *responseArray = nil;
    
    if(m_intResponseCode==200) {
        NSString *tempString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];	
        responseArray = [tempString JSONValue];
        
        [tempString release];
    }
    return  responseArray;
}

- (void) getComments:(id) delegate
{
    NSUserDefaults *prefs2=[NSUserDefaults standardUserDefaults];
    NSString *tmp_String=[prefs2 stringForKey:@"userName"];
    NSString *messageId = [prefs2 stringForKey:@"MessageBoardID"];
    
    [self getComments:delegate custID:tmp_String messageID:messageId];
}
    
- (void) getComments:(id) delegate custID:(NSString *)customerId messageID:(NSString *)messageId
{
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		//[tnApplication.tabBarController.tabBar setUserInteractionEnabled:FALSE];

		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getFittingRoomRequest&custid=%@&msgid=%d",kServerUrl,customerId,[messageId intValue]];

		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
        
        [NSURLConnection sendAsynchronousRequest:theRequest queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
            [delegate performSelector:@selector(setCommentsArray:) withObject:[self onGetCommentsBoard:response data:data error:jsonError]];
        }];
	}
}

- (BOOL) showError:(NSError *)error
{
    if (error) {
        [[NewsDataManager sharedManager] showErrorByCode:6 fromSource:NSStringFromClass([self class])];
        return YES;
    }
    
    return NO;
}

@end
