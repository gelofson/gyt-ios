//
//  BuzzButton UseCase.h
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuzzButtonUseCase : UIViewController {
	int  m_intBackType;
	UIButton *m_btnBack;
}

@property (nonatomic,retain) IBOutlet UIButton *m_btnBack;
@property int m_intBackType;
@property(nonatomic,retain)IBOutlet UITableView *tab;

-(IBAction)m_goToBackView;
- (IBAction)actionCheckThisOut:(id)sender;
- (IBAction)actionBuyItOrNot:(id)sender;
- (IBAction)actionFoundASale:(id)sender;
- (IBAction)actionHowDoesThisLook:(id)sender;
- (IBAction)actionIBoughtThis:(id)sender;

@end
