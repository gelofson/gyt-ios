//
//  LoadingIndicatorView.m
//  QNavigator
//
//  Created by softprodigy on 28/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LoadingIndicatorView.h"


@implementation LoadingIndicatorView
#pragma mark -
#pragma mark Custom methods
-(void)startLoadingView:(id)target
{

	self.frame=[[UIScreen mainScreen] bounds];
	self.backgroundColor=[UIColor colorWithWhite:0 alpha:0.55];
	if (![self viewWithTag:10])
	{
		UIActivityIndicatorView *tmp_activityIndicator=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		tmp_activityIndicator.frame=CGRectMake(140, 230, 30, 30);
		tmp_activityIndicator.tag=10;
		[self addSubview:tmp_activityIndicator];
		[tmp_activityIndicator setHidesWhenStopped:YES];
		[tmp_activityIndicator startAnimating];
		[tmp_activityIndicator release];
		tmp_activityIndicator=nil;
		[[target view] addSubview:self];
	}
	else {
		NSLog(@"Loading view already exists");
	}
	
}


-(void)stopLoadingView
{
	[(UIActivityIndicatorView *)[self viewWithTag:10] stopAnimating];
	[[self viewWithTag:10] removeFromSuperview];
	[self removeFromSuperview];
	
}


#pragma mark Singleton Instance funtion

+ (id) SharedInstance 
{
	static id sharedManager = nil;
	
    if (sharedManager == nil) {
        sharedManager = [[self alloc] init];
    }
	
    return sharedManager;
}

- (void)dealloc {
    [super dealloc];
}


@end
