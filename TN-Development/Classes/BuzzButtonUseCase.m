//
//  BuzzButton UseCase.m
//  QNavigator
//
//  Created by softprodigy on 04/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BuzzButtonUseCase.h"
#import "BuzzButtonCameraFunctionality.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

QNavigatorAppDelegate *l_appDelegate;

@interface BuzzButtonUseCase (PRIVATE)
- (void)pushBuzzScreenWithText:(NSString*)text;
@end

@implementation BuzzButtonUseCase

@synthesize m_btnBack;
@synthesize m_intBackType;
@synthesize tab;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
	
	if(m_intBackType==0)
		[m_btnBack setImage:[UIImage imageNamed:@"myPage_back.png"] forState:UIControlStateNormal];
	else if(m_intBackType==1)
		[m_btnBack setImage:[UIImage imageNamed:@"fitting_room_back button.png"] forState:UIControlStateNormal];
	else if(m_intBackType==2)
		[m_btnBack setImage:[UIImage imageNamed:@"main_back.png"] forState:UIControlStateNormal];
	//[self performSelector:@selector(btnBoughtItAction:)];
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}	
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
 
-(IBAction)m_goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionCheckThisOut:(id)sender {
    [self pushBuzzScreenWithText:kCheckThisOut]; //3rd button
}

- (IBAction)actionBuyItOrNot:(id)sender {
    [self pushBuzzScreenWithText:kBuyItOrNot]; //1st button
}

- (IBAction)actionFoundASale:(id)sender {
    [self pushBuzzScreenWithText:kFoundASale]; //5th button
}

- (IBAction)actionHowDoesThisLook:(id)sender {
    [self pushBuzzScreenWithText:kHowDoesThisLook]; //4th button
}

- (IBAction)actionIBoughtThis:(id)sender {
    [self pushBuzzScreenWithText:kIBoughtIt]; //2nd button
}

- (void)pushBuzzScreenWithText:(NSString*)text{
    BuzzButtonCameraFunctionality *BuzzCamera_Obj1=[[BuzzButtonCameraFunctionality alloc] init];
	[BuzzCamera_Obj1 setHidesBottomBarWhenPushed:YES];
	BuzzCamera_Obj1.m_strType=text;
	[self.navigationController pushViewController:BuzzCamera_Obj1 animated:YES];
	[BuzzCamera_Obj1 release];
	BuzzCamera_Obj1=nil;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_btnBack = nil;
    self.tab = nil;
}

- (void)dealloc {
    [m_btnBack release];
    [tab release];
    
    [super dealloc];
}

@end
