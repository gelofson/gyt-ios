//
//  AddFriendsMypageViewController.h
//  QNavigator
//
//  Created by softprodigy on 22/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AddAddressBookFriends.h"
#import "ContentViewController.h"


@interface AddFriendsMypageViewController : ContentViewController <UISearchBarDelegate,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource>{
    BOOL isSearchBarEditing;
	UIButton	*m_btnTellAFriend;
    UISearchBar *searchbar;
    UITableView *tblView;
    UIActivityIndicatorView *activity;
    
	AddAddressBookFriends *m_addFriend;
	NSArray *m_arrUserInfo;
	UIScrollView *m_scrollView;
	
	IBOutlet UIButton *btnBack;
    
    NSMutableData *m_mutResponseData;
    NSMutableArray *arrImage;
    NSMutableArray *arrData;
    int responseResult;
    BOOL optionClick;
    BOOL isSearching;
    
    NSInteger startIndex;
    NSInteger endIndex;
    
    UIButton *m_exceptionPageYesButton;//**
    UIButton *m_exceptionPageNoButton;//**
    UIImageView *m_exceptionPage;//**
    
    IBOutlet UIImageView *feedBackPage;//visible when no search records found;//**
 
}
@property(retain,nonatomic)IBOutlet UIImageView *feedBackPage;//**
@property(nonatomic,retain)IBOutlet UIButton *m_btnTellAFriend;
@property(nonatomic,retain)IBOutlet UISearchBar *searchbar;
@property(nonatomic,retain)IBOutlet UITableView *tblView;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activity;


@property(nonatomic,retain)	NSArray *m_arrUserInfo;
@property (nonatomic,retain) UIScrollView *m_scrollView;

-(IBAction)btnTellAFriendAction:(id)sender;
-(IBAction)m_addAdrressBookFriend;
-(IBAction) m_goToBackView;	
-(IBAction) btnFindTwitterFrnd:(id)sender;
-(IBAction)addFacebookfriends;
-(IBAction)btnTellFriendByYahooAction:(id)sender;


-(void)gotAllFriendsFromTwitter:(NSArray *)tempFriendsArray;

-(void)setBackButton;
-(void)loadSearchData;
-(void)sendSearchRequest;


-(void)facebookYesButtonClicked;//**
-(void)twitterYesButtonClicked;//**
-(void)exceptionPageNoButtonPressed;//**

- (void)enableCancelButton:(UISearchBar *)aSearchBar;
@end
