//
//  BuyItCommentViewController.h
//  QNavigator
//
//  Created by softprodigy on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BuyItCommentViewController : UIViewController<UITextViewDelegate,UIAlertViewDelegate> {
	UITextView *m_textView;

	UIImageView *doneBackground;
	
	UIButton *doneButton;
	
	NSArray *m_MainArray;
	
	UILabel *m_PersonName;
	
	UILabel *m_Subject;
		
	NSString *m_UserID;
	
	NSMutableDictionary *m_Dictionary;
	
    // GDL: Not retained, so do not release on dealloc.
    // GDL: This is retained by m_MainArray.
	NSString *m_Type;
	
	NSInteger m_messageId;
	
	NSString *m_FavourString;
	
	UIViewController *m_CallBackViewController;
	UIImage *m_personImage;
//	
//	UILabel *m_Label1;
//	
//	UILabel *m_Label2;
	UIImageView* senderImage;
}

@property(nonatomic,retain) IBOutlet UITextView *m_textView;

@property(nonatomic,retain) IBOutlet	UILabel *m_PersonName;

@property(nonatomic,retain) IBOutlet	UILabel *m_Subject;

@property(nonatomic,retain) IBOutlet	NSArray *m_MainArray;

@property(nonatomic,retain) IBOutlet UILabel *m_HeaderLabel;

@property(nonatomic,retain) NSMutableDictionary *m_Dictionary;

@property(nonatomic,retain) UIImage *m_personImage;
@property(nonatomic,retain) NSString *m_FavourString;

@property(nonatomic,retain) UIViewController *m_CallBackViewController;

-(IBAction) goToBackView;
-(IBAction) BtnSendAction;
-(void)animateViewUpward;
-(void)animateViewDownward;
-(void)addTopBarOnKeyboard;
-(void)doneClick;
-(IBAction)BtnCancelAction;
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles;
@end
