//
//  NewsMoreViewController.m
//  TinyNews
//
//  Created by Nava Carmon on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewsMoreViewController.h"
#import "Cell.h"
#import "NewsDataManager.h"
#import "CategoryData.h"
#import "LoadingIndicatorView.h"
#import "QNavigatorAppDelegate.h"
#import "AsyncButtonView.h"
#import "FittingRoomMoreViewController.h"
#import "Constants.h"
#import "DetailPageViewController.h"
#import "VerticalScrollViewController.h"

@interface NewsMoreViewController ()

- (void) showController:(UIViewController *) aController;

@end

@implementation NewsMoreViewController

@synthesize table;
@synthesize m_data;
@synthesize filter, curPage;
@synthesize m_CallBackViewController;
@synthesize m_Type, m_headerLabel, detailController, verticalScrollController;
@synthesize fetchingData, moreButton, footerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.moreButton = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.footerView = nil;
    self.table.tableFooterView = [self gridFooterView];
    self.m_headerLabel.font = [UIFont boldSystemFontOfSize:20.];
    self.m_headerLabel.textColor = [UIColor whiteColor];
    self.m_headerLabel.textAlignment  = UITextAlignmentCenter;
}

- (void) viewWillAppear:(BOOL)animated
{
    NSString *titleStr = self.m_Type;
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:titleStr];
    
    if (convertedName)
        self.m_headerLabel.text = convertedName;
    else {
        self.m_headerLabel.text = self.m_Type;
    }
    if (fetchingData) {
        [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    }
    [self enableMore];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) updateData
{
    CategoryData *data = [[NewsDataManager sharedManager] getCategoryDataByType:self.m_Type];
    if (data) {
        self.m_data = data;
        numPages = data.numPages;
        totalRecords = data.totalRecords;
        if (fetchingData) {
            [[LoadingIndicatorView SharedInstance] stopLoadingView];
            fetchingData = NO;
            [table reloadData];
            [self enableMore];
        } 
    }
}

#pragma UIGridViewDelegate
- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 106;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 106;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 3;
}


- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid
{
	return [m_data.categoryData count];
}

- (UIView *) gridFooterView
{
    if (!self.footerView) {
        self.footerView = [[[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 30)] autorelease];
        
        UIButton *moreRecords=[[UIButton alloc] initWithFrame:CGRectMake(170, 0, 150, 30)];
        [moreRecords setTitle:@"More Records" forState:UIControlStateNormal];
        [moreRecords.titleLabel setFont:[UIFont fontWithName:kFontName size:14]];
        [moreRecords    setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [moreRecords    setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [moreRecords    setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
        [moreRecords addTarget:self action:@selector(getMoreRecords:) forControlEvents:UIControlEventTouchUpInside];
        [self.footerView addSubview:moreRecords];
        self.moreButton = moreRecords;
        [moreRecords release];
    }
    return  self.footerView;
}


- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
	Cell *cell = (Cell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[[Cell alloc] init] autorelease];
	}
	
    NSInteger indexInArray = rowIndex * 3 + columnIndex;
	NSDictionary *aData = [self.m_data messageData:indexInArray];
    
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"productImageUrl"]];
    
	//Bharat: 11/23/11: US44 - Check if product info exists, if yes, show the image2
	id wsProductDictionary = [aData valueForKey:@"wsProduct"];
    
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) && 
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}	
	}
    
    NSDictionary *temp_text_dict= [aData valueForKey:@"wsBuzz"];
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        cell.headline.text = [temp_text_dict objectForKey:@"headline"];
    } else
        cell.headline.text = @"No headline";
    NSInteger messageId = [[aData valueForKey:@"messageId"] intValue];
    cell.thumbnail.messageTag = messageId;
    cell.thumbnail.cropImage = YES;
    cell.thumbnail.maskImage = YES;
    
//    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:[aData valueForKey:@"senderName"]];
    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked1:) btnText:[aData valueForKey:@"senderName"]];
    
	return cell;
}

- (void) ProductBtnClicked:(id)sender
{
    self.detailController = [[[DetailPageViewController alloc] init] autorelease];
    self.detailController.m_data = self.m_data;
    self.detailController.m_Type = self.m_data.type;
    self.detailController.m_messageId = ((UIButton *) sender).tag;
    self.detailController.filter = filter;
    self.detailController.fetchingData = NO;
    self.detailController.mineTrack = (filter == MINE);
    self.detailController.curPage = curPage;
    self.detailController.m_CallBackViewController = self;
    [self showController:self.detailController];

}

- (void) ProductBtnClicked1:(id)sender
{
    self.verticalScrollController = [[[VerticalScrollViewController alloc] init] autorelease];
    self.verticalScrollController.m_data = self.m_data;
    self.verticalScrollController.m_Type = self.m_data.type;
//    self.verticalScrollController.m_messageId = ((UIButton *) sender).tag;
    self.verticalScrollController.firstPictureID = ((UIButton *) sender).tag;
    self.verticalScrollController.fetchingData = NO;
    self.verticalScrollController.curPage = curPage;
    self.verticalScrollController.m_CallBackViewController = self;
    [self showController:self.verticalScrollController];
    
}

- (void) updateCurrentPage:(NSNumber *)aPage
{
    curPage = [aPage intValue];
    [self.table reloadData];
}

- (void) enableMore
{
    BOOL enableMore = curPage < m_data.numPages;
    self.moreButton.hidden = !enableMore;
}

- (void) showController:(UIViewController *) aController
{
	[self.navigationController pushViewController:aController animated:YES];
}

- (void) gridView:(UIGridView *)grid didSelectRowAt:(int)rowIndex AndColumnAt:(int)colIndex
{
	NSLog(@"%d, %d clicked", rowIndex, colIndex);
}

- (IBAction)getMoreRecords:(id)sender
{
    if (curPage + 1 <= numPages) {
        [[LoadingIndicatorView SharedInstance] startLoadingView:self];
        self.curPage++;
        [[NewsDataManager sharedManager] getOneCategoryData:self type:self.m_Type filter:self.filter number:24 numPage:self.curPage addToExisting:YES];
        fetchingData = YES;
        return;
    }    
}

- (IBAction)backPressed:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void) dealloc
{
    [table release];
    [m_headerLabel release];
    [m_Type release];
    [m_CallBackViewController release];
    [m_data release];
    [detailController release];
    self.moreButton = nil;
    self.footerView = nil;
    [super dealloc];
}

@end
