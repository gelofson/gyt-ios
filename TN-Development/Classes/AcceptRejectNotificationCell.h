//
//  AcceptRejectNotificationCell.h
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/12/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationCell.h"
#import "AsyncImageView.h"

@protocol AcceptRejectNotificationDelegate;

@interface AcceptRejectNotificationCell : UITableViewCell{
    AsyncButtonView*                        m_imageView;
    UIButton*                               m_acceptButton;
    UIButton*                               m_rejectButton;
    UILabel*                                m_nameLabel;
    UILabel*                                m_wouldLikeToAddYouAsFriend;
    UIImageView*                    m_clockView;
    UILabel*                    	m_clockLabel;
    Notification*                           m_notification;
    id<AcceptRejectNotificationDelegate>    m_delegate;
}

@property (nonatomic, retain) Notification*                                 notification;
@property (nonatomic, retain) id<AcceptRejectNotificationDelegate>          delegate;
@property (nonatomic,retain) UIImageView*                  m_clockView;
@property (nonatomic,retain) UILabel*                  m_clockLabel;

@end


@protocol AcceptRejectNotificationDelegate <NSObject>

- (void)acceptInvitationFromNotification:(Notification*)notification;
- (void)denyInvitationFromNotification:(Notification*)notification;
- (void)selectedProfileWithEmail:(NSString*)email withName:(NSString*)name;

@end