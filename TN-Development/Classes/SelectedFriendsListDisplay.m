//
//  SelectedFriendsListDisplay.m
//  QNavigator
//
//  Created by softprodigy on 12/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SelectedFriendsListDisplay.h"
#import"Constants.h"
#import"AsyncImageView.h"
#import"ViewAFriendViewController.h"
#import"QNavigatorAppDelegate.h"
#import "NewCell.h"
@implementation SelectedFriendsListDisplay

@synthesize m_theTable;

@synthesize m_FriendsListArray;

@synthesize LevelTrackSelectedFriend;
@synthesize footerView;
@synthesize table;
QNavigatorAppDelegate *l_appDelegate;

#pragma mark -
#pragma mark custom
-(IBAction)m_goToBackView;    // for navigating to the back view
{
	[self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)FilterFriends
{
	m_AcceptedFriendsArray=[[NSMutableArray alloc] init];
	int Count=[m_FriendsListArray count];
	
	for (int i=0; i<Count;i++) 
	{
		NSString *RequestType=[[m_FriendsListArray objectAtIndex:i] valueForKey:@"requestType"];
		if (![RequestType isEqualToString:@"pending"]&&![RequestType isEqualToString:@"acceptORreject"]) 
		{
			[m_AcceptedFriendsArray addObject:[m_FriendsListArray objectAtIndex:i]];
		}
	}
	[m_FriendsListArray release];
	m_FriendsListArray=nil;
	
	
	
	
}


#pragma mark- UIGridViewDelegate

- (UIView *) gridFooterView
{
    if (!self.footerView) {
        self.footerView = [[[UIView alloc] initWithFrame:CGRectMake(0., 0., 320, 30)] autorelease];
        
    }
    return  self.footerView;
}



- (CGFloat) gridView:(UIGridView *)grid widthForColumnAt:(int)columnIndex
{
	return 80;
}

- (CGFloat) gridView:(UIGridView *)grid heightForRowAt:(int)rowIndex
{
	return 120;
}

- (NSInteger) numberOfColumnsOfGridView:(UIGridView *) grid
{
	return 4;
}


- (NSInteger) numberOfCellsOfGridView:(UIGridView *) grid
{
	return [m_AcceptedFriendsArray count];
}

- (UIGridViewCell *) gridView:(UIGridView *)grid cellForRowAt:(int)rowIndex AndColumnAt:(int)columnIndex
{
	NewCell *cell = (NewCell *)[grid dequeueReusableCell];
	
	if (cell == nil) {
		cell = [[[NewCell alloc] init] autorelease];
	}
	
    NSInteger indexInArray = rowIndex * 4 + columnIndex;
	
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[m_AcceptedFriendsArray   objectAtIndex:indexInArray]valueForKey:@"imageUrl"]];
    cell.headline.text= [[m_AcceptedFriendsArray objectAtIndex:indexInArray]valueForKey:@"firstname"];
    cell.thumbnail.messageTag = indexInArray+1;
    cell.thumbnail.cropImage = YES;
    cell.thumbnail.maskImage = YES;
    
    [cell.thumbnail loadImageFromURL:[NSURL URLWithString:productImageUrl] target:self action:@selector(ProductBtnClicked:) btnText:[[m_AcceptedFriendsArray objectAtIndex:indexInArray]valueForKey:@"email"]];
    
	return cell;
}

- (void) ProductBtnClicked:(id)sender
{
    int tag=((UIButton *)sender).tag-1;
        
    if (LevelTrackSelectedFriend<2)
	{
		ViewAFriendViewController *tmp_obj=[[ViewAFriendViewController alloc] init];
		tmp_obj.m_strLbl1 = [[m_AcceptedFriendsArray objectAtIndex:tag]valueForKey:@"firstname"];
		tmp_obj.m_Email=[[m_AcceptedFriendsArray objectAtIndex:tag]valueForKey:@"email"];
		tmp_obj.LevelTrackFriendsView=LevelTrackSelectedFriend;
		[	tmp_obj.m_strLbl1 retain];
		if ([[[m_AcceptedFriendsArray objectAtIndex:tag] valueForKey:@"requestType"] isEqualToString:@"pending"]&&[[[m_AcceptedFriendsArray objectAtIndex:tag] valueForKey:@"requestType"] isEqualToString:@"acceptORreject"])
		{
			tmp_obj.isFriendRequestPending=YES;
		}
		else
		{
			tmp_obj.isFriendRequestPending=NO;
		}
		
        tmp_obj.messageLayout = NO;
		[self.navigationController pushViewController:tmp_obj animated:YES];
		[tmp_obj release];
		tmp_obj=nil;
	}

    
    
    
    
}

#pragma mark -

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	[self FilterFriends];
	
}
-(void)viewWillAppear:(BOOL)animated
{
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_theTable = nil;
    
    // I don't think this was supposed to be an IBOutlet
    //self.m_FriendsListArray = nil;
    
}

- (void)dealloc {
	[m_theTable release];
	[m_FriendsListArray release];
    [m_AcceptedFriendsArray release];
    [footerView release];
    [table release];
    [super dealloc];
}

@end
