//
//  FittingRoomBoughtItViewController.h
//  QNavigator
//
//  Created by softprodigy on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FittingRoomBoughtItViewController : UIViewController<UIAlertViewDelegate,UIActionSheetDelegate,UITextViewDelegate> {
	IBOutlet UIScrollView *m_scrollViewMoreInfo;
	UIButton *m_LikeBtn;
	UIButton *m_WishListBtn;
	UIButton *m_IboughtItBtn;
	UIButton *m_ChangeInMindBtn;
	NSArray *m_mainArray;
	NSMutableDictionary *m_MainDictionary;
	NSInteger m_messageId;
	NSString *m_messageType;
	UIView *m_BigPicView;
	UIImageView *m_PictureView;
	BOOL ChangeinMindBtnClicked;
	BOOL MineTrack;
	NSString *m_StrMessageId;
	NSString *UserName;
	UIViewController *m_CallbackViewController;
	UIImageView *m_backgroundImageView;
	UIButton *m_CommentButton;
	IBOutlet UILabel *m_LabelWishlist;
	UILabel *m_FollowLabel;
	UIButton *m_FollowBtn;
	NSInteger m_currentMessageID;
	NSMutableArray *m_messageIDsBoughtItArray;
	IBOutlet UITextView *m_textViewSubject;
	UILabel *m_elapsedTimelabel;
	UIImageView *m_likeImageView;
	IBOutlet UILabel *m_likeLabel;
	IBOutlet UILabel *m_Namelbl;
	//////////////
	UIButton *m_MineCommentButton;
    IBOutlet UILabel *m_lableButItOrNot;
    IBOutlet UIImageView *greyLine1;
    IBOutlet UIImageView *greyLine2;
    IBOutlet UIImageView *greyLine3;
    BOOL follow;
    
    UIButton *m_leftArrow;
	UIButton *m_rightArrow;
	
}

@property(nonatomic,retain) IBOutlet	UIButton *m_leftArrow;
@property(nonatomic,retain) IBOutlet	UIButton *m_rightArrow;
@property(nonatomic,retain) IBOutlet UIButton *m_MineCommentButton;
@property(nonatomic,retain)IBOutlet UILabel *m_lableButItOrNot;
@property(nonatomic,retain)NSString *UserName;
@property(nonatomic,retain)NSString *m_StrMessageId;
@property(nonatomic,retain) IBOutlet	UIButton *m_LikeBtn;
@property(nonatomic,retain) IBOutlet   	UIButton *m_WishListBtn; 
@property(nonatomic,retain) IBOutlet   	UIButton *m_IboughtItBtn;
@property(nonatomic,retain) IBOutlet   	UIButton *m_ChangeInMindBtn;
@property(nonatomic,retain) IBOutlet    NSArray *m_mainArray;
@property(nonatomic,retain) IBOutlet    UIView *m_BigPicView;
@property(nonatomic,retain) IBOutlet    UIImageView *m_PictureView;
@property(nonatomic,readwrite) BOOL MineTrack;
@property(nonatomic,retain) UIViewController *m_CallbackViewController;
@property(nonatomic,retain) IBOutlet UIImageView *m_backgroundImageView;
@property(nonatomic,retain) IBOutlet UIButton *m_CommentButton;
@property(nonatomic,retain) IBOutlet UILabel *m_FollowLabel;
@property(nonatomic,retain) IBOutlet UIButton *m_FollowBtn;
@property(nonatomic,retain) NSMutableArray *m_messageIDsBoughtItArray;
@property(nonatomic,readwrite) NSInteger m_currentMessageID;

@property(nonatomic, retain) IBOutlet UILabel    *headerLabel;
@property(nonatomic, retain) IBOutlet UITextView *whoWhatTextView;
@property(nonatomic,retain)IBOutlet UITextView *whoWhattitleTextView;
@property(nonatomic, assign) IBOutlet UILabel *m_likeLabel;
@property (nonatomic,retain) IBOutlet UIScrollView *m_scrollViewMoreInfo;
@property (nonatomic,retain) IBOutlet UILabel *m_Namelbl;

-(IBAction) goToBackView;
-(IBAction) BtnLikeAction;
-(IBAction) btnFlagItAction;

-(IBAction) btnFollowAction:(id)sender;
-(IBAction)btnUnfollowAction:(id)sender;
-(IBAction)btnWishListAction;
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles;
-(void)addToWishListPrivate:(id)sender;
-(void)addToWishListPublic:(id)sender;
-(IBAction)CloseBtnAction;
- (void)bounce2AnimationStopped;
- (void)bounce1AnimationStopped;
-(void)initialDelayEnded;
-(IBAction)ProductBtnClicked:(id)sender;
-(IBAction)ChangeinMindAction;
-(IBAction)CommentBtnAction;
-(IBAction)FollowOrUnfollowAction;
-(IBAction)FindtheCurrentIndexInTheArray;
-(IBAction)LeftMovement:(id)sender;
-(IBAction)RightMovement:(id)sender;
- (NSString *) formatWhoWhatText:(NSDictionary *)dataDict;
-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle;
-(NSString*)dateReFormated:(NSString*)date;
-(IBAction)UpdateScrollViewContent;
- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;
-(IBAction)CommentBtnAction;
- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;

-(void) showHideArrowButton;

@end
