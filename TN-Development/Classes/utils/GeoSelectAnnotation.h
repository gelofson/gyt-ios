
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKReverseGeocoder.h>

@interface GeoSelectAnnotation : NSObject<MKAnnotation, MKReverseGeocoderDelegate> {
	CLLocationCoordinate2D coordinate;
	
	MKReverseGeocoder *geoCoder;
	MKPlacemark *placeMark;
	
}

@property (nonatomic, retain) MKReverseGeocoder * geoCoder;
@property (nonatomic, retain) MKPlacemark * placeMark;

-(NSString *) getFormatedAddressSubtitle;
-(NSString *) getFormatedAddressTitle;

@end

