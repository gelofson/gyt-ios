//
//  ImageCacheManager.h
//  ooVoo
//
//  Created by Nava Carmon on 12/21/10.
//  Copyright 2010 ooVoo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ImageCacheManager : NSObject {
    NSMutableDictionary *imagesCache;
}

+ (ImageCacheManager*)sharedManager;

- (UIImage *) getImage:(NSString *)fileName;
- (void) removeFromCache:(NSString *) keyName;
- (void) savePicture:(NSData *) imageData forName:(NSString *)name;
- (void) cleanCache;

@end
