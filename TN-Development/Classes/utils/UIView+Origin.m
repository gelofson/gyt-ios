//
//  UIView+Origin.m
//  TinyNews
//
//  Created by Nava Carmon on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIView+Origin.h"

@implementation UIView (Origin)

- (void) changeViewYOriginTo:(CGFloat)newY
{
    CGRect newRect = self.frame;
    newRect.origin.y = newY;
    self.frame = newRect;
}


@end
