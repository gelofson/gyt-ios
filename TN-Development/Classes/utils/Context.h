//
//  Context.h
//  QNavigator
//
//  Created by Bharat Biswal on 11/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define APP_CONFIG_FILENAME @"fashiongram_settings_x67w8.plist"

#define USER_SELECTED_TEXT_FONT_SIZE_KEY @"userSelectedTextFontSizeKey"

#define APP_PREFIX_KEY @"appPrefixKey"

@interface Context : NSObject 
{
	NSMutableDictionary * m_settings;
    NSMutableArray *categories;
	
}
@property (nonatomic, retain) NSMutableDictionary * m_settings;
@property (nonatomic, retain) NSMutableArray *categories;

/** 
 * Returns Singleton instance of this context class
 * @return singleton instance
 */
+ (Context *) getInstance;
+ (NSString *) GetDocumentDirectoryPath;
+ (NSString *) GetResourceDirectoryPath;
+(bool) FileExistsAtPath:(NSString*) path;

- (NSString *) getValue:(NSString *) keyId;
-(void) setValue:(NSString *) val forKey:(NSString *) keyId;
- (NSString *) getAppVersion;
- (NSString *) getFontTypeForKey:(NSUInteger) fontTypeKey;
- (CGFloat) getFontSizeForKey:(NSUInteger) fontSizeKey;

- (BOOL) getFormatedGrouponAdRdeemTimingMessageWithStart:(NSString *) redeemStart withEnd:(NSString *) redeemEnd saveTo:(NSMutableArray *) msgArray;
- (NSString *) getDisplayTextFromMessageType:(NSString *) strType;


@end
