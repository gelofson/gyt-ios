//
//  VerticalScrollViewController.m
//  TinyNews
//
//  Created by Nava Carmon on 5/27/13.
//
//

#import "VerticalScrollViewController.h"
#import "QNavigatorAppDelegate.h"
#import "UIViewController+NibCells.h"
#import "Constants.h"
#import "ViewAFriendViewController.h"
#import "MoreButton.h"
#import "CategoryData.h"

#define IMAGE_TAG           10
#define HEADER_TAG          11
#define ORIGINATOR_PICTURE_TAG  12
#define ORIGINATOR_NAME_TAG     13
#define POSTED_TAG          14
#define LIKES_TAG           15
#define DISLIKES_TAG        16
#define COMMENTS_TAG        17
#define ARROW_TAG           18

@interface VerticalScrollViewController ()

@end

@implementation VerticalScrollViewController

@synthesize detailTableView;
@synthesize m_data;
@synthesize curPage;
@synthesize m_CallBackViewController;
@synthesize m_Type, m_headerLabel, detailController;
@synthesize fetchingData;
@synthesize firstPictureID;
@synthesize currentIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.firstPictureID = -1;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.m_headerLabel.font = [UIFont boldSystemFontOfSize:20.];
    self.m_headerLabel.textColor = [UIColor whiteColor];
    self.m_headerLabel.textAlignment  = UITextAlignmentCenter;

    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    NSString *titleStr = self.m_Type;
    NSString *convertedName = [[NewsDataManager sharedManager] getCategoryNewName:titleStr];
    
    numPages = self.m_data.numPages;
    totalRecords = self.m_data.totalRecords;

    if (convertedName)
        self.m_headerLabel.text = convertedName;
    else {
        self.m_headerLabel.text = self.m_Type;
    }
    if (fetchingData) {
        [[LoadingIndicatorView SharedInstance] startLoadingView:self];
    }
    if (self.firstPictureID < 0) {
        return;
    }
    [self.detailTableView reloadData];
    NSInteger rowToSelect = [self.m_data indexOfMessage:self.firstPictureID];
    if (rowToSelect >= 0) {
        [self selectDetailRow:[NSNumber numberWithInt:rowToSelect]];
        self.firstPictureID = -1;
    }
}

- (void)selectDetailRow:(NSNumber *)row
{
    [self.detailTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[row intValue] inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    return;
    if (self.firstPictureID < 0) {
        return;
    }
    [self.detailTableView reloadData];
    NSInteger rowToSelect = [self.m_data indexOfMessage:self.firstPictureID];
    if (rowToSelect >= 0) {
        [self selectDetailRow:[NSNumber numberWithInt:rowToSelect]];
        self.firstPictureID = -1;
    }
    
}

- (void) updateData
{
    CategoryData *data = [[NewsDataManager sharedManager] getCategoryDataByType:self.m_Type];
    if (data) {
        self.m_data = data;
        numPages = data.numPages;
        totalRecords = data.totalRecords;
        if (fetchingData) {
            [[LoadingIndicatorView SharedInstance] stopLoadingView];
            fetchingData = NO;
            [detailTableView reloadData];
        }
    }
}

- (void) showDetail:(NSInteger) indexToShow
{
    NSDictionary *aData = [self.m_data messageData:indexToShow];
    NSDictionary *tmp_dict = [aData valueForKey:@"wsMessage"];
    
    CategoryData *catData = [[[CategoryData alloc] initWithData:[NSArray arrayWithObject:aData] fromList:NO] autorelease];
    
    self.detailController = [[[DetailPageViewController alloc] init] autorelease];
    [self.detailController hideArrows];
    self.detailController.m_data = catData;
    self.detailController.m_Type = self.m_Type;
    self.detailController.m_messageId = [[tmp_dict objectForKey:@"id"] intValue];
    self.detailController.filter = POPULAR;
    self.detailController.mineTrack = NO;
    self.detailController.curPage = 1;
    
    self.detailController.m_CallBackViewController = self;
    [self.navigationController pushViewController:self.detailController animated:YES];
}

- (IBAction)showDetailController:(id)sender
{
    MoreButton *button = (MoreButton *)sender;
    NSIndexPath *path = [button.data objectForKey:@"index"];
    [self showDetail:path.row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBackAction:(id)sender
{
    if (self.m_CallBackViewController && [self.m_CallBackViewController respondsToSelector:@selector(updateCurrentPage:)]) {
        [self.m_CallBackViewController performSelector:@selector(updateCurrentPage:) withObject:[NSNumber numberWithInt:self.curPage]];
    }
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark UITableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 415.;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [m_data.categoryData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self loadReusableTableViewCellFromNibNamed:@"VSCell"];

    NSDictionary *aData = [self.m_data messageData:indexPath.row];
    
	//================== Product image ======================================
	NSString *productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"productImageUrl"]];
    
	id wsProductDictionary = [aData valueForKey:@"wsProduct"];
    
	if ((wsProductDictionary != nil) && ([wsProductDictionary isKindOfClass:[NSDictionary class]] == YES)) {
		id imageUrlsArr = [((NSDictionary *)wsProductDictionary) valueForKey:@"imageUrls"];
		if ((imageUrlsArr != nil) && ([imageUrlsArr isKindOfClass:[NSArray class]] == YES) &&
            ([((NSArray *)imageUrlsArr) count] > 1) && ([[imageUrlsArr objectAtIndex:1] length] > 1)) {
			productImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[((NSArray *)imageUrlsArr) objectAtIndex:1]];
		}
	}
    AsyncButtonView *storyImageView = (AsyncButtonView *)[cell.contentView viewWithTag:IMAGE_TAG];
    storyImageView.cropImage = YES;
    storyImageView.delegate = self;
    [storyImageView resetButton];
    
	[storyImageView loadImageFromURL:[NSURL URLWithString:productImageUrl] target:nil action:nil btnText:@"" ignoreCaching:NO];
	//================== Headline ======================================

    NSDictionary *temp_text_dict= [aData valueForKey:@"wsBuzz"];
    UILabel *headlineLabel = (UILabel *)[cell.contentView viewWithTag:HEADER_TAG];
    if ([temp_text_dict isKindOfClass:[NSDictionary class]]) {
        headlineLabel.text = [temp_text_dict objectForKey:@"headline"];
    }
	//================== Originator Picture ======================================
    /*
    NSString *Url=[NSString stringWithFormat:@"%@%@",kServerUrl,[aData valueForKey:@"originatorThumbImageUrl"]];
	NSURL *temp_loadingUrl=[NSURL URLWithString:Url];
    //Nava adopting new layout

    AsyncButtonView *originatorImageView = (AsyncButtonView *)[cell.contentView viewWithTag:ORIGINATOR_PICTURE_TAG];
    
	originatorImageView.messageTag=10001;
	[originatorImageView loadImageFromURL:temp_loadingUrl target:nil action:nil btnText:@"" ignoreCaching:NO];
    
    //set contentMode to scale aspect to fit
    originatorImageView.contentMode = UIViewContentModeScaleAspectFit;
    */
	//================== Originator Name ======================================
    
    UILabel *originatorLabel = (UILabel *)[cell.contentView viewWithTag:ORIGINATOR_NAME_TAG];
    NSString *userName =[aData valueForKey:@"originatorName"];
	
	originatorLabel.text=[NSString stringWithFormat:@"by %@",userName];
//	originatorLabel.font = [UIFont fontWithName:kMyriadProRegularFont size:13];
    
	//================== Posted Tag ======================================
    NSDictionary *temp_dictionary=[aData valueForKey:@"wsMessage"];
    UILabel *postedLabel = (UILabel *)[cell.contentView viewWithTag:POSTED_TAG];
    postedLabel.text = [temp_dictionary valueForKey:@"elapsedTime"];;
    
    //=================  Adding likes text

    UILabel *likeLabel = (UILabel *)[cell.contentView viewWithTag:LIKES_TAG];
	if ([aData valueForKey:@"likes"])
	{
        int value = [[aData valueForKey:@"likes"] intValue];
        if (value > 1 || value == 0) {
            likeLabel.text=[NSString stringWithFormat:@"%d likes",value];
        } else {
            likeLabel.text=[NSString stringWithFormat:@"%d like",value];
        }
	}
    
    likeLabel = (UILabel *)[cell.contentView viewWithTag:DISLIKES_TAG];
	if ([aData valueForKey:@"notFavourCount"])
	{
        int value = [[aData valueForKey:@"notFavourCount"] intValue];
        if (value > 1 || value == 0) {
            likeLabel.text=[NSString stringWithFormat:@"%d dislikes",value];
        } else {
            likeLabel.text=[NSString stringWithFormat:@"%d dislike",value];
        }
	}
    
    likeLabel = (UILabel *)[cell.contentView viewWithTag:COMMENTS_TAG];
	if ([aData valueForKey:@"comments"])
	{
        int value = [[aData valueForKey:@"comments"] intValue];
        if (value > 1 || value == 0) {
            likeLabel.text=[NSString stringWithFormat:@"%d comments",value];
        } else {
            likeLabel.text=[NSString stringWithFormat:@"%d comment",value];
        }
	}
    
    MoreButton *arrowButton = (MoreButton *)[cell.contentView viewWithTag:ARROW_TAG];
    arrowButton.data = [NSDictionary dictionaryWithObject:indexPath forKey:@"index"];
    
    [arrowButton  addTarget:self action:@selector(showDetailController:) forControlEvents:UIControlEventTouchUpInside];
    
    currentIndex = indexPath.row;
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [cell.contentView addGestureRecognizer:swipeLeft];
    
    // if the index > than the number of data, get the next 24 pages
    if (!fetchingData && ([m_data.categoryData count] - 1) - indexPath.row < 1) {
        // Get next page...
        NSInteger   page = indexPath.row/24 + 1;
        if (page < numPages) {
            [[NewsDataManager sharedManager] getOneCategoryData:self type:self.m_Type filter:POPULAR number:24 numPage:page + 1 addToExisting:YES];
            fetchingData = YES;
            self.curPage = page+1;
            [[LoadingIndicatorView SharedInstance] startLoadingView:self];

        }
        
    }

    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void) swipeLeft:(UISwipeGestureRecognizer *)recognizer
{
	if(recognizer.direction==UISwipeGestureRecognizerDirectionLeft) {
        [self showDetail:currentIndex];
    }
}

- (void) dealloc
{
    self.m_CallBackViewController = nil;
    self.detailTableView = nil;
    self.m_data = nil;
    self.m_headerLabel = nil;
    self.m_Type = nil;
    [super dealloc];
}
/*
-(void)ProductBtnClicked:(id)sender
{
	
    if([sender tag]==10001)
	{
        ViewAFriendViewController *tempViewAFriend=[[ViewAFriendViewController alloc]init];
        NSDictionary *aData=[self.m_data messageData:m_currentMessageID];
        tempViewAFriend.m_Email=[[aData valueForKey:@"wsMessage"] valueForKey:@"sentFromCustomer"];
        tempViewAFriend.m_strLbl1=[aData valueForKey:@"originatorName"];
        tempViewAFriend.messageLayout = NO;
        [self.navigationController pushViewController:tempViewAFriend animated:YES];
        [tempViewAFriend release];
        tempViewAFriend=nil;
        
        [[GAI sharedInstance].defaultTracker sendEventWithCategory:@"DetailPage"
                                                        withAction:@"authorImageClick"
                                                         withLabel:nil
                                                         withValue:nil];
	}
	
}
 */

@end
