//
//  ChangePictureViewController.m
//  QNavigator
//
//  Created by softprodigy on 01/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ChangePictureViewController.h"
#import"QNavigatorAppDelegate.h"


@implementation ChangePictureViewController

@synthesize m_thePictureView;
QNavigatorAppDelegate *l_appDelegate;


#pragma mark -
#pragma mark custom methods

-(IBAction)m_goToBackView{
	[self.navigationController popViewControllerAnimated:YES];
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated{
}

// GDL: Changed everything below.

#pragma mark -

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_thePictureView = nil;
}

- (void)dealloc {
    [m_thePictureView release];
    
    [super dealloc];
}

@end
