//
//  YahooFriendsViewController.h
//  QNavigator
//
//  Created by softprodigy on 08/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YOSSocial.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface YahooFriendsViewController : UIViewController<MFMailComposeViewControllerDelegate> {
	YOSSession *session;
	NSString *m_strVerificationCode;
	UITableView *m_tableView;
	NSMutableArray *m_friendsDetails;
	UITextField *m_textField;
	UIView *m_verificationView;
	NSArray *tempValueArray;
	NSMutableArray *temp_ArrCountIds;
	NSMutableArray *m_shopBeeArray;
	NSMutableArray *m_nonshopBeeArray;
	NSMutableArray *tmpArrshopbeeAndNonShopbee;
	NSString *m_string_email;
	NSMutableArray *m_ArrAllEmails;

	BOOL shouldShowVerificationView;
}
@property  BOOL shouldShowVerificationView;

@property(nonatomic,retain)NSMutableArray *m_ArrAllEmails;
@property(nonatomic,retain)NSString *m_string_email;
@property(nonatomic,retain)NSMutableArray *tmpArrshopbeeAndNonShopbee;
@property(nonatomic,retain)NSMutableArray *m_nonshopBeeArray;
@property(nonatomic,retain)NSMutableArray *m_shopBeeArray;
@property(nonatomic,retain)NSMutableArray *temp_ArrCountIds;
@property(nonatomic,retain)NSArray *tempValueArray;
@property(nonatomic,retain)IBOutlet UIView *m_verificationView;
@property(nonatomic,retain)IBOutlet UITextField *m_textField;
@property(nonatomic,retain)NSMutableArray *m_friendsDetails;
@property (nonatomic,retain) YOSSession *session;
@property (nonatomic,retain)NSString *m_strVerificationCode;
@property(nonatomic,retain)IBOutlet UITableView *m_tableView;

-(void)sendRequests;
-(void)sendRequestsForSocialProfile;
-(IBAction)btnOkAction:(id)sender;

// GDL: Why do we have this with both the capital and lower case b.
-(IBAction)btnbackAction:(id)sender;
//-(IBAction) btnBackAction:(id)sender;

-(void)btnAddAction:(id)sender;

@end
