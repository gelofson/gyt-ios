//
//  LoginHomeViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 30/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "LoginHomeViewController.h"
#import "NewRegistration.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import "LoginViewController.h"
#import "AuditingClass.h"
#import "JSON.h"
#import "UploadImageAPI.h"
#import "MessageBoardManager.h"
#import "NewsDataManager.h"
#import "NSString+MD5.h"
UploadImageAPI *l_buzzRequest;

@interface LoginHomeViewController ()

- (void)onGetRegistrationResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)jsonError;
- (void)onGetLoginResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)jsonError;
- (void)addCookies:(NSURLResponse*)response; 
- (void)sendRequestForRegistration;
- (void)getFBData;
- (void)onGetMyInfo:(NSData *)data;

@end

@implementation LoginHomeViewController

@synthesize backgroundImageView;
@synthesize loginButton;
@synthesize signupButon;
@synthesize spinner;

@synthesize isUserLoggedOut;
@synthesize firstName;
@synthesize lastName;
@synthesize fbID;
@synthesize accountStore;
@synthesize twitterAccount;
@synthesize twID;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    if (IS_WIDESCREEN) {
        [backgroundImageView setImage:[UIImage imageNamed:@"TinyNews_landing_bg-586h"]];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
	
}

#pragma mark -
-(IBAction)btnLoginAction:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"fbRegistration"] && [prefs boolForKey:@"fbRegistration"]) {
        [prefs removeObjectForKey:@"fbRegistration"];
        [prefs removeObjectForKey:@"fbID"];
        [prefs removeObjectForKey:@"userName"];
		[prefs removeObjectForKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
		[prefs removeObjectForKey:kRegistrationKey];
    }
    if ([prefs objectForKey:@"TWRegistration"] && [prefs boolForKey:@"TWRegistration"]) {
        [prefs removeObjectForKey:@"TWRegistration"];
        [prefs removeObjectForKey:@"userName"];
		[prefs removeObjectForKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
        [prefs  removeObjectForKey:@"twID"];
		[prefs removeObjectForKey:kRegistrationKey];
    }

    if ([self.presentedViewController isKindOfClass:[LoginViewController class]]) {
        [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
    }
    
    if ([prefs objectForKey:kRegistrationKey] && [prefs boolForKey:kRegistrationKey]) {
        [self sendRequestForLogin:NO];
    } else {
        LoginViewController *temp_loginViewCtrl=[[LoginViewController alloc]init];
        temp_loginViewCtrl.m_isLoggedOut=TRUE;
        temp_loginViewCtrl.m_objLoginHome = self;
        [self presentViewController:temp_loginViewCtrl animated:YES completion:nil];
        [temp_loginViewCtrl release];
        temp_loginViewCtrl=nil;
    }
}

-(IBAction)registerWithFacebook:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"kRegistrationKey"]) {
        [prefs removeObjectForKey:@"fbRegistration"];
        [prefs removeObjectForKey:@"fbID"];
        [prefs removeObjectForKey:@"userName"];
		[prefs removeObjectForKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
        [prefs  removeObjectForKey:@"twID"];
        [prefs  removeObjectForKey:@"TWRegistration"];
		[prefs removeObjectForKey:kRegistrationKey];
    }
    NSArray *permissions = [NSArray arrayWithObjects:@"user_photos",@"user_videos",@"publish_stream",@"read_stream",@"offline_access",@"user_checkins",@"friends_checkins", nil];
    
    [[tnApplication m_session] setSessionDelegate:self];
    
    if (![[tnApplication m_session] isSessionValid] || ![prefs objectForKey:@"fbRegistration"]) {
        [[tnApplication m_session] authorize:permissions];
    } else {
        self.fbID = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbID"];
        [self sendRequestForLogin];
    }
}

-(IBAction)registerWithTwitter:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"kRegistrationKey"]) {
        [prefs removeObjectForKey:@"fbRegistration"];
        [prefs removeObjectForKey:@"fbID"];
        [prefs removeObjectForKey:@"userName"];
		[prefs removeObjectForKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
        [prefs  removeObjectForKey:@"twID"];
        [prefs  removeObjectForKey:@"TWRegistration"];
		[prefs removeObjectForKey:kRegistrationKey];
    }
    
    if (self.accountStore == nil) {
            ACAccountStore *acStore = [[ACAccountStore alloc]init];
            self.accountStore  = acStore;
            [acStore release];
        }
        ACAccountType *twaccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];

        [self.accountStore requestAccessToAccountsWithType:twaccountType options:nil completion:^(BOOL granted, NSError *error) {
            if(granted) {
                NSArray *accountsArray = [self.accountStore accountsWithAccountType:twaccountType];
                if(accountsArray != nil && [accountsArray count] == 0)
                {
                    [self noTwitterAccount];
                }
                else
                {
                    ACAccount *twAccount = [accountsArray lastObject];
                    NSURL *url = [NSURL URLWithString:@"http://api.twitter.com/1/account/verify_credentials.json"];
                    SLRequest *req = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
                    
                    // Important: attach the user's Twitter ACAccount object to the request
                    req.account = twAccount;
                    
                    [req performRequestWithHandler:^(NSData *responseData,
                                                     NSHTTPURLResponse *urlResponse,
                                                     NSError *error) {
                        
                        // If there was an error making the request, display a message to the user
                        if(error != nil) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Error"
                                                                            message:@"There was an error connecting to Twitter. Please try again later."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil];
                            [alert show];
                            [alert release];
                            return;
                        }
                        
                        // Parse the JSON response
                        NSError *jsonError = nil;
                        id resp = [NSJSONSerialization JSONObjectWithData:responseData
                                                                  options:0
                                                                    error:&jsonError];
                        
                        // If there was an error decoding the JSON, display a message to the user
                        if(jsonError != nil) {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Error"
                                                                            message:@"There was an error in parsing data. Please try again later."
                                                                           delegate:self
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil];
                            [alert show];
                            [alert release];
                            return;
                        }
                        NSLog(@"the res data is %@",resp);
                        
                        NSString *idString=[[resp objectForKey:@"id"] stringValue];
                        
                        if([idString length]<12)
                        {
                            self.twID=[idString stringByAppendingString:idString];
                        }
                        else
                        {
                            self.twID=[resp objectForKey:@"id"];
                        }
                        
                        self.firstName=[resp objectForKey:@"name"];
                        self.lastName=@"lastname";
                            
                        NSLog(@"the res data is %@\n %@",resp,self.twID);
                        // Make sure to perform our operation back on the main thread
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self registrationWithTwitterAccount];
                            
                        });
                    }];
                    
                    
                }
            }else{
                NSLog(@"error getting permission %@",error.description);
               // 
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                   [self noTwitterAccount];
                    
                });
                
            }
        }];
        
}
    
-(void)noTwitterAccount
{
    UIAlertView *twAlert=[[UIAlertView alloc]initWithTitle:@"No Twitter Account"  message:@"There are no Twitter account configured. You can add or create a Twitter account in Settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [twAlert show];
    [twAlert release];        
}

-(void)registrationWithTwitterAccount
{
    [tnApplication CheckInternetConnection];
    if(tnApplication.m_internetWorking==0)//0: internet working
    {
        self.view.userInteractionEnabled=FALSE;
        [spinner startAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSMutableString *temp_url;
        temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=registerCustomer",kServerUrl];
        temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSLog(@"%@",temp_url);
        NSString *twEmail = [NSString stringWithFormat:@"%@%@",[self.twID substringWithRange:NSMakeRange(0, 12)]  , @"@test.com"];
        NSLog(@"generated TW email = %@", twEmail);
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
        NSString *tempEmail=[NSMutableString stringWithFormat:@"%@/%@.jpg",pathDoc,twEmail];
        NSLog(@"Doc Directory: %@",tempEmail);
        UIImage *image = [UIImage imageNamed:@"no-image"];
        NSData *m_personData=UIImageJPEGRepresentation(image,0.0);
            
        [m_personData writeToFile:tempEmail atomically:YES];
            
        NSUInteger len = [m_personData length];
        
        const char *raw=[m_personData bytes];
            
        //creates the byte array from image
        NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
        for (int i = 0; i < len; i++)
        {
            [tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
        }
        
        NSString * latString = [NSString stringWithFormat:@"%f", [tnApplication.m_objGetCurrentLocation m_latitude]];
        NSString * lngString = [NSString stringWithFormat:@"%f", [tnApplication.m_objGetCurrentLocation m_longitude]];
        NSLog(@"NewRegistration:lat=[%@], lng=[%@]",latString, lngString);
            
        NSMutableDictionary *temp_dictRegData=[NSMutableDictionary dictionaryWithObjectsAndKeys:[tempByteArray JSONFragment] ,@"photo",self.firstName,@"firstname",lngString,@"longitude",latString,@"latitude",self.lastName,@"lastname",twEmail,@"email",@"qwe",@"password",nil];
            
        //release the temporary byte array
        [tempByteArray release];
        tempByteArray=nil;
        NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"customer=%@",[temp_dictRegData JSONRepresentation]];
        NSLog(@"JSON str:%@", temp_strJson);
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
                                                                      cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
            
        NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
            
        [theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
        [theRequest setAllHTTPHeaderFields:headerFieldsDict];
        [theRequest setHTTPMethod:@"POST"];
            
        NSURLResponse *response;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        [self onGetRegistrationResponseforTwitter:response data:data error:error];
    }
    else {
        self.view.userInteractionEnabled=TRUE;
        [spinner stopAnimating];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [netCheckAlert show];
        [netCheckAlert release];
    }
        
        
}
- (void)onGetRegistrationResponseforTwitter:(NSURLResponse *)response data:(NSData *)data error:(NSError *)jsonError
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [spinner stopAnimating];
    self.view.userInteractionEnabled=TRUE;
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int m_intResponseCode = [httpResponse statusCode];
        
    NSString *responseFromServer = [[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSLog(@"response from server: %@",responseFromServer);
    NSString *twEmail = [NSString stringWithFormat:@"%@%@", [self.twID substringWithRange:NSMakeRange(0, 12)] , @"@test.com"];
    if(m_intResponseCode==500 || m_intResponseCode==501)
    {
        UIAlertView *signupAlert=[[UIAlertView alloc]initWithTitle:@"Signup Failed!" message:@"Internal error occured. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [signupAlert show];
        [signupAlert release];
        signupAlert=nil;
    }
    else if(m_intResponseCode==412 || m_intResponseCode == 401)
    {
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
            
    }
    else if(m_intResponseCode==200)
    {
        NSLog(@"data downloaded");
        if (m_intResponseCode == 200) {
            UIAlertView *successfulRegisteredAlert=[[UIAlertView alloc]initWithTitle:@"Congratulations!!" message:@"You are registered successfully, please login with your email id and password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
           [successfulRegisteredAlert show];
           [successfulRegisteredAlert release];
           successfulRegisteredAlert=nil;
        }
        [prefs setValue:twEmail forKey:@"userName"];
        [prefs setValue:@"qwe" forKey:@"password"];
        [prefs setValue:@"" forKey:@"remember"];
        [prefs setObject:self.twID forKey:@"twID"];
        [prefs setBool:YES forKey:@"TWRegistration"];
        [self performSelector:@selector(logInWithTwitter) withObject:nil afterDelay:0.2];
    }
    else  if ([responseFromServer rangeOfString:@"Found another user with the same email"].length != 0){
        [prefs setValue:twEmail forKey:@"userName"];
        [prefs setValue:@"qwe" forKey:@"password"];
        [prefs setValue:@"" forKey:@"remember"];
        [prefs setBool:YES forKey:@"TWRegistration"];
        [self performSelector:@selector(logInWithTwitter) withObject:nil afterDelay:0.2];
    }
}


-(void)logInWithTwitter
{
    [self sendRequestForTwitterLogin:YES];
    
}
-(void)sendRequestForTwitterLogin:(BOOL) fromTW
{
    [tnApplication CheckInternetConnection];
    if(tnApplication.m_internetWorking == 0)//0: internet working
    {
        self.view.userInteractionEnabled = FALSE;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        NSMutableString *url;
        if (fromTW) {
            NSString *twEmail = [NSString stringWithFormat:@"%@%@",[self.twID substringWithRange:NSMakeRange(0, 12)], @"@test.com"];
            url = [NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=loginCustomer&email=%@&pass=%@",kServerUrl, twEmail, @"qwe"];
            } else {
            NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
            NSString *pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
            url = [NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=loginCustomer&email=%@&pass=%@",kServerUrl, email, pass];
            }
            url = (NSMutableString *)[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSLog(@"%@", url);
            
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: url]
                                                                      cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];
            NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
            
            if(![[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
            {
                NSDictionary *deviceDetails = [NSDictionary dictionaryWithObjectsAndKeys:tnApplication.m_strDeviceId,@"did", tnApplication.m_strDeviceToken,@"deviceToken",nil];
                NSString *strJson = [NSString stringWithFormat:@"devicedetails=%@",[deviceDetails JSONFragment]];
                [theRequest setHTTPBody:[strJson dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [theRequest setAllHTTPHeaderFields:headerFieldsDict];
            [theRequest setHTTPMethod:@"POST"];
            NSURLResponse *response;
            NSError *jsonError = nil;
            
            NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&jsonError];
            if (!jsonError) {
                [self onGetLoginResponse:response data:data error:jsonError];
            } else {
                NSLog(@"error on login %@", [jsonError localizedDescription]);
            }
            
        }
        else
        {
            self.view.userInteractionEnabled = TRUE;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showAlertView:kNetNotAvailableAlertTitle alertMessage:kNetNotAvailableAlertMsg];
        }
}


-(IBAction)btnSignupAction:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:@"fbRegistration"] && [prefs boolForKey:@"fbRegistration"]) {
        [prefs removeObjectForKey:@"fbRegistration"];
        [prefs removeObjectForKey:@"fbID"];
        [prefs removeObjectForKey:@"userName"];
		[prefs removeObjectForKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
		[prefs removeObjectForKey:kRegistrationKey];
    }
    if ([prefs objectForKey:@"TWRegistration"] && [prefs boolForKey:@"TWRegistration"]) {
        [prefs removeObjectForKey:@"TWRegistration"];
        [prefs removeObjectForKey:@"userName"];
		[prefs removeObjectForKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
        [prefs  removeObjectForKey:@"twID"];
		[prefs removeObjectForKey:kRegistrationKey];
    }

	NewRegistration *temp_regViewCtrl=[[NewRegistration alloc]init];
	temp_regViewCtrl.m_homeViewObj=self;
	[self presentViewController:temp_regViewCtrl animated:YES completion:nil];
	[temp_regViewCtrl release];
	temp_regViewCtrl=nil;
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    // none
}

- (void)dealloc {

	[backgroundImageView release];
	[loginButton release];
	[signupButon release];
    self.firstName = nil;
    self.lastName = nil;
    [twitterAccount release];
    [accountStore release];
    [twID release];
    [super dealloc];
}

#pragma mark FBSessionDelegate


- (void)fbDidLogin  
{	
    NSLog(@"fbDidLogin()");
    tnApplication.isLoginFacebook = true;
    [tnApplication storeOAuthData];
    [self getFBData];
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
    tnApplication.isLoginFacebook = true;
    [tnApplication removeOAuthData];
}

- (void)fbDidExtendToken:(NSString*)accessToken expiresAt:(NSDate*)expiresAt
{
    [tnApplication storeOAuthData];
}

- (void)fbSessionInvalidated {
    tnApplication.isLoginFacebook = false;
    [tnApplication removeOAuthData];
}

- (void) getFBData
{
    NSString *request = [NSString stringWithFormat:@"https://graph.facebook.com/me?access_token=%@", tnApplication.m_session.accessToken];
    NSURL   *url    = [NSURL URLWithString:request];
    NSURLRequest *requestURL = [[[NSURLRequest alloc] initWithURL:url]autorelease];
    NSURLResponse *response;
    NSError *jsonError = nil;
    
    NSData *data = [NSURLConnection sendSynchronousRequest:requestURL returningResponse:&response error:&jsonError];
    
    if (!jsonError) {
        [self onGetMyInfo:data];
    } else {
        NSLog(@"getFBData error = %@", [jsonError localizedDescription]);
        self.view.userInteractionEnabled = TRUE;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self showAlertView:kFBConnectionErrorTitle alertMessage:kFBConnectionErrorMsg];
    }
//    [NSURLConnection sendAsynchronousRequest:requestURL queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *jsonError) {
//        [self onGetMyInfo:data];
//    }];
}

- (void) onGetMyInfo:(NSData *)data
{
    NSError *jsonError = nil;
    
    NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
    
    NSDictionary *errorDict = [json objectForKey:@"error"];
    if (errorDict) {
        NSLog(@"onGetMyInfo - error in getting info");
    }
    
    NSLog(@"get my info %@", json);
    self.fbID = [NSString stringWithFormat:@"%@%@", [json objectForKey:@"id"], @"619345"];
    self.fbID = [self.fbID md5];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs setObject:self.fbID forKey:@"fbID"];
    
    self.firstName = [json objectForKey:@"first_name"];
    self.lastName = [json objectForKey:@"last_name"];

// Add getting picture from account
    [self sendRequestForRegistration];
}
///////////////////////  REGISTRATION //////////////////////////

-(void)sendRequestForRegistration
{
	[tnApplication CheckInternetConnection];
	if(tnApplication.m_internetWorking==0)//0: internet working
	{
		self.view.userInteractionEnabled=FALSE;
		[spinner startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		        
        temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=registerCustomer",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		NSString *fbEmail = [NSString stringWithFormat:@"%@%@", [self.fbID substringWithRange:NSMakeRange(0, 12)], @"@test.com"];
        
        NSLog(@"generated FB email = %@", fbEmail);
        
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
		NSString *tempEmail=[NSMutableString stringWithFormat:@"%@/%@.jpg",pathDoc,fbEmail];
		NSLog(@"Doc Directory: %@",tempEmail);
		
        UIImage *image = [UIImage imageNamed:@"no-image"];
        NSData *m_personData=UIImageJPEGRepresentation(image,0.0);
            
		[m_personData writeToFile:tempEmail atomically:YES];
		
		NSUInteger len = [m_personData length];
		
		const char *raw=[m_personData bytes];
		
		
		//creates the byte array from image
		NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
		for (int i = 0; i < len; i++)
		{
			[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
		}
		
		
		NSString * latString = [NSString stringWithFormat:@"%f", [tnApplication.m_objGetCurrentLocation m_latitude]];
		NSString * lngString = [NSString stringWithFormat:@"%f", [tnApplication.m_objGetCurrentLocation m_longitude]];
		NSLog(@"NewRegistration:lat=[%@], lng=[%@]",latString, lngString);
		
        NSMutableDictionary *temp_dictRegData=[NSMutableDictionary dictionaryWithObjectsAndKeys:[tempByteArray JSONFragment] ,@"photo",self.firstName,@"firstname",lngString,@"longitude",latString,@"latitude",self.lastName,@"lastname",fbEmail,@"email",@"qwe",@"password",nil];
        
        //release the temporary byte array
		[tempByteArray release];
		tempByteArray=nil;
		
		NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"customer=%@",[temp_dictRegData JSONRepresentation]];
        NSLog(@"JSON str:%@", temp_strJson);
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
        NSURLResponse *response;
        NSError *error = nil;
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&error];
        [self onGetRegistrationResponse:response data:data error:error];
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		[spinner stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}	
}

- (void)onGetRegistrationResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)jsonError
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[spinner stopAnimating];
	
	self.view.userInteractionEnabled=TRUE;
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int m_intResponseCode = [httpResponse statusCode];
	
    NSString *responseFromServer = [[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSLog(@"response from server: %@",responseFromServer);
    NSString *fbEmail = [NSString stringWithFormat:@"%@%@", [self.fbID substringWithRange:NSMakeRange(0, 12)], @"@test.com"];
	
	if(m_intResponseCode==500 || m_intResponseCode==501)
	{
		UIAlertView *signupAlert=[[UIAlertView alloc]initWithTitle:@"Signup Failed!" message:@"Internal error occured. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[signupAlert show];
		[signupAlert release];
		signupAlert=nil;
	}
	else if(m_intResponseCode==412 || m_intResponseCode == 401)
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
		
	}
	else if(m_intResponseCode==200)
	{
		NSLog(@"data downloaded");
		
        if (m_intResponseCode == 200) {
            UIAlertView *successfulRegisteredAlert=[[UIAlertView alloc]initWithTitle:@"Congratulations!!" message:@"You are registered successfully, please login with your email id and password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [successfulRegisteredAlert show];
            [successfulRegisteredAlert release];
            successfulRegisteredAlert=nil;
        }
		
		[prefs setValue:fbEmail forKey:@"userName"];
		[prefs setValue:@"qwe" forKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
		
		[prefs setBool:YES forKey:@"fbRegistration"];
        //[[MessageBoardManager sharedManager] sendBuzzRequest];
		[self performSelector:@selector(sendRequestForLogin) withObject:nil afterDelay:0.2];
    } else  if (m_intResponseCode == 409 || [responseFromServer rangeOfString:@"Found another user with the same email"].length != 0){
		[prefs setValue:fbEmail forKey:@"userName"];
		[prefs setValue:@"qwe" forKey:@"password"];
		[prefs setValue:@"" forKey:@"remember"];
		
		[prefs setBool:YES forKey:@"fbRegistration"];
		[self performSelector:@selector(sendRequestForLogin) withObject:nil afterDelay:0.2];
    }	
}

///////////////////////  LOGIN //////////////////////////
-(void)sendRequestForLogin
{
    [self sendRequestForLogin:YES];
}

-(void)sendRequestForLogin:(BOOL) fromFB
{
    [tnApplication CheckInternetConnection];
	
    if(tnApplication.m_internetWorking == 0)//0: internet working
	{
		self.view.userInteractionEnabled = FALSE;
		//[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        NSMutableString *url;
        if (fromFB) {
            NSString *fbEmail = [NSString stringWithFormat:@"%@%@", [self.fbID substringWithRange:NSMakeRange(0, 12)], @"@test.com"];
            url = [NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=loginCustomer&email=%@&pass=%@",kServerUrl, fbEmail, @"qwe"];
        } else {
            NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
            NSString *pass = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
            url = [NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=loginCustomer&email=%@&pass=%@",kServerUrl, email, pass];
        }
		url = (NSMutableString *)[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		NSLog(@"%@", url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		if(![[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
		{
			NSDictionary *deviceDetails = [NSDictionary dictionaryWithObjectsAndKeys:tnApplication.m_strDeviceId,@"did", tnApplication.m_strDeviceToken,@"deviceToken",nil];
			NSString *strJson = [NSString stringWithFormat:@"devicedetails=%@",[deviceDetails JSONFragment]];
			[theRequest setHTTPBody:[strJson dataUsingEncoding:NSUTF8StringEncoding]];
		}
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		NSURLResponse *response;
        NSError *jsonError = nil;
        
        NSData *data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:&jsonError];
        if (!jsonError) {
            [self onGetLoginResponse:response data:data error:jsonError];
        } else {
            NSLog(@"error on login %@", [jsonError localizedDescription]);
            self.view.userInteractionEnabled = TRUE;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            [self showAlertView:kNetworkDownErrorTitle alertMessage:kNetworkDownErrorMsg];
        }
        
	}
	else
	{
		self.view.userInteractionEnabled = TRUE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[self showAlertView:kNetNotAvailableAlertTitle alertMessage:kNetNotAvailableAlertMsg];
	}
	
}

- (void)onGetLoginResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *)jsonError
{
    [self addCookies:response];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	self.view.userInteractionEnabled=TRUE;
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	int m_intResponseCode = [httpResponse statusCode];

	if(m_intResponseCode==500 || m_intResponseCode==404)
	{
		[spinner stopAnimating];
		NSLog(@"response from server: %@",[[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease]);
		
		UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:@"Authentication failure!" message:@"Wrong username/password. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[networkDownAlert show];
		[networkDownAlert release];
		networkDownAlert=nil;
	} else if (m_intResponseCode == 401) {
        [[NewsDataManager sharedManager] showErrorByCode:m_intResponseCode fromSource:NSStringFromClass([self class])];
        return;
	} else if(data!=nil && m_intResponseCode==200) {
		//NSLog(@"response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
        NSString *responseStr = [[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease];
        
        NSString *trimResponse = [responseStr stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"[]"]];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[trimResponse intValue]] forKey:kCustomerId];
		[[NSUserDefaults standardUserDefaults] setObject:@"Registered" forKey:kRegistrationKey];
        tnApplication.isLogged = YES;
		//[self dismissModalViewControllerAnimated:YES];
		
		//------------------------------------------------------------
		AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
		[temp_objAudit initializeMembers];
		NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
		[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
		
		
		if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
		{
			NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
									 @"User logged in",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-1",@"productId",[NSNumber numberWithFloat:[tnApplication.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[tnApplication.m_objGetCurrentLocation m_longitude]],@"lng",nil];
			
			[temp_objAudit.m_arrAuditData addObject:temp_dict];
		}
		
		if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
		{
			[temp_objAudit sendRequestToSubmitAuditData];
		}
		//------------------------------------------------------------
		
		
        tnApplication.isUserLoggedInAfterLoggedOutState=TRUE;

        [[NewsDataManager sharedManager] getActiveCategories:tnApplication.contentViewController];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatePopular" object:nil];
        [tnApplication goToNewsPage];
        
        [[MyCLController sharedInstance].locationManager startUpdatingLocation];
        
	}	
}

#pragma mark -
#pragma mark Connection response methods

/*
 -(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
 {
 NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
 intResponseStatusCode = [httpResponse statusCode];
 [responseData setLength:0];	
 }
 */

- (void)addCookies:(NSURLResponse*)response 
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    intResponseStatusCode = [httpResponse statusCode];
    
	
	NSArray *all = [NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
    
    NSLog(@"How many Cookies: %d", all.count);
	
	[[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:all forURL:[NSURL URLWithString:
																		  [NSString stringWithFormat:@"%@/salebynow/", kServerUrl]] mainDocumentURL:nil];
	
	NSArray *availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:
                                 [NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
	
	NSLog(@"no. of cookies: %d", availableCookies.count);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory =[paths objectAtIndex:0];
	NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:@"cookieFile.plist"];
	NSDictionary *temp_dict;
	
	for (NSHTTPCookie *cookie in availableCookies)
	{
        temp_dict = [NSDictionary dictionaryWithObjectsAndKeys:
                     [NSString stringWithFormat:@"%@",kServerUrl], NSHTTPCookieOriginURL,
                     cookie.name, NSHTTPCookieName,
                     cookie.path, NSHTTPCookiePath,
                     cookie.value, NSHTTPCookieValue,
                     nil];
		[temp_dict writeToFile:writableStatePath atomically:YES];
	}
	
}

-(void) showAlertView:(NSString *)title alertMessage:(NSString *)message
{
    UIAlertView *networkDownAlert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self 
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [networkDownAlert show];
    [networkDownAlert release];
}

- (void)fbDidLogout
{
    NSLog(@"face book did logout");
}


@end
