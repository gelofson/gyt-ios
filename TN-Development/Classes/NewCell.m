//
//  NewCell.m
//  TinyNews
//
//  Created by jay kumar on 5/31/13.
//
//

#import "NewCell.h"
#import <QuartzCore/QuartzCore.h> 
@interface NewCell ()

@end

@implementation NewCell


@synthesize thumbnail;


- (id)init {
	
    if (self = [super init]) {
		
        self.frame = CGRectMake(0, 0, 80, 120);
		
		//[[NSBundle mainBundle] loadNibNamed:@"Cell" owner:self options:nil];
        CGRect tRect = CGRectInset(self.bounds, 2, 2);
        tRect.size.height = 76;
        
        self.thumbnail = [[AsyncButtonView alloc] initWithFrame:tRect];
        [self addSubview:self.thumbnail];
        
        
        self.headline = [[[UILabel alloc] initWithFrame:CGRectMake(2, 77, 76, 40)] autorelease];
        self.headline.font = [UIFont boldSystemFontOfSize:13.0];
        self.headline.numberOfLines = 0;
       // self.headline.contentMode=UIViewContentModeCenter;
        self.headline.lineBreakMode = UILineBreakModeWordWrap;
        self.headline.backgroundColor = [UIColor clearColor];
        self.headline.textColor = [UIColor blackColor];
        self.headline.textAlignment  = UITextAlignmentCenter;
        
        [self addSubview:self.headline];
	}
	
    return self;
	
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code.
 }
 */

- (void)dealloc {
    self.thumbnail = nil;
    self.headline = nil;
    [super dealloc];
}

@end
