//
//  GuideViewController.m
//  QNavigator
//
//  Created by softprodigy on 04/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GuideViewController.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"

QNavigatorAppDelegate *l_appDelegate;

@implementation GuideViewController

@synthesize m_webView;
@synthesize m_activityIndicator;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	
}


-(void)viewWillAppear:(BOOL)animated
{
	
	[m_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:
														 [NSString stringWithFormat:@"%@/salebynow/json.htm?action=getUserGuide", kServerUrl]]]];
	
	
}

-(void)viewDidDisappear:(BOOL)animated
{
	[m_webView stopLoading];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark -
#pragma mark UIWebView Methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	[m_activityIndicator startAnimating];
	return YES;
}

//webview started loading page
- (void)webViewDidStartLoad:(UIWebView *)webView
{
	
}

//webview finished loading page
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	[m_activityIndicator stopAnimating];
	
}


//webview fails to load the html page
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	[m_activityIndicator stopAnimating];
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_webView = nil;
    self.m_activityIndicator = nil;
}

- (void)dealloc {
    [m_webView release];
    [m_activityIndicator release];
    
    [super dealloc];
}

@end
