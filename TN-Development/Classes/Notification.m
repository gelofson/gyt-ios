//
//  Notification.m
//  QNavigator
//
//  Created by Nicolas Jakubowski on 10/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Notification.h"

@implementation Notification

@synthesize elapsedTime = m_elapsedTime;
@synthesize email = m_email;
@synthesize firstName = m_firstName;
@synthesize imageUrl = m_imageUrl;
@synthesize lastname = m_lastname;
@synthesize locationDetails = m_locationDetails;
@synthesize messageId = m_messageId;
@synthesize messageType = m_messageType;
@synthesize notificationMessage = m_notificationMessage;
@synthesize notificationId = m_notificationId;
@synthesize thumbImageUrl = m_thumbImageUrl;
@synthesize notificationType = m_notificationType;

- (id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)parseFromDictionary:(NSDictionary*)d{
    
    if (!d) return;
    
    self.elapsedTime = [d objectForKey:@"elapsedTime"];
    self.email = [d objectForKey:@"email"];
    self.firstName = [d objectForKey:@"firstname"];
    self.imageUrl = [d objectForKey:@"imageUrl"];
    self.lastname = [d objectForKey:@"lastname"];
    self.locationDetails = [d objectForKey:@"locationDetails"];
    self.messageId = [d objectForKey:@"messageId"];
    self.messageType = [d objectForKey:@"messageType"];
    self.notificationId = [d objectForKey:@"notificationId"];
    self.notificationMessage = [d objectForKey:@"notificationMessage"];
    self.thumbImageUrl = [d objectForKey:@"thumbImageUrl"];
    
    if ([m_messageType isEqualToString:@"AcceptOrReject"]) {
        m_notificationType = NotificationTypeAcceptReject;
    }else{
        m_notificationType = NotificationTypeMessage;
    }
    
}

@end
