//
//  VerticalScrollViewController.h
//  TinyNews
//
//  Created by Nava Carmon on 5/27/13.
//
//

#import <UIKit/UIKit.h>
#import "LoadingIndicatorView.h"
#import "NewsDataManager.h"
#import "CategoryData.h"
#import "DetailPageViewController.h"

@interface VerticalScrollViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    int curPage;
    int numPages;
    int totalRecords;
    IBOutlet UITableView *detailTableView;
	UIViewController *m_CallBackViewController;
    UILabel *m_headerLabel;
    LoadingIndicatorView *l_buyItIndicatorView;
    CategoryData *m_data;
    DetailPageViewController *detailController;
    BOOL fetchingData;
    NSInteger firstPictureID, currentIndex;
}

@property (nonatomic, retain) IBOutlet UITableView *detailTableView;
@property(nonatomic,retain) NSString *m_Type;
@property(nonatomic,retain) IBOutlet UILabel *m_headerLabel;
@property(nonatomic,retain) UIViewController *m_CallBackViewController;
@property(nonatomic,retain) CategoryData *m_data;
@property(nonatomic,retain) DetailPageViewController *detailController;
@property int curPage;
@property BOOL fetchingData;
@property NSInteger firstPictureID;
@property NSInteger currentIndex;

- (IBAction)showDetailController:(id)sender;

@end
