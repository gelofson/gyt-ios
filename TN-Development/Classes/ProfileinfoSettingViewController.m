//
//  ProfileinfoSettingViewController.m
//  QNavigator
//
//  Created by softprodigy on 31/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProfileinfoSettingViewController.h"
#import"QNavigatorAppDelegate.h"
#import"ShopbeeAPIs.h"
#import"Constants.h"
#import"LoadingIndicatorView.h"
#include<QuartzCore/QuartzCore.h>
#import "NewsDataManager.h"
@implementation ProfileinfoSettingViewController

@synthesize m_theAboutMe;
@synthesize m_interest;
@synthesize m_favouriteBrand;
@synthesize m_favouriteQuotation;
@synthesize currentText;

QNavigatorAppDelegate *l_appDelegate;
ShopbeeAPIs *l_requestObj;
LoadingIndicatorView *l_ProfileindicatorView;
#pragma mark animaion methods

-(void)animateViewUpward
{
    CGRect  rect = self.currentText.frame;
    CGFloat yOrigin = self.view.frame.size.height - 216; // height after the keyboard is up
    
    if (rect.origin.y + rect.size.height > yOrigin) {
        [UIView beginAnimations: @"upView" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.4];
        [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
        self.view.frame = CGRectMake(0, -(rect.origin.y + rect.size.height - yOrigin), 320,460);
        [UIView commitAnimations];
    }
}

-(void)animateViewDownward
{
	[UIView beginAnimations: @"downView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	self.view.frame = CGRectMake(0,0, 320,460);
	[UIView commitAnimations];
}
#pragma mark textview delegate methods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if ([text isEqualToString:@"\n"]) 
	{	
		if (textView==m_theAboutMe) 
		{
			[m_interest becomeFirstResponder];
		}
		else if(textView==m_interest) 
		{
			[m_interest resignFirstResponder];
			[m_favouriteBrand becomeFirstResponder];
			[self animateViewUpward];
		}
		else if (textView==m_favouriteBrand) 
		{
			[m_favouriteQuotation becomeFirstResponder];
		}
		else if(textView==m_favouriteQuotation)
		{
			[self animateViewDownward];
			[m_favouriteQuotation resignFirstResponder];
		}
		
        self.currentText = nil;
		//[textView resignFirstResponder];
		
		return FALSE;
	}
	//else if([textView.text isEqualToString:@"About Me: "])
//	{
//		return FALSE;
//	}
	
	return TRUE;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
	if (textView==m_favouriteBrand) {
        self.currentText = m_favouriteBrand;
		[self animateViewUpward];
	}
	else if(textView==m_favouriteQuotation) {
        self.currentText = m_favouriteQuotation;
		[self animateViewUpward];
	}
	

}


//-(BOOL)textViewShouldEndEditing:(UITextView *)textView
//{
//if (textView==m_favouriteBrand||textView==m_favouriteQuotation) {
//	[self animateViewDownward];
//}	
//	
//}
#pragma mark Custom methods


-(IBAction)btnCancelAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnSaveAction:(id)sender
{
	NSMutableString *tmp_aboutme;
	NSMutableString *tmp_interest;
	NSMutableString *tmp_favBrand;
	NSMutableString *tmp_favQuotation;
	NSString *tmp_usrId;
	tmp_aboutme=[NSMutableString stringWithString:m_theAboutMe.text];
	tmp_interest=[NSMutableString stringWithString:m_interest.text];
	tmp_favBrand=[NSMutableString stringWithString:m_favouriteBrand.text];
	tmp_favQuotation=[NSMutableString stringWithString:m_favouriteQuotation.text];
	tmp_usrId=[l_appDelegate.userInfoDic valueForKey:@"emailid"];
	
	[tmp_aboutme replaceOccurrencesOfString:@"About Me:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, m_theAboutMe.text.length)];
	[tmp_interest replaceOccurrencesOfString:@"Interests:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, m_interest.text.length)];
	[tmp_favBrand replaceOccurrencesOfString:@"Favorite News Category:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, m_favouriteBrand.text.length)];
	[tmp_favQuotation replaceOccurrencesOfString:@"Favorite Quotation:" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, m_favouriteQuotation.text.length)];
	NSDictionary *tmp_dictionary=[[NSDictionary alloc] initWithObjectsAndKeys:tmp_aboutme,@"aboutMe",tmp_interest,@"interest",tmp_favBrand,@"favBrand",tmp_favQuotation,@"favQuotes",tmp_usrId,@"userId",nil];
	l_requestObj=[[ShopbeeAPIs alloc] init];
	[l_appDelegate.userInfoDic setValue:tmp_aboutme forKey:@"aboutMe"];
	[l_appDelegate.userInfoDic setValue:tmp_interest forKey:@"interest"];
	[l_appDelegate.userInfoDic setValue:tmp_favBrand forKey:@"favBrand"];
	[l_appDelegate.userInfoDic setValue:tmp_favQuotation forKey:@"favQuotes"];
	[l_ProfileindicatorView startLoadingView:self];
	[l_requestObj updateUserInfo:@selector(requestCallBackMethod:responseData:) tempTarget:self tmp_dictionary:tmp_dictionary];
	
	
	//[self.navigationController popViewControllerAnimated:YES];
	[l_requestObj release];
	l_requestObj=nil;
	[tmp_dictionary release];
	tmp_dictionary=nil;
}
-(IBAction)btnBackAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) sendDataToDatabase{
	
	NSArray *tmp_objects=[[NSArray alloc] initWithObjects:m_theAboutMe.text,m_interest.text,m_favouriteBrand.text,m_favouriteQuotation.text,nil];
	NSArray *tmp_keys=[[NSArray alloc] initWithObjects:@"AboutMe",@"Interest",@"FavouriteBrand",@"FavouriteQuotation",nil];
	m_ProfileInfoDict=[[NSDictionary dictionaryWithObjects:tmp_objects forKeys:tmp_keys]retain ];
	DBManager *dbmanager=[DBManager SharedInstance];
	[dbmanager insertProfileData:m_ProfileInfoDict];
	[tmp_keys release];
	tmp_keys=nil;
	[tmp_objects  release];
	tmp_objects=nil;
	[self.navigationController popViewControllerAnimated:YES];
	
}
#pragma mark -
#pragma mark call back method
-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_ProfileindicatorView stopLoadingView];
	NSLog(@"data downloaded");
	//NSLog(@"Response code: %d, response string:%@",[responseCode intValue],[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
	
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	
	if ([responseCode intValue]==200) 
	{
		[self.navigationController popViewControllerAnimated:YES];
	}
    else {
        NSLog(@"Privacy settings response result: %d",[responseCode intValue]);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	[tempString release];
	tempString=nil;
}	

//-(IBAction) m_getDataFromDatabase{
//	m_profileSettings=[[NSArray alloc] initWithObjects:[m_getDatabaseData valueForKey:@"AboutMe"],[m_getDatabaseData valueForKey:@"Interest"]
//					   ,[m_getDatabaseData valueForKey:@"FavouritBrand"],[m_getDatabaseData valueForKey:@"FavouriteQuotation"],nil];
//
//
//}



#pragma mark ---------------
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void)viewDidAppear:(BOOL)animated
{
	
}
-(void)viewDidLoad
{
    
    m_theAboutMe.layer.borderWidth=1.0f;
    m_theAboutMe.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    m_interest.layer.borderWidth=1.0f;
    m_interest.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    m_favouriteBrand.layer.borderWidth=1.0f;
    m_favouriteBrand.layer.borderColor=[[UIColor lightGrayColor] CGColor];
    
    m_favouriteQuotation.layer.borderWidth=1.0f;
    m_favouriteQuotation.layer.borderColor=[[UIColor lightGrayColor] CGColor];

    
//	m_theAboutMe.font=[UIFont fontWithName:kFontName size:14];
	m_theAboutMe.textColor =[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	
//	m_interest.font=[UIFont fontWithName:kFontName size:14];
	m_interest.textColor=[UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	
//	m_favouriteQuotation.font=[UIFont fontWithName:kFontName size:14];
	m_favouriteQuotation.textColor= [UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	
//	m_favouriteBrand.font=[UIFont fontWithName:kFontName size:14];
	m_favouriteBrand.textColor= [UIColor colorWithRed:.32f green:.32f blue:.32f alpha:1.0f];
	l_ProfileindicatorView=[LoadingIndicatorView SharedInstance];

	
}

-(void)viewWillAppear:(BOOL)animated
{
	[m_theAboutMe setText:[NSString stringWithFormat:@"About Me: %@ ",[l_appDelegate.userInfoDic valueForKey:@"aboutMe"]]];
	[m_favouriteBrand setText:[NSString stringWithFormat:@"Favorite Brand: %@ ",[l_appDelegate.userInfoDic valueForKey:@"favBrand"]]];
	[m_favouriteQuotation setText:[NSString stringWithFormat:@"Favorite Quotation: %@ ",[l_appDelegate.userInfoDic valueForKey:@"favQuotes"]]];
	[m_interest setText:[NSString stringWithFormat:@"Interests: %@",[l_appDelegate.userInfoDic valueForKey:@"interest"]]];
	if ([[l_appDelegate.userInfoDic valueForKey:@"aboutMe"] isEqualToString:@""]) {
		[m_theAboutMe setText:@"About Me: "];
	}
	if ([[l_appDelegate.userInfoDic valueForKey:@"favBrand"] isEqualToString:@""]) {
		[m_favouriteBrand setText:@"Favorite Brand: "];
	}
	if ([[l_appDelegate.userInfoDic valueForKey:@"favQuotes"] isEqualToString:@""]) {
		[m_favouriteQuotation setText:@"Favorite Quotation: "];
	}
	if ([[l_appDelegate.userInfoDic valueForKey:@"interest"] isEqualToString:@""]) {
		[m_interest setText:@"Interests: "];
	}
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];

    // Release retained IBOutlets.
    self.m_theAboutMe = nil;
    self.m_interest = nil;
    self.m_favouriteBrand = nil;
    self.m_favouriteQuotation = nil;
}

- (void)dealloc {
	[m_theAboutMe release];
	[m_interest release];
	[m_favouriteBrand release];	
	[m_favouriteQuotation release];
    [m_ProfileInfoDict release];
    [m_getDatabaseData release];
	
    [super dealloc];
}

@end
