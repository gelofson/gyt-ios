//
//  PrivacySettingsViewController.h
//  QNavigator
//
//  Created by softprodigy on 14/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PrivacySettingsViewController : UIViewController <UIActionSheetDelegate>{
	
	UIScrollView *m_scrollView;
	UILabel *m_label;
	UILabel *m_aboutmelabel;
	UILabel *m_interestedlabel;
	UILabel *m_fvbrandlabel;
	UILabel *m_fvquotationlabel;
	NSDictionary *m_SettingsDictionary;
    UILabel *m_HeaderLable;
}
@property(nonatomic,retain)IBOutlet UILabel *m_HeaderLable;
@property(nonatomic,retain) IBOutlet UIScrollView *m_scrollView;

@property(nonatomic,retain) IBOutlet UILabel *m_label;

-(IBAction) m_goToBackView;

-(IBAction)m_feedBtnAction:(id)sender;

-(IBAction)SaveBtnAction;

-(IBAction)InitializeScrollView;

@end
