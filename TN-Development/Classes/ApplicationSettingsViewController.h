//
//  ApplicationSettingsViewController.h
//  QNavigator
//
//  Created by Bharat Biswal on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GeoSelectViewController;

@interface ApplicationSettingsViewController : UIViewController <UIActionSheetDelegate>{
	
	UIScrollView *m_scrollView;
	UILabel *m_label;
	GeoSelectViewController * locationPickerController;
}

@property (nonatomic, retain) GeoSelectViewController	*locationPickerController;
@property(nonatomic,retain) IBOutlet UIScrollView *m_scrollView;

@property(nonatomic,retain) IBOutlet UILabel *m_label;

-(IBAction) m_goToBackView;

-(IBAction) editFontSizeAction:(id)sender;

-(IBAction)SaveBtnAction;

-(IBAction)InitializeScrollView;

@end
