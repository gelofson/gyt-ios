//
//  LoginViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 30/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"
#import "MyCLController.h"
#import "AuditingClass.h"
#import "JSON.h"
#import "MessageBoardManager.h"
// GDL: We use this, so we should import it.
#import "ForgetPassword.h"
#import "NewsDataManager.h"

@implementation LoginViewController

@synthesize m_txtEmail;
@synthesize m_txtPassword;
@synthesize m_mutResponseData;	
@synthesize m_activityIndicator;
@synthesize m_intResponseStatusCode;
@synthesize m_btnCancel;
@synthesize m_fromRegistration;
@synthesize m_rememberMeButton;
@synthesize m_isLoggedOut;
@synthesize m_objLoginHome;

NSURLConnection *l_theConnection;
QNavigatorAppDelegate *l_appDelegate;
ForgetPassword *l_forgotpassword;

#pragma mark -

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *userName = [prefs stringForKey:@"userName"];
	NSString *password = [prefs stringForKey:@"password"];
	NSString *remember = [prefs stringForKey:@"remember"];
	
    
	
	if([remember isEqualToString:@"dontRememberMe"])
	{   
		m_txtEmail.text =@"";
		m_txtPassword.text=@"";
		isDontRememberMeState=YES;
		
		
	}
	else 
	{
		m_txtEmail.text = userName;
		m_txtPassword.text=password;
		isDontRememberMeState=NO;
				
		
	}
	
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	m_mutResponseData=[[NSMutableData alloc]init];
	[m_txtEmail becomeFirstResponder];
    
	[m_rememberMeButton addTarget:self action:@selector(dontRememberMeAction:) forControlEvents:UIControlEventTouchUpInside];
	
	[m_rememberMeButton setTag:116];

}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *userName = [prefs stringForKey:@"userName"];
	NSString *password = [prefs stringForKey:@"password"];
	NSString *remember = [prefs stringForKey:@"remember"];
	
	
	if([remember isEqualToString:@"dontRememberMe"])
	{   
		m_txtEmail.text =@"";
		m_txtPassword.text=@"";
		
		isDontRememberMeState=YES;
	}
	else 
	{
		m_txtEmail.text = userName;
		m_txtPassword.text=password;
		isDontRememberMeState=NO;
	}
	
}

// GDL: Changed following two methods.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_txtEmail = nil;
    self.m_txtPassword = nil;
    self.m_activityIndicator = nil;
    self.m_btnCancel = nil;
}

- (void)dealloc {
    [m_txtEmail release];
    [m_txtPassword release];
    [m_activityIndicator release];
    [m_mutResponseData release];
    [m_btnCancel release];
    [m_rememberMeButton release];
    [m_objLoginHome release];
    [doneBackground release];
    [doneButton release];
    
    [super dealloc];
}

#pragma mark -
#pragma mark Text field delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if(textField.text.length>=50 && range.length==0)
	{
		return NO;
	}
	return YES;
	
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	if(!doneBackground)
	{
		[self addTopBarOnKeyboard];	
	}
	
return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if(textField==m_txtEmail)
	{
		[m_txtPassword becomeFirstResponder];
	}
	else if(textField==m_txtPassword)
	{
		m_txtEmail.text=[m_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		m_txtPassword.text=[m_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
		if(m_txtEmail.text.length==0 || m_txtPassword.text.length==0)
		{
			UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input username/password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[temp_alertView show];
			[temp_alertView release];
			temp_alertView=nil;
		}
		else
		{
			[self performSelector:@selector(btnGoAction:)];
		}
	}
	//[textField resignFirstResponder];
	
	return YES;
	doneButton.hidden=YES;
	doneBackground.hidden=YES;

}

#pragma mark -
#pragma mark Custom methods
- (void) dontRememberMeAction :(id) sender
{
	
	if (!isDontRememberMeState) {
		
		
		[(UIButton*)sender setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"sharenow_checkbox_enabled" ofType:@"png"]] 
						   forState:UIControlStateNormal];
		
		isDontRememberMeState = YES;
	}
	else {
		
		[(UIButton*)sender setImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"sharenow_checkbox_disabled" ofType:@"png"]] 
						   forState:UIControlStateNormal];
		isDontRememberMeState = NO;
	}
}


-(IBAction)btnCancelAction:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnGoAction:(id)sender
{
	[m_txtEmail resignFirstResponder];
	[m_txtPassword resignFirstResponder];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	
	m_txtEmail.text=[m_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	m_txtPassword.text=[m_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if(m_txtEmail.text.length==0 || m_txtPassword.text.length==0)
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input username/password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}
	
	else{
		
		if (isDontRememberMeState==YES )  //|| isDontRememberMeState==NO) 
		{
		[prefs setObject:[m_txtEmail.text lowercaseString] forKey:@"userName"];
		[prefs setObject:m_txtPassword.text forKey:@"password"];
		[prefs setObject:@"dontRememberMe" forKey:@"remember"];
		[self sendRequestForLogin];
		
		NSLog(@"Prefs value:- %@",prefs);
			
		//m_txtEmail.text=@"";
		//m_txtPassword.text=@"";
		
	}
	else 
	{
		[prefs setObject:[m_txtEmail.text lowercaseString]  forKey:@"userName"];
		[prefs setObject:m_txtPassword.text forKey:@"password"];
		[prefs setObject:@"rememberMe" forKey:@"remember"];
		NSLog(@"The username is %@",[prefs valueForKey:@"userName"]);
		NSLog(@"The password is %@",[prefs valueForKey:@"password"]);
		[self sendRequestForLogin];
	}
	}

	doneButton.hidden=YES;
	doneBackground.hidden=YES;
}


-(IBAction)btnforgotPassword:(id)sender
{
	l_forgotpassword=[[ForgetPassword alloc]init];//initWithNibName:@"ForgetPassword" bundle:nil];
	//l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
//	[self.view removeFromSuperview];
//	[l_appDelegate.window addSubview:m_objforgetpassword.view];
//	
	[self presentViewController:l_forgotpassword animated:YES completion:nil];
	[l_forgotpassword release];
	l_forgotpassword=nil;
	
	
}
-(void)addTopBarOnKeyboard
{	if(doneBackground)
	{
			[doneBackground removeFromSuperview];
			[doneBackground release];
			doneBackground=nil;
		}
		if(doneButton)
		{
			[doneButton removeFromSuperview];
			[doneButton release];
			doneButton=nil;
		}
	
	
	
doneBackground=[[UIImageView alloc]initWithFrame:CGRectMake(0,205,320,40)];
	//[doneBackground setTag:1001];
	doneBackground.image=[UIImage imageNamed:@"bar-for-wheel.png"];
	[self.view addSubview:doneBackground];
	
	doneButton = [[UIButton alloc]initWithFrame:CGRectMake(255,210, 54, 28)];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    if (screenRect.size.height > 480)
    {
        int diff = screenRect.size.height - 480; // device has these many pixels more
        
        [doneBackground setFrame:CGRectMake(0, 205 + diff, 320, 40)];
        [doneButton setFrame:CGRectMake(255, 210 + diff, 54, 28)];
        
    }
    
    
	//[doneButton retain];
	//	[doneButton setTag:1002];
	[doneButton setBackgroundImage:[UIImage imageNamed:@"wheel_cancel.png"] forState:UIControlStateNormal];
	[doneButton addTarget:self action:@selector(doneClick)
		 forControlEvents:UIControlEventTouchUpInside];	
	[self.view addSubview:doneButton];
	}

-(void)doneClick
{
	[m_txtEmail resignFirstResponder];	
	[m_txtPassword resignFirstResponder];
	doneButton.hidden=YES;
	doneBackground.hidden=YES;
	[doneBackground release];
	doneBackground = nil;
}

#pragma mark -
#pragma mark Web Service methods

-(void)sendRequestForLogin
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=loginCustomer&email=%@&pass=%@",kServerUrl,[m_txtEmail.text lowercaseString] , m_txtPassword.text];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		//NSLog(@"device used:%@\n",[[UIDevice currentDevice] model]);
		
		if([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"])
		{
			NSLog(@"No need to send token message\n");
		}
		else 
		{
			NSDictionary *tmp_deviceDetails=[NSDictionary dictionaryWithObjectsAndKeys:l_appDelegate.m_strDeviceId,@"did",l_appDelegate.m_strDeviceToken,@"deviceToken",nil];
			NSString *temp_strJson=[NSString stringWithFormat:@"devicedetails=%@",[tmp_deviceDetails JSONFragment]];
			[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		}
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
	
	[doneBackground removeFromSuperview];
	[doneBackground release];
	doneBackground=nil;
	
	[doneButton removeFromSuperview];
	[doneButton release];
	doneButton=nil;
	
}

#pragma mark -
#pragma mark Connection response methods
/*
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	[m_mutResponseData setLength:0];	
}*/

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse*)response 
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseStatusCode = [httpResponse statusCode];
	
	NSArray * all = [NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
    NSLog(@"How many Cookies: %d", all.count);
	
	[[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:all forURL:[NSURL URLWithString:
																		  [NSString stringWithFormat:@"%@/salebynow/", kServerUrl]] mainDocumentURL:nil];
	
	NSArray * availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:
								  [NSURL URLWithString:[NSString stringWithFormat:@"%@/salebynow/", kServerUrl]]];
	
	NSLog(@"no. of cookies: %d",availableCookies.count);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory =[paths objectAtIndex:0];
	NSString *writableStatePath=[documentsDirectory stringByAppendingPathComponent:@"cookieFile.plist"];
	NSDictionary *temp_dict;
	
	for (NSHTTPCookie *cookie in availableCookies)
	{
        temp_dict = [NSDictionary dictionaryWithObjectsAndKeys:
									[NSString stringWithFormat:@"%@",kServerUrl], NSHTTPCookieOriginURL,
									cookie.name, NSHTTPCookieName,
									cookie.path, NSHTTPCookiePath,
									cookie.value, NSHTTPCookieValue,
									nil];
		[temp_dict writeToFile:writableStatePath atomically:YES];
		//NSHTTPCookie *cookie2 = [NSHTTPCookie cookieWithProperties:temp_dict];
		//NSLog(@"%@",temp_dict);
		//NSLog(@"Name: %@ : Value: %@, Expires: %@", cookie2.name, cookie2.value, cookie2.expiresDate); 
		
	}
	
	NSLog(@"%d",m_intResponseStatusCode);
	
	[m_mutResponseData setLength:0];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"%@",[[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease]);
	[m_mutResponseData appendData:data];
	
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [m_activityIndicator stopAnimating];
	
	self.view.userInteractionEnabled=TRUE;
	
	l_appDelegate.m_loginViewCtrl=self;//pass the self object so that we can dismiss login view when data is loaded
	
	if(m_intResponseStatusCode==404)
	{
		NSLog(@"response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:@"Authentication failure!" message:@"Wrong username/password. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[networkDownAlert show];
		[networkDownAlert release];
		networkDownAlert=nil;
	} else if (m_intResponseStatusCode == 401 || m_intResponseStatusCode == 500){
        [[NewsDataManager sharedManager] showErrorByCode:m_intResponseStatusCode fromSource:NSStringFromClass([self class])];
	} else if(m_mutResponseData!=nil && m_intResponseStatusCode==200)
	{
		//NSLog(@"response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
        NSString *responseStr = [[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease];
        
        NSString *trimResponse = [responseStr stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"[]"]];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[trimResponse intValue]] forKey:kCustomerId];
        
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:kRegistrationKey];
        l_appDelegate.isLogged = YES;
        [self performSelectorOnMainThread:@selector(dismissLoginView) withObject:nil waitUntilDone:YES];
		
        [[NewsDataManager sharedManager] getAllCategories:nil];
        
		//------------------------------------------------------------
		AuditingClass *temp_objAudit=[AuditingClass SharedInstance];
		[temp_objAudit initializeMembers];
		NSDateFormatter *temp_dateFormatter=[[[NSDateFormatter alloc]init] autorelease] ;
		[temp_dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm:ss z"];
		
		
		if(temp_objAudit.m_arrAuditData.count<=kTotalAuditRecords)
		{
			NSDictionary *temp_dict=[NSDictionary dictionaryWithObjectsAndKeys:
									 @"User logged in",@"action",[NSString stringWithFormat:@"%@",[temp_dateFormatter stringFromDate:[NSDate date]]],@"dateStr",@"-1",@"productId",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_latitude]],@"lat",[NSNumber numberWithFloat:[l_appDelegate.m_objGetCurrentLocation m_longitude]],@"lng",nil];
			
			[temp_objAudit.m_arrAuditData addObject:temp_dict];
			//NSLog(@"count: %d, dict: %@",[temp_objAudit.m_arrAuditData count], temp_dict);
		}
		
		if(temp_objAudit.m_arrAuditData.count>=kTotalAuditRecords)
		{
			[temp_objAudit sendRequestToSubmitAuditData];
		}
		//------------------------------------------------------------
		
        if (m_fromRegistration) {
            [[MessageBoardManager sharedManager] sendBuzzRequest];
            m_fromRegistration = NO;
        }
        
		if(m_isLoggedOut==TRUE)
		{
			
			m_isLoggedOut=FALSE;
			
			l_appDelegate.isUserLoggedInAfterLoggedOutState=TRUE;
			
            [[NewsDataManager sharedManager] getActiveCategories:tnApplication.contentViewController];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdatePopular" object:nil];
            [tnApplication goToNewsPage];

			[[MyCLController sharedInstance].locationManager startUpdatingLocation];
			
            
		}
		else 
		{
			//[m_activityIndicator startAnimating];
			// to be set to false later
			[self.view setUserInteractionEnabled:FALSE];
			[m_txtEmail resignFirstResponder];
			
			[m_txtPassword resignFirstResponder];
			
			if ([CLLocationManager locationServicesEnabled])
                [[MyCLController sharedInstance].locationManager startUpdatingLocation];
            else
                [tnApplication failToUpdateLocation];
        }	
    }
}

-(void)dismissLoginView
{
	[self.m_objLoginHome dismissViewControllerAnimated:YES completion:^{
        self.m_objLoginHome = nil;
    }];
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [m_activityIndicator stopAnimating];
    [self dismissLoginView];
	
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
	
}

@end
