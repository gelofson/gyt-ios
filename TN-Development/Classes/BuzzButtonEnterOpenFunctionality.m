//
//  BuzzButtonEnterOpenFunctionality.m
//  TinyNews
//
//  Created by jay kumar on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BuzzButtonEnterOpenFunctionality.h"
#import "QNavigatorAppDelegate.h"
#import "BuzzButtonShareNowFunctionality.h"
#import "Constants.h"

@implementation BuzzButtonEnterOpenFunctionality

QNavigatorAppDelegate *l_appDelegate;

@synthesize m_textView;
@synthesize m_strType;
@synthesize  m_strMessage;
@synthesize m_whoMessage;
@synthesize m_whatMessage;
@synthesize m_whenMessage;
@synthesize m_whereMessage;
@synthesize m_howMessage;
@synthesize m_whyMessage;
@synthesize photoImage;
@synthesize m_headerMessage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark- textView delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose the best place for this new photo:" 
                                                                 delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil 
                                                        otherButtonTitles:[[Context getInstance] getDisplayTextFromMessageType:@"How Do I Look?"], 
                                      [[Context getInstance] getDisplayTextFromMessageType:@"I Bought It!"],
                                      [[Context getInstance] getDisplayTextFromMessageType:@"Found a sale!"],
                                      [[Context getInstance] getDisplayTextFromMessageType:@"Check This Out..."],
                                      
                                      [[Context getInstance] getDisplayTextFromMessageType:@"Buy It Or Not?"],
                                      
                                      nil];
        
        [actionSheet showFromTabBar:l_appDelegate.tabBarController.tabBar];
        [actionSheet release];
 return NO;
    }
    
    return YES;
}

#pragma ActionheetDelegate method
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex == 0) {
		self.m_strType = kHowDoesThisLook;
	}else if(buttonIndex == 1) {
		self.m_strType = kIBoughtIt;
	}else if(buttonIndex == 2) {
		self.m_strType = kFoundASale;
	}else if(buttonIndex == 3) {
		self.m_strType = kCheckThisOut;
	}else if(buttonIndex == 4) {
		self.m_strType = kBuyItOrNot;
	}else {
		self.m_strType = kCheckThisOut;
	}
    [actionSheet dismissWithClickedButtonIndex:buttonIndex animated:YES];
    if (buttonIndex != 5)
        [self finallyPerformRealShare];
    
}

-(void) finallyPerformRealShare {
	
	BuzzButtonShareNowFunctionality * vc = [[BuzzButtonShareNowFunctionality alloc] init];
	vc.photoImage =self.photoImage;
	vc.m_strType = self.m_strType;
	vc.m_strMessage = self.m_strMessage;
    vc.m_whoMessage= self.m_whoMessage;
    vc.m_whatMessage=self.m_whatMessage;
    vc.m_whenMessage=self.m_whenMessage;
    vc.m_whereMessage=self.m_whereMessage;
    vc.m_howMessage=self.m_howMessage;
    vc.m_whyMessage=self.m_whyMessage;
    vc.m_headerMessage=self.m_headerMessage;
    vc.m_openMessage = self.m_textView.text;
	[self.navigationController pushViewController:vc animated:YES];
	[vc release];
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIColor *color=[UIColor colorWithPatternImage:[UIImage imageNamed:@"opedtextbox.png"]];
    m_textView.backgroundColor=color;
    // Do any additional setup after loading the view from its nib.
}
#pragma mark- Custome Button Action

/*
-(IBAction)clicktoBack:(id)sender
{
    [self.m_textView resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];   
}
*/

-(IBAction)clickToNext:(id)sender
{
    [self.m_textView resignFirstResponder];
    [self finallyPerformRealShare];   
}

-(IBAction)clickToCancel:(id)sender
{
    [self.m_textView resignFirstResponder];
    [self.navigationController popToRootViewControllerAnimated:YES];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc
{
   // [m_textView release];
    [m_strType release];
    
    [m_strMessage release];
    [m_whoMessage release];
    [m_whatMessage release];
    [m_whenMessage release];
    [m_whereMessage release];
    [m_howMessage release];
    [m_whyMessage release];
    [photoImage release];
    [m_headerMessage release];
    [super dealloc];
}
@end
