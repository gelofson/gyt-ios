//
//  TellAFriendViewController.h
//  QNavigator
//
//  Created by softprodigy on 04/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface TellAFriendViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
	UIButton	*m_btnTellAFriend;
}

@property(nonatomic,retain)IBOutlet UIButton *m_btnTellAFriend;

-(IBAction)btnTellAFriendAction:(id)sender;


@end
