//
//  TOSViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 30/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TOSViewController.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"
#import "NewsDataManager.h"
@implementation TOSViewController

@synthesize m_btnDone;
@synthesize m_strTypeOfData;
@synthesize m_webView;
@synthesize m_lblTitle;
@synthesize m_activityIndicator;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	if ([m_strTypeOfData intValue]==0)
	{
		m_lblTitle.text=@"Terms of Use";
        NSString * urlSTring = [NSString stringWithFormat:@"%@/salebynow/json.htm?action=getProductTOS&product=fashiongram",kServerUrl];
		[m_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:
															 urlSTring]]];
        NSLog(@"TermsOfUse URL: [%@]", urlSTring);
	}
	else if([m_strTypeOfData intValue]==1)
	{
		m_lblTitle.text=@"Privacy policy";
        NSString * urlSTring = [NSString stringWithFormat:@"%@/salebynow/json.htm?action=getPrivacyAgreement&product=fashiongram",kServerUrl];
		[m_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:
															 urlSTring]]];
        NSLog(@"PrivacyPolicy URL: [%@]", urlSTring);

	}
}

-(IBAction)btnDoneAction:(id)sender
{
	[m_webView stopLoading];
	[self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIWebView Methods
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
	[m_activityIndicator startAnimating];
	return YES;
}

//webview started loading page
- (void)webViewDidStartLoad:(UIWebView *)webView
{
	
}

//webview finished loading page
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	[m_activityIndicator stopAnimating];
	
}


//webview fails to load the html page
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	[m_activityIndicator stopAnimating];
	if (error) {
        NSLog(@"Privacy settings response result: %d",[error code]);
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
    }
	
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_btnDone = nil;
    self.m_lblTitle = nil;
    self.m_webView = nil;
    self.m_activityIndicator = nil;
}


- (void)dealloc {
    [m_btnDone release];
    [m_lblTitle release];
    [m_webView release];
    [m_strTypeOfData release];
    [m_activityIndicator release];
    
    [super dealloc];
}

@end
