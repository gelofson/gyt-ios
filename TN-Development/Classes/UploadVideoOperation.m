//
//  UploadVideoOperation.m
//  TinyNews
//
//  Created by Nava Carmon on 2/7/13.
//
//

#import "UploadVideoOperation.h"
#import "ASIFormDataRequest.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"

@implementation UploadVideoOperation

@synthesize delegate, urlString, tempPath;

- (void) main
{
    NSAutoreleasePool *aPool = [[NSAutoreleasePool alloc] init];
    
    if (![self isCancelled]) {
        [self performMain];
    }
    
    [aPool drain];
}

- (void) performMain
{
    __block NSURL *url = [NSURL URLWithString:[self.urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    __block ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request addFile:self.tempPath forKey:@"file"];
    [request addPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:kCustomerId] forKey:@"custid"];
    [request setRequestMethod:@"POST"];
    [request setDelegate:self];
    [request setTimeOutSeconds:500];
    @try {
        [request startSynchronous];
    }
    @catch (NSException *exception) {
        [self requestFailed:request];
    }

    NSLog(@"responseStatusCode %i",[request responseStatusCode]);
    NSLog(@"responseString %@",[request responseString]);
}

- (void) requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"failed with error %@", [[request error] localizedDescription]);
    if ([delegate respondsToSelector:@selector(problemWithSendingVideo)]) {
        [delegate performSelector:@selector(problemWithSendingVideo) withObject:nil];
    }
}

- (void) requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"%@ succeeded", [request url]);
    
    if (request.rawResponseData) {
        if ([delegate respondsToSelector:@selector(sendVideoBuzz:)]) {
            [delegate performSelectorInBackground:@selector(sendVideoBuzz:) withObject:[request responseString]];
        }
    }
}


@end
