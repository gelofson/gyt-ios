//........................................................................//
//..................................................//
//  MyPageFeedBackViewController.h
//  QNavigator
//
//  Created by softprodigy on 01/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyPageFeedBackViewController : UIViewController<UITextViewDelegate,UIAlertViewDelegate> {
	UIButton *grayButton1,*grayButton2,*grayButton3,*grayButton4,*grayButton5;
	UITextView *m_textView;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	NSInteger Rating;
		
}

@property(nonatomic,retain)IBOutlet UIButton *grayButton1;
@property(nonatomic,retain)IBOutlet UIButton *grayButton2;
@property(nonatomic,retain)IBOutlet UIButton *grayButton3;
@property(nonatomic,retain)IBOutlet UIButton *grayButton4;
@property(nonatomic,retain)IBOutlet UIButton *grayButton5;
@property(nonatomic,retain) IBOutlet UITextView *m_textView;

-(IBAction)btn_gray1;
-(IBAction)btn_gray2;
-(IBAction)btn_gray3;
-(IBAction)btn_gray4;
-(IBAction)btn_gray5;
-(IBAction) m_goToBackView;
-(IBAction)sendBtnAction;
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage ;

@end
