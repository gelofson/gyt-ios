//
//  InviteFriendsViaEmail.h
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
//#import "TwitterProcessing.h"
#import "JSON.h"

#import"SA_OAuthTwitterEngine.h"
#import "SA_OAuthTwitterController.h"


#define CONTACT_ENTRY_SELECT_TAG 20011
#define CONTACT_ENTRY_NAME_TAG 20012
#define CONTACT_ENTRY_EMAIL_TAG 20013

#define SHARENOW_REQUEST_TYPE_ADDCONTACT 1111
#define SHARENOW_REQUEST_TYPE_DELCONTACT 2222
#define SHARENOW_REQUEST_TYPE_GETCONTACT 3333
#define SHARENOW_REQUEST_TYPE_SHARENOW	 4444

@class BuzzButtonAddFromAddressBook;
@interface InviteFriendsViaEmail : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate > {
	
	NSString *  m_strType;
	NSString *  m_strMessage;

	IBOutlet UITableView *m_emailTableView;
	IBOutlet UIButton *m_facebookButton;
	IBOutlet UIButton *m_twitterButton;
	IBOutlet UIButton *m_selectAllButton;
	
	IBOutlet UIView *m_sectionHeaderView;
	IBOutlet UIImageView *m_photoView;

	UIImage * photoImage;
	
	NSMutableDictionary *m_DictOfContactDicts;
	NSMutableDictionary *m_newlyAddedContactsDict;
	NSMutableDictionary *m_deletedContactsDict;

	BuzzButtonAddFromAddressBook * addrBookVc;
	
	NSMutableData *m_mutResponseData;
	int m_intResponseCode;
	int m_intRequestType;
}

@property (nonatomic,assign) int m_intRequestType;
@property (nonatomic,assign) int m_intResponseCode;
@property (nonatomic,retain) NSMutableData *m_mutResponseData;
@property (nonatomic,retain) IBOutlet UITableView *m_emailTableView;
@property (nonatomic,retain) IBOutlet UIButton *m_selectAllButton;
@property (nonatomic,retain) IBOutlet UIView *m_sectionHeaderView;
@property (nonatomic,retain) NSMutableDictionary *m_DictOfContactDicts;
@property (nonatomic,retain) NSMutableDictionary *m_newlyAddedContactsDict;
@property (nonatomic,retain) NSMutableDictionary *m_deletedContactsDict;
@property (nonatomic,retain) BuzzButtonAddFromAddressBook *addrBookVc;

-(IBAction)goToBackView;
-(IBAction) contactsSelectAllToggleAction:(id) sender;
-(IBAction) contactSelectToggleAction:(id) sender;

-(IBAction) doShareNow:(id) sender;
-(IBAction) addNewEmailAction:(id) sender;


@end


