//
//  MyPageFeedBackViewController.m
//  QNavigator
//
//  Created by softprodigy on 01/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyPageFeedBackViewController.h"
#import"QNavigatorAppDelegate.h"
#import<QuartzCore/QuartzCore.h>
#import"Constants.h"
#import"ShopbeeAPIs.h"
#import"LoadingIndicatorView.h"
#import "NewsDataManager.h"

@implementation MyPageFeedBackViewController

QNavigatorAppDelegate *l_appDelegate;

LoadingIndicatorView *l_feedbackPageIndicatorView;

@synthesize grayButton1,grayButton2,grayButton3,grayButton4,grayButton5;
@synthesize m_textView;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	Rating=0;
	[m_textView.layer setCornerRadius:8.0f];
	[m_textView.layer setMasksToBounds:YES]; 
	m_textView.font=[UIFont fontWithName:kFontName size:14];
    m_textView.text=@"Tell us about it...";
	m_textView.delegate=self;
	l_feedbackPageIndicatorView=[LoadingIndicatorView SharedInstance];
}
-(void)viewWillAppear:(BOOL)animated{
	
}
-(void)viewDidAppear:(BOOL)animated
{
	
}
#pragma mark -
#pragma mark custom methods
-(IBAction)btn_gray1
{
	Rating=1;
	 UIImage *img = [UIImage imageNamed:@"blue_star.png"];
	[grayButton1 setImage:img forState:UIControlStateNormal];
	[grayButton2 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	[grayButton3 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	[grayButton4 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	[grayButton5 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	
}

-(IBAction)btn_gray2
{
	Rating=2;
	UIImage *img = [UIImage imageNamed:@"blue_star.png"];
	[grayButton1 setImage:img forState:UIControlStateNormal];
	[grayButton2 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton3 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	[grayButton4 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	[grayButton5 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
}

-(IBAction)btn_gray3
{
	Rating=3;
	UIImage *img = [UIImage imageNamed:@"blue_star.png"];
	[grayButton1 setImage:img forState:UIControlStateNormal];
	[grayButton2 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton3 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton4 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	[grayButton5 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	
	//[Radio1 setTag:0]; [Radio2 setTag:0]; [Radio3 setTag:1]; [Radio4 setTag:0]; [Radio5 setTag:0];
}

-(IBAction)btn_gray4
{
	Rating=4;
	UIImage *img = [UIImage imageNamed:@"blue_star.png"];
	[grayButton1 setImage:img forState:UIControlStateNormal];
	[grayButton2 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton3 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton4 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton5 setImage:[UIImage imageNamed:@"gray_star.png"] forState:UIControlStateNormal];
	
}


-(IBAction)btn_gray5
{
	Rating=5;
	UIImage *img = [UIImage imageNamed:@"blue_star.png"];
	[grayButton1 setImage:img forState:UIControlStateNormal];
	[grayButton2 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton3 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton4 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	[grayButton5 setImage:[UIImage imageNamed:@"blue_star.png"] forState:UIControlStateNormal];
	
}

-(IBAction)sendBtnAction
{
	
	[m_textView resignFirstResponder];
	NSString *Username= [l_appDelegate.userInfoDic valueForKey:@"emailid"] ;
	NSLog(@"the name is%@",Username); 
	NSMutableString *tmp_comments=[NSMutableString stringWithString:m_textView.text];
	//[tmp_comments stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	//[tmp_comments stringByReplacingOccurrencesOfString:@"  " withString:@""];
	tmp_comments=[NSMutableString stringWithString:[tmp_comments stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
	[tmp_comments replaceOccurrencesOfString:@"Tell us about it..." withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, tmp_comments.length)];
	NSLog(@"The length of the string is %d",[tmp_comments length]);
	NSLog(@"The comments are %@",tmp_comments);
	if (Rating==0||[tmp_comments length]==0)
	{
		UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please provide rating and your comments!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	else if(Rating>0&&tmp_comments.length!=0) 
	{
		
		NSDictionary *tmp_dictionary=[[NSDictionary alloc]initWithObjectsAndKeys:Username,@"custid",[NSNumber numberWithInt:Rating],@"rating",tmp_comments,@"comments",nil];
		ShopbeeAPIs *tmp_obj=[[ShopbeeAPIs alloc] init];
		[l_feedbackPageIndicatorView startLoadingView:self];
		[tmp_obj RateShopbeeApp:@selector(requestCallBackMethod:responseData:) tempTarget:self tmp_dictionary:tmp_dictionary];
		[tmp_dictionary release];
		tmp_dictionary=nil;
		[tmp_obj release];
		tmp_obj=nil;
	}
}

-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage 
{
	UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:alertTitle message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[tempAlert show];
	[tempAlert release];
	tempAlert=nil;
}

-(IBAction) m_goToBackView{
	
	[self.navigationController popViewControllerAnimated:YES];
	
}

#pragma mark -
#pragma mark callBack methods

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData
{
	[l_feedbackPageIndicatorView stopLoadingView];
	NSLog(@"data downloaded");
	NSString *tempString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSLog(@"response string: %@",tempString);
	[tempString release];
	tempString=nil;
	
	if ([responseCode intValue]==200)
	{
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Message sent"message:@"Your message was sent successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tempAlert show];
		[tempAlert setTag:1];
		[tempAlert release];
		tempAlert=nil;;
	}
	else             
        [[NewsDataManager sharedManager] showErrorByCode:[responseCode intValue] fromSource:NSStringFromClass([self class])];
	
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark textview delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	[m_textView setText:@""];
	return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	if ([text isEqualToString:@"\n"]) 
	{
		[m_textView resignFirstResponder];
	}
	return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
	
	[m_textView resignFirstResponder];	
	
}
#pragma mark -
#pragma mark alertview delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (alertView.tag==1) {
		
	
if (buttonIndex==0) {
	[self.navigationController popViewControllerAnimated:YES];
}	
	
	}
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.grayButton1 = nil;
    self.grayButton2 = nil;
    self.grayButton3 = nil;
    self.grayButton4 = nil;
    self.grayButton5 = nil;
    self.m_textView = nil;
}


- (void)dealloc {
    
	[grayButton1 release];
	[grayButton2 release];
	[grayButton3 release];
	[grayButton4 release];
	[grayButton5 release];
	[m_textView release];
    [m_view release];
    [m_indicatorView release];
    
    [super dealloc];
}


@end
