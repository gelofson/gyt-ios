//
//  TwitterProcessing.m
//  QNavigator
//
//  Created by softprodigy on 29/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TwitterProcessing.h"
#import "MGTwitterStatusesParser.h"
#import "MGTwitterXMLParser.h"
#import "WhereBox.h"
#import "QNavigatorAppDelegate.h"
#import "CustomTopBarView.h"
#import "CategoriesViewController.h"
#import "CategoryModelClass.h"
#import "AuditingClass.h"
#import "Constants.h"
#import "QNavigatorAppDelegate.h"

#import "SBJSON.h"
#import "ATIconDownloader.h"
#import "DBManager.h"
#import "StreetMallMapViewController.h"
#import "TellAFriendViewController.h"
#import "SA_OAuthTwitterEngine.h" 
#import"SA_OAuthTwitterController.h"
#import "ShopbeeFriendsViewController.h"
#import "MGTwitterEngine.h"
#import "TwitterFriendsViewController.h"
#import "AddFriendsMypageViewController.h"
#import "LoadingIndicatorView.h"



@implementation TwitterProcessing

@synthesize m_moreDetailView;
@synthesize m_tableView;
@synthesize m_theConnection;
@synthesize m_mallMapConnection;
@synthesize m_isConnectionActive;
@synthesize	m_isMallMapConnectionActive;
@synthesize m_activityIndicator;
@synthesize m_intResponseCode;

@synthesize m_shareView;
@synthesize m_scrollView;
@synthesize m_mutCatResponseData;
@synthesize twitterEngine, m_TwitterFrndsVC; 
@synthesize m_addFrndMypageVC;
@synthesize strUserId;
@synthesize m_TextViewData;

CategoryModelClass *l_objCatModel;
QNavigatorAppDelegate *l_appDelegate;
CLLocation *l_cllocation;
TwitterProcessing *Twitter_obj;

NSString *temp_strCity;
NSString *temp_strState;
NSString *temp_strZip;
NSString *temp_strAdd2;
NSArray *objarr;
int temp_var=0;

#pragma mark -
#pragma mark custom methods


-(IBAction)btnTellFriendByTwitterAction:(NSInteger)sender
{
	if(sender==5)
	{
				
		temp_var=5;
		if(!_engine)
		{  
			_engine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];  
			_engine.consumerKey    = kOAuthConsumerKey;  
			_engine.consumerSecret = kOAuthConsumerSecret;  
		}
		
		//	BOOL isEmpty = [ self validateIsEmpty:txtField.text];                                               
		if(![_engine isAuthorized])
		{  
			UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_engine delegate:self];  
			if (controller)
				[m_addFrndMypageVC presentModalViewController: controller animated: YES];
		}
		else 
		{
			NSMutableString *TweetBody;//=[[[NSMutableString alloc] init]autorelease];
			TweetBody=[NSMutableString stringWithFormat:@"%@",l_objCatModel.m_strBrandName];
			[TweetBody appendFormat:@"\n"];
			[TweetBody appendFormat:@"%@",l_objCatModel.m_strTitle];
			[TweetBody appendFormat:@"\n"];
			[TweetBody appendFormat:@"%@",l_objCatModel.m_strDiscount];
			[TweetBody appendFormat:@"\n"];
			[TweetBody appendFormat:@"%@",l_objCatModel.m_strAddress];
			
			//[requestDict setObject:@"direct_message" forKey:[twitterObj sendDirectMessage:@"this is test from iphone app" to:@"user_id"]];
		
			//[_engine sendDirectMessage:@"helloooooooooooooooooooooooooooo" to:@"106946019"];
		
			[_engine sendUpdate:TweetBody];
		}	
		
	}	
	else if(sender==6)
	{
		temp_var=6;
		if(!_engine)
		{  
			_engine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];  
			_engine.consumerKey    = kOAuthConsumerKey;  
			_engine.consumerSecret = kOAuthConsumerSecret;  
		}
		
		if(![_engine isAuthorized])
		{  
			UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_engine delegate:self];  
			if (controller)
			[m_addFrndMypageVC presentModalViewController: controller animated: YES];
		}
		else 
		{
			NSString *tempBodyString=[NSString stringWithFormat:@"%@\n%@",kTellAFriendMailSubject,kTellAFriendMailBody];
			[_engine sendUpdate:tempBodyString];

		}	
	}
	else
	{
			if(!_engine)
			{  
				_engine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];  
				_engine.consumerKey    = kOAuthConsumerKey;  
				_engine.consumerSecret = kOAuthConsumerSecret;  
			}
	
			if(![_engine isAuthorized])
			{  
				UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_engine delegate:self];  
				if (controller)
					[m_addFrndMypageVC presentModalViewController: controller animated: YES];
			}
			else 
			{
					NSLog(@"getFollowers   :  %@",[_engine getFollowersIncludingCurrentStatus:YES]);
			}
	}
	
}


-(void)btnInviteAction:(NSString *)str
{
	NSLog(@"m_TextViewData:%@",m_TextViewData);
	temp_var=7;
	if(!_engine)	
	{  
		_engine = [[SA_OAuthTwitterEngine alloc] initOAuthWithDelegate:self];  
		_engine.consumerKey    = kOAuthConsumerKey;  
		_engine.consumerSecret = kOAuthConsumerSecret;		
	}

	if(![_engine isAuthorized])
	{  
		UIViewController *controller = [SA_OAuthTwitterController controllerToEnterCredentialsWithTwitterEngine:_engine delegate:self];  
		if (controller)
		[m_addFrndMypageVC presentModalViewController: controller animated: YES];	
	}
	else 
	{
		[_engine sendDirectMessage:m_TextViewData to:str];
	}	

}

#pragma mark MGTwitterEngineDelegate



- (void)userInfoReceived:(NSArray *)userInfo forRequest:(NSString *)connectionIdentifier
{
	
	[m_addFrndMypageVC gotAllFriendsFromTwitter:userInfo];
	NSLog(@"Got user lists for %@: count: %d", userInfo,userInfo.count);
	
}

# pragma mark Twitter 
#pragma mark SA_OAuthTwitterEngineDelegate



- (void) storeCachedTwitterOAuthData: (NSString *) data forUsername: (NSString *) username
{
	NSUserDefaults	*defaults = [NSUserDefaults standardUserDefaults];
	NSUserDefaults *userDefaultU_IdTwitter=[NSUserDefaults standardUserDefaults];
	
	
	[defaults setObject: data forKey: @"authData"];
	
	NSLog(@"%@",data);
	NSLog(@"%@",defaults);
	NSArray *str22 = [data	componentsSeparatedByString:@"&"];
		
	for (int i = 0; i < [str22 count]; i++) {
		
		NSLog(@"%@",[str22 objectAtIndex:i]);
		if([[str22 objectAtIndex:i] rangeOfString:@"user_id="].location != NSNotFound)
		{
			strUserId = [[str22 objectAtIndex:i] stringByReplacingOccurrencesOfString:@"user_id=" withString:@""];//- (NSString *)stringByReplacingOccurrencesOfString:(NSString *)target withString:(NSString *)replacement;]
			NSLog(@"%@",strUserId);
		}
		[userDefaultU_IdTwitter setObject:strUserId forKey:@"twitterUid"];
}
	[defaults synchronize];
	
}

- (NSString *) cachedTwitterOAuthDataForUsername: (NSString *) username 
{
	return [[NSUserDefaults standardUserDefaults] objectForKey: @"authData"];
}

#pragma mark SA_OAuthTwitterController Delegate 

// following delegate methods are related with login process..
- (void) OAuthTwitterController: (SA_OAuthTwitterController *) controller authenticatedWithUsername: (NSString *) user_name {
	
	NSLog(@"Authenticated with user %@", user_name);
	
	if(temp_var==5)
	{
		//..temp_var=0;
		temp_var=5;
		NSMutableString *TweettBody;
		TweettBody=[NSMutableString stringWithFormat:@"%@",l_objCatModel.m_strBrandName];
		[TweettBody appendFormat:@"\n"];
		[TweettBody appendFormat:@"%@",l_objCatModel.m_strTitle];
		[TweettBody appendFormat:@"\n"];
		[TweettBody appendFormat:@"%@",l_objCatModel.m_strDiscount];
		[TweettBody appendFormat:@"\n"];
		[TweettBody appendFormat:@"%@",l_objCatModel.m_strAddress];
		[_engine sendUpdate:TweettBody];
				
	}
	else if(temp_var==6)//incase inviting friends from twitter (mypage)
	{
		temp_var=0;
		NSString *tempBodyString=[NSString stringWithFormat:@"%@\n%@",kTellAFriendMailSubject,kTellAFriendMailBody];
								  
		[_engine sendUpdate:tempBodyString];
	}
	else {
	[_engine getFollowersIncludingCurrentStatus:YES];	
	}

	[m_shareView setHidden:YES];
}

- (void) OAuthTwitterControllerFailed: (SA_OAuthTwitterController *) controller {
	
	NSLog(@"Authentication Failure");
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"login Unsuccesfull" message:@"login failed!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alert show];
	[alert release];
}

- (void) OAuthTwitterControllerCanceled: (SA_OAuthTwitterController *) controller {
	
	NSLog(@"Authentication Canceled");
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];
	
	[m_shareView setHidden:YES];
}

#pragma mark TwitterEngineDelegate

- (void) requestSucceeded: (NSString *) requestIdentifier {
	NSLog(@"Request %@ succeeded", requestIdentifier);
	
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];

	if (temp_var==5) 
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully posted." message: @"Tweet successfully posted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
		[alert release];
		
	}
	else if(temp_var==6)
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully posted." message: @"Tweet successfully posted to all followers with invitation text." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
		[alert release];
	}
	else if(temp_var==7)
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully posted." message: @"Message successfully posted with invitation text." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
		[alert release];

		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator stopLoadingView];

		
		[m_TwitterFrndsVC hideProcessing];
	}
	
}

- (void) requestFailed: (NSString *) requestIdentifier withError: (NSError *) error {
	NSLog(@"Request %@ failed with error: %@", requestIdentifier, error);
	
	LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
	[tempLoadingIndicator stopLoadingView];
	
	if (temp_var==5) 
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message: @"This sale is already posted as tweet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	else if(temp_var==6)
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message: @"The followers are already invited through tweet." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[alert release];
	}
	else if(temp_var==7)
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message: @"Invitation message has already sent to this user." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
		[alert release];
		
		
		
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator stopLoadingView];
		[m_TwitterFrndsVC hideProcessing];
	}
	else 
	{
		temp_var=0;
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message: @"Internal error occurred." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
		[alert release];
		
		LoadingIndicatorView *tempLoadingIndicator=[LoadingIndicatorView SharedInstance];
		[tempLoadingIndicator stopLoadingView];

		
	}
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void) dealloc {
    [m_moreDetailView release];
    [m_shareView release];
    [m_tableView release];
    [m_mallMapConnection release];
    [m_activityIndicator release];
    [m_mutCatResponseData release];
    [_engine release];
    [m_cancel release];
    [popupView release];
    [twitterEngine release];
    [m_TwitterFrndsVC release];
    [m_addFrndMypageVC release];
    [strUserId release];
    [m_view release];
    [m_indicatorView release];
    [m_TextViewData release];
    
    [super dealloc];
}

@end
