//
//  CategoriesViewController.m
//  QNavigator
//
//  Created by Soft Prodigy on 30/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CategoriesViewController.h"
#import "QNavigatorAppDelegate.h"
#import "CategoryModelClass.h"
#import "Constants.h"
#import "SBJSON.h"
#import "ATIconDownloader.h"
#import "DBManager.h"
#import "AuditingClass.h"
#import "StreetMallMapViewController.h"
#import "GrouponAsyncButtonView.h"
#import "ShopbeeAsyncButtonView.h"
#import "LoadingIndicatorView.h"
#import "ShopbeeAPIs.h"
#import "AsyncImageView.h"
#import "WhereBox.h"
#import "GrouponWhereBox.h"
#import "NewsDataManager.h"
// GDL
#import "UISearchBar+ContentInset.h"

BOOL l_isViewDisappeared;
CLLocation *l_cllocation;

@implementation CategoriesViewController

@synthesize nav;
@synthesize m_tableView;
@synthesize m_moreDetailView;
@synthesize m_lblCaption;
@synthesize m_strCaption;
@synthesize m_activityIndicator;
@synthesize m_mutCatResponseData;
@synthesize m_theConnection;
@synthesize m_mallMapConnection;
@synthesize m_isConnectionActive;
@synthesize m_pageNumber;
@synthesize m_intPrevCatIndex;
@synthesize m_locationManager;
@synthesize m_isMallMapConnectionActive;
@synthesize m_intResponseCode;
@synthesize m_scrollView;
@synthesize m_logosScrollView;
@synthesize m_searchBar;
@synthesize isCurrentListingView;
@synthesize m_exceptionPage;
@synthesize exceptionImageNamesArray;


NSMutableArray *l_arrBackupCategory;

UILabel *l_lblDistance;

QNavigatorAppDelegate *l_appDelegate;
CategoryModelClass *l_objCatModel;

int pageNumber;
int totalPages;

int l_searchResultIndex;

NSMutableArray *l_arrSearchResults;

NSString *temp_strCity;
NSString *temp_strState;
NSString *temp_strZip;
NSString *temp_strAdd2;

LoadingIndicatorView *l_catLoadingView;

#pragma mark -
#pragma mark reloading

// GDL: For reloading the category ads every 10 minutes.
- (void)reloadTimerFired:(NSTimer*)theTimer {
    static NSDate *lastReloadDate = nil;
    
    if ( lastReloadDate == nil || [lastReloadDate timeIntervalSinceNow] < - 9.5 * 60 ) {
        [self  sendRequestForCategoryData:1];
        [lastReloadDate release];
        lastReloadDate = [[NSDate alloc] init];
    }
}
 
#pragma mark -
#pragma mark UIView methods

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil 
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) 
	{
       //self.m_arrCategoryData=[[NSMutableArray alloc]init]; 
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
		m_intPrevCatIndex=-1;
		
		// Custom initialization
		
    }
	
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)viewWillAppear:(BOOL)animated
{	
   [super viewWillAppear:animated]; 
   
    [m_logosScrollView setHidden:NO];
	[m_tableView setHidden:YES];
	[m_exceptionPage setHidden:YES];
	isCurrentListingView=NO;
	l_appDelegate.m_SelectedMainPageClassObj=self;
	l_appDelegate.m_intCurrentView=3;
    
/*	
    // GDL: Removed so that we can reload data on a timer below.
	//send request to load data when session resumes after expiration
	if(l_appDelegate.m_isDataReloadOnSessionExpire==YES && l_appDelegate.m_arrCategorySalesData.count==0)
	{
		l_appDelegate.m_isDataReloadOnSessionExpire=NO;
		[self  sendRequestForCategoryData:1];	
		
	}
*/
	
    // GDL: Set up a timer that fires every minute to reload the category data.
    [reloadTimer invalidate];
    [reloadTimer release];
	reloadTimer = [[NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(reloadTimerFired:) userInfo:nil repeats:YES] retain];
    
    // GDL: Reload the category data manually once.
    [self  reloadTimerFired:nil];	
}

- (void)viewWillDisappear:(BOOL)animated
{
    // GDL: Cancel the reloadData timer.
    [reloadTimer invalidate];
    [reloadTimer release];
    reloadTimer = nil;
    
	ATIconDownloader *iconDownloader = [[[ATIconDownloader alloc] init] autorelease];
	BOOL temp_response=[iconDownloader closeAllActiveConnections];
	NSLog(@"cancel image download on disappear: %d",temp_response);
	[self updateDBonOtherViewNavigation];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	exceptionImageNamesArray = [[NSArray alloc] initWithObjects:
		@"exception_alarm clock",
		@"exception_entertainment",
		@"exception_just not yet",
		@"exception_search universe",
		@"exception_team sport", nil];

	 m_isConnectionActive=FALSE;
	self.m_locationManager = [[CLLocationManager alloc] init];
	self.m_locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
	self.m_locationManager.distanceFilter = 80.0;
	
	m_tableView.delegate=self;
	m_tableView.dataSource=self;
	
	/* search bar background setting */
	
	/* Bharat: Comment out search bars on sales page, and replace with grey bar 
	 	(to expunge a source of bugs we cannot fix related to search) 
    	Bharat: 11/8/2011: extending width of search bar to beyond display region by 100 pixels
     	and then will set the contentInset to non-displayable region, thus masking the search bar.
		Resetting the width back to 320 will make the search-bar appear as before.	
	 */
	//m_searchBar.frame=CGRectMake(0, 99, 320, 32);
    m_searchBar.frame=CGRectMake(0, 91, 320+200, 32);
	 UITextField *searchField=nil;
	 NSUInteger numViews = [m_searchBar.subviews count];
	 for(int i = 0; i < numViews; i++) 
	 {
		 
		 [[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		 
		 if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		 {
			 searchField = [m_searchBar.subviews objectAtIndex:i];
		 }
	 }
	
	if(!(searchField == nil)) 
	 {
		 [searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
		 [searchField setBorderStyle:UITextBorderStyleNone];
	 }
	 
	 
	 /***********************************/
		
	//Bharat : 11/19/11: expanding by 8 pixels
	//m_moreDetailView=[[UIView alloc]initWithFrame:CGRectMake(8, 135, 305,230)]; //13,142,295,195)];
	m_moreDetailView=[[UIView alloc]initWithFrame:CGRectMake(8, 127, 305,238)]; //13,142,295,195)];
	m_moreDetailView.backgroundColor=[UIColor colorWithRed:0 green:.51f blue:.57f alpha:1.0];
	
	//Bharat: 11/9/2011: Changing font of caption to GillSans-Bold
	m_lblCaption.font=[UIFont fontWithName:kFontName size:16];
	//m_lblCaption.font=[UIFont fontWithName:kGillSansFont size:16];
	[m_lblCaption setText:m_strCaption];
	
	m_mutCatResponseData=[[NSMutableData alloc]init];
	l_arrSearchResults=[[NSMutableArray alloc]init];
	

	
	self.tabBarController.selectedIndex=0;
	pageNumber=1;//initialize the page number
	
	
	l_catLoadingView=[LoadingIndicatorView SharedInstance];
	
	[self addLogosButtonViews];
	
	
    
    //Bharat: 11/8/2011: setting the contentInset to (frameWidth - 100)
    // If frameWidth is greater than (320+100), then search bar will be out of visible area to the right.
	//[m_searchBar setContentInset:UIEdgeInsetsMake(5, 220, 5, 5)];
    [m_searchBar setContentInset:UIEdgeInsetsMake(5, m_searchBar.frame.size.width-100, 5, 5)]; // outside

	[m_searchBar setText:@""];
	
	//Bharat: 11/9/2011: Let Caption field occupy full width relative to the width of the m_searchBar object.
	// This will help defer auto-shrink of font-size till limit exceeds furthure
	CGRect aFrame = m_lblCaption.frame;
	aFrame.size.width = m_searchBar.frame.size.width-200;
	m_lblCaption.frame = aFrame;
	
	
	//m_searchBar.tintColor=[UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0];
		
}
//implemented to enable the canel button all the time

- (void)enableCancelButton:(UISearchBar *)aSearchBar {
    for (id subview in [aSearchBar subviews]) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [subview setEnabled:TRUE];
        }
    }  
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark Custom Methods
//called in case of logos view
-(void)whereBtnPressed:(int)tagg prev_image:(UIImage *)prev_image
{
	if (isCurrentListingView==YES)
	{
		l_objCatModel= [l_arrSearchResults objectAtIndex:tagg];
	}
	else
	{
		if ((l_appDelegate.m_strCategoryName != nil) && 
				(([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kDining] == NSOrderedSame)  
				 ) ) {
			GrouponWhereBox *whereBox=[[GrouponWhereBox alloc] initWithNibName:@"GrouponWhereBox" bundle:nil];
			whereBox.m_productImage=prev_image;
			whereBox.tagg=tagg;
			[self.navigationController pushViewController:whereBox animated:YES];
			[whereBox release];
			whereBox=nil;
		} else {
			WhereBox *whereBox=[[WhereBox alloc] init];
			whereBox.m_productImage=prev_image;
			whereBox.tagg=tagg;
			[self.navigationController pushViewController:whereBox animated:YES];
			[whereBox release];
			whereBox=nil;
		}
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromLeft;
		animation.duration = 0.3;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
		[CATransaction commit];
		
	}
}

-(void)btnCloseAction:(id)sender
{
	[m_moreDetailView removeFromSuperview];
	//[m_tableView setUserInteractionEnabled:YES];
	
	l_appDelegate.m_intCategorySalesIndex=-1;
	
	[self.tabBarController.tabBar setUserInteractionEnabled:TRUE];
}

-(void)btnMapItAction:(id)sender
{
	[self performSelector:@selector(checkForMallMapExistence)];
	//StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
//	[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
//	[temp_streetMapViewCtrl release];
//	temp_streetMapViewCtrl=nil;
//	
//	[CATransaction begin];
//	CATransition *animation = [CATransition animation];
//	animation.type = kCATransitionFromLeft;
//	animation.duration = 0.6;
//	//animation.delegate=self;
//	[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
//	[[l_appDelegate.m_customView layer] addAnimation:animation forKey:@"Slide"];
//	[l_appDelegate.m_customView setHidden:YES];
//	[CATransaction commit];
}

#pragma mark -
#pragma mark Web Service methods

- (void)checkIfDataAvailable {
	
	DBManager *temp_dbObj = [DBManager SharedInstance];
	
	BOOL temp_isDataAlreadyInserted=[temp_dbObj checkForAlreadyInsertedData:m_intPrevCatIndex];
	
	if (!temp_isDataAlreadyInserted)
	{
		[temp_dbObj insertCategoryRecord:m_intPrevCatIndex];
	}
	
	m_intPrevCatIndex=l_appDelegate.m_intCategoryIndex;
	
	l_appDelegate = (QNavigatorAppDelegate *)[[UIApplication sharedApplication] delegate];
	
    /*
	if ( l_appDelegate.m_arrCategorySalesData ) {
		[l_appDelegate.m_arrCategorySalesData release];
		l_appDelegate.m_arrCategorySalesData = nil;
	}
	*/
    
    l_appDelegate.m_arrCategorySalesData = [[NSMutableArray alloc] initWithCapacity:3];
    [l_appDelegate.m_arrCategorySalesData addObjectsFromArray:[temp_dbObj GetCategoriesData:l_appDelegate.m_intCategoryIndex]];
    
	if ( l_appDelegate.m_arrCategorySalesData.count > 0 ) {
		if ( [m_moreDetailView superview] ) {
			[m_moreDetailView removeFromSuperview];
			//[m_tableView setUserInteractionEnabled:YES];
			
			l_appDelegate.m_intCategorySalesIndex=-1;//tracks the selected item in specific category. used on street map and mall map views
			
			[self.tabBarController.tabBar setUserInteractionEnabled:TRUE];
			
		}
		
		if ( m_isConnectionActive ) {
			[m_theConnection cancel];
			[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
			[m_activityIndicator stopAnimating];
			[self.view setUserInteractionEnabled:TRUE];
			m_isConnectionActive=FALSE;
		}
		//[m_tableView reloadData];
		
		//if (l_appDelegate.m_arrCategorySalesData.count>0) {
//			[m_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
//		}
		
		//[self performSelector:@selector(loadImagesForOnscreenRows) withObject:nil afterDelay:0.5];	
		
		[self addLogosButtonViews];
		
	}
	else 
		[self  sendRequestForCategoryData:1];
}


//method to insert the records in db if user navigates to big sale or indy shops view from category sales views
-(void)updateDBonOtherViewNavigation
{
	DBManager *temp_dbObj=[DBManager SharedInstance];
	
	BOOL temp_isDataAlreadyInserted=[temp_dbObj checkForAlreadyInsertedData:m_intPrevCatIndex];
	
	if (!temp_isDataAlreadyInserted)
	{
		[temp_dbObj insertCategoryRecord:m_intPrevCatIndex];
	}
}

- (void) sendRequestForCategoryData:(int )temp_pageNumber {
	[l_appDelegate CheckInternetConnection];
	if ( l_appDelegate.m_internetWorking == 0 ) {//0: internet working
		//self.tabBarController.selectedIndex=0;
		if ( [m_moreDetailView superview] ) {
			[m_moreDetailView removeFromSuperview];
			//[m_tableView setUserInteractionEnabled:YES];
			
			l_appDelegate.m_intCategorySalesIndex=-1;//tracks selected item in specific category. used on street map and mall map views
			
			[self.tabBarController.tabBar setUserInteractionEnabled:TRUE];
			
		}
		[l_catLoadingView startLoadingView:self];
		
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		m_pageNumber=temp_pageNumber;// required..
				
		NSMutableString *temp_url = nil;
		NSMutableURLRequest * theRequest = nil;
		// Bharat: 11/28/11: Make a Groupon call in case if "Dining"
		if ((l_appDelegate.m_strCategoryName != nil) && 
				(([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kDining] == NSOrderedSame)  
				 ) ) {

			// Groupon deal params
			// largeImageUrl": "https://secure-assets.grouponcdn.com/images/site_images/1836/4219/Ride-The-Ducks_grouponicus2.jpg" -> 440Wx267H image
			// sidebarImageUrl": "https://secure-assets.grouponcdn.com/images/site_images/1836/4219/Ride-The-Ducks_grouponicus2_sidebar.jpg" -> 200Wx121H image
			// mediumImageUrl": "https://secure-assets.grouponcdn.com/images/site_images/1836/4219/Ride-The-Ducks_grouponicus2_profile.jpg" -> 100Wx61H image
			// smallImageUrl": "https://secure-assets.grouponcdn.com/images/site_images/1836/4219/Ride-The-Ducks_grouponicus2_square.jpg" -> 50Wx30H image
			// 
			// grid4ImageUrl": "https://secure-assets.grouponcdn.com/images/site_images/1836/4219/Ride-The-Ducks_grouponicus2_grid_4.jpg" -> 300Wx182H image
			// grid6ImageUrl": "https://secure-assets.grouponcdn.com/images/site_images/1836/4219/Ride-The-Ducks_grouponicus2_grid_6.jpg" -> 440Wx267H image

			// "title": "$28 for a Holiday Lights & Sights Tour for Two from Classic Cable Car Sightseeing (Up To $56 Value)",
			// "startAt": "2011-11-28T08:02:44Z"
			// "endAt": "2011-12-01T07:59:59Z",
			// "placementPriority": "nearby",
			// "id": "ride-the-ducks-sf-1",
			// "status": "open",
			// "isNowDeal": false,
			// "dealUrl": "http://www.groupon.com/deals/ride-the-ducks-sf-1"
			// "announcementTitle": "Half Off Holiday Cable-Car Tour for Two",
			// "grouponRating": 4.5,
			/*

			options": [{
				expiresAt": "2012-01-02T07:59:00Z",
				buyUrl": "https://www.groupon.com/deals/ride-the-ducks-sf-1/confirmation?pledge_id=1348529",
				value": {
					currencyCode": "USD",
					formattedAmount": "$56.00",
					amount": 5600
				}
				price": {
					currencyCode": "USD",
					formattedAmount": "$28.00",
					amount": 2800
				}
				discount": {
					currencyCode": "USD",
					formattedAmount": "$28.00",
					amount": 2800
				}
				id": 1348529,
				redeemedQuantity": null,
				discountPercent": 50,
				title": "Classic Cable Car Sightseeing ",

			 }]

			 */

			/*
			   SAMPLE GROUPON NOW API
			
			https://api.groupon.com/v2/now_deals.json?client_id=cc35475ee57e712759abdc2702c5ca9f7cfe5478&lat=41.896579&lng=-87.643583&category=fine-dining
			
			https://api.groupon.com/v2/now_deals.json?client_id=cc35475ee57e712759abdc2702c5ca9f7cfe5478&lat=41.896579&lng=-87.643583&category=fine-dining&show=	
			 */
			NSString * grouponCatName = @"food-drink";
			if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) {
				grouponCatName = @"beauty-spa";
			} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) {
				grouponCatName = @"health-fitness";
			} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) {
				grouponCatName = @"activities";
			}
			temp_url=[NSMutableString stringWithFormat:
				@"https://api.groupon.com/v2/now_deals.json?client_id=cc35475ee57e712759abdc2702c5ca9f7cfe5478&visitor_id=%@&lng=%f&lat=%f&limit=100&categories[]=%@",
				l_appDelegate.m_strDeviceId,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude,grouponCatName];
			
			[temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
			temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			NSLog(@"%@",temp_url);
			theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																	  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
			
			[theRequest setHTTPMethod:@"GET"];
		} else {
			temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=getAllActiveItemsByCategory&did=%@&lng=%f&lat=%f&num=%d&pagenum=%d&cat=%@&dist=50",kServerUrl,l_appDelegate.m_strDeviceId,l_appDelegate.m_geoLongitude,l_appDelegate.m_geoLatitude,20,temp_pageNumber,l_appDelegate.m_strCategoryName];
					
			[temp_url replaceOccurrencesOfString:@"(null)" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [temp_url length])];
			
			temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
			
			NSLog(@"%@",temp_url);
			
			theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																	  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
			
			NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"text/xml; charset=utf-8", @"Content-Type", nil];
			
			//[theRequest setHTTPBody:[temp_url dataUsingEncoding:NSUTF8StringEncoding]];
			[theRequest setHTTPShouldHandleCookies:YES];
			[theRequest setAllHTTPHeaderFields:headerFieldsDict];
			[theRequest setHTTPMethod:@"GET"];
		}
			
		if ( m_isConnectionActive )
			[m_theConnection cancel];
		
		m_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self] autorelease];
        
        // GDL: Not needed, because the connection automatically starts.
		//[m_theConnection start];
        
		m_isConnectionActive = TRUE;
		
		if ( m_theConnection )
			NSLog(@"Request sent to get data");
	}
	else {
		self.view.userInteractionEnabled=TRUE;
		[m_activityIndicator stopAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}
}

#pragma mark -
#pragma mark Connection response methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	
	if (connection == m_mallMapConnection) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		long long temp_length=[response expectedContentLength];
		
		[m_mallMapConnection cancel];
		m_isMallMapConnectionActive=FALSE;
		
		StreetMallMapViewController *temp_streetMapViewCtrl=[[StreetMallMapViewController alloc]init];
		
		if (temp_length ==0)
		{
			temp_streetMapViewCtrl.m_isShowMallMapTab=FALSE;	
		}
		else {
			temp_streetMapViewCtrl.m_isShowMallMapTab=TRUE;	
		}
		
		[self.navigationController pushViewController:temp_streetMapViewCtrl animated:YES];
		[temp_streetMapViewCtrl release];
		temp_streetMapViewCtrl=nil;
		
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromLeft;
		animation.duration = 0.6;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
		[CATransaction commit];
		
	}
	else {
		m_intResponseCode = [httpResponse statusCode];
		NSLog(@"%d",m_intResponseCode);
		[m_mutCatResponseData setLength:0];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[m_mutCatResponseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	[self.view setUserInteractionEnabled:TRUE];
	m_isConnectionActive=FALSE;
	[l_catLoadingView stopLoadingView];
	
	if ( m_intResponseCode == 401 ) {
		UIAlertView *temp_timeoutAlert = [[UIAlertView alloc]initWithTitle:@"Session timeout!" message:@"Session expired. Please login again to continue." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_timeoutAlert show];
		[temp_timeoutAlert release];
		temp_timeoutAlert=nil;
		
		[m_tableView reloadData];
		
		l_appDelegate.isUserLoggedInAfterLoggedOutState=FALSE;
		
		[l_appDelegate.m_objGetCurrentLocation showLoginScreenOnSessionExpire:self];
		
		
	}
	else if ( m_mutCatResponseData != nil && m_intResponseCode == 200 ) {	
		
		NSLog(@"%@",[[[NSString alloc]initWithData:m_mutCatResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		NSString *temp_string=[[NSString alloc]initWithData:m_mutCatResponseData encoding:NSUTF8StringEncoding];
		
		SBJSON *temp_objSBJson = [[SBJSON alloc]init];
			
		// GDL: We were adding the data to the array that already contained the data, so everything got duplicated.
		[l_appDelegate.m_arrCategorySalesData removeAllObjects];
			
		
		
		if ((l_appDelegate.m_strCategoryName != nil) && 
				(([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) || 
				 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kDining] == NSOrderedSame)  
				 ) ) {
			NSDictionary *temp_dictResponse = [temp_objSBJson objectWithString:temp_string];
			NSArray * nowDealsArray = [temp_dictResponse objectForKey:@"nowDeals"];

			NSString * grouponCatName = @"food-drink";
			if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) {
				grouponCatName = @"beauty-spa";
			} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) {
				grouponCatName = @"health-fitness";
			} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) {
				grouponCatName = @"activities";
			}
			BOOL categoryFoundShouldBreakNow = NO;

			if ((nowDealsArray) && ([nowDealsArray count] > 0)) {
				for (int i=0; i< [nowDealsArray count]; i++) {
					
					if (categoryFoundShouldBreakNow == YES)
						break;
					
					NSDictionary * dict = [nowDealsArray objectAtIndex:i];
					if ((dict != nil) && ([dict objectForKey:@"category"] != nil)) {
						NSString * catId = [[dict objectForKey:@"category"] objectForKey:@"id"];
						if ((catId) && ([catId caseInsensitiveCompare:grouponCatName] == NSOrderedSame)) {
							categoryFoundShouldBreakNow = YES;

                            CategoryModelClass * temp_objCatSales = nil;
							NSArray * dealsArray = [dict objectForKey:@"deals"];

							if ((dealsArray) && ([dealsArray count] > 0)) {
								for (int j=0; j < [dealsArray count]; j++) {
									NSDictionary * dataDict = [dealsArray objectAtIndex:j];
									if ((dataDict) && ([dataDict count] > 0)) {
										temp_objCatSales = [[CategoryModelClass alloc]init];
										
										temp_objCatSales.m_strStartRedemptionAt = [[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"startRedemptionAt"];
                                        
                                        if ((temp_objCatSales.m_strStartRedemptionAt == nil) || 
												([temp_objCatSales.m_strStartRedemptionAt isKindOfClass:[NSString class]] != YES))
                                            temp_objCatSales.m_strStartRedemptionAt = @"";

										temp_objCatSales.m_strEndRedemptionAt = [[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"endRedemptionAt"];
                                        if ((temp_objCatSales.m_strEndRedemptionAt == nil) ||
												([temp_objCatSales.m_strEndRedemptionAt isKindOfClass:[NSString class]] != YES))
                                            temp_objCatSales.m_strEndRedemptionAt = @"";


										id latId = [[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"lat"];

										id lngId = [[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"lng"];

										NSLog(@"Class of lat/lng object=[%@,%@] value=[%@,%@]",[latId class], [lngId class],
												[NSString stringWithFormat:@"%f",[latId floatValue]],
												[NSString stringWithFormat:@"%f",[lngId floatValue]]);

										temp_objCatSales.m_strLatitude = [NSString stringWithFormat:@"%f",[latId floatValue]]; 
										temp_objCatSales.m_strLongitude = [NSString stringWithFormat:@"%f", [lngId floatValue]];
										
										// ????
										//temp_objCatSales.m_strStoreHours=TBD
										temp_objCatSales.m_strDistance=  [NSString stringWithFormat:@"%f",
											[[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"distanceFromOriginInMiles"] floatValue]];
										temp_objCatSales.m_strPhoneNo= [[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"phoneNumber"];
										
										temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@ %@, %@, %@ %@",
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"streetAddress1"],
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"streetAddress2"],
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"city"],
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"state"],
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"postalCode"]];
										
											
										temp_strCity=
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"city"];
										temp_strState=
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"state"];
										temp_strZip=
											[[[[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"redemptionLocations"] objectAtIndex:0] objectForKey:@"postalCode"];
										
																		
										temp_objCatSales.m_strMallImageUrl=[dataDict objectForKey:@"largeImageUrl"];
										temp_objCatSales.m_strDealUrl=[dataDict objectForKey:@"dealUrl"];
                                        if ((temp_objCatSales.m_strDealUrl == nil) || 
												([temp_objCatSales.m_strDealUrl isKindOfClass:[NSString class]] != YES))
                                            temp_objCatSales.m_strDealUrl = @"";
										temp_objCatSales.m_strTitle=[dataDict objectForKey:@"title"];
										temp_objCatSales.m_strDiscount=[dataDict objectForKey:@"subtitle"];

										NSMutableString * mutString = [[NSMutableString alloc] init];
										NSArray * strArr = [[[dataDict objectForKey:@"options"] objectAtIndex:0] 
											objectForKey:@"details"];
										for (id aObj in strArr) {
											if ([aObj objectForKey:@"description"] != nil) {
												[mutString appendString:[aObj objectForKey:@"description"]];
												[mutString appendString:@" "];
											}
										}
										temp_objCatSales.m_strOverview=[NSString stringWithFormat:@"%@",mutString];
										[mutString release];
										
											temp_objCatSales.m_strStoreWebsite=[[dataDict objectForKey:@"merchant"] objectForKey:@"websiteUrl"];
                                        if ((temp_objCatSales.m_strStoreWebsite == nil) ||
												([temp_objCatSales.m_strStoreWebsite isKindOfClass:[NSString class]] != YES))
                                            temp_objCatSales.m_strStoreWebsite = @"";
										temp_objCatSales.m_strProductName=[[dataDict objectForKey:@"merchant"] objectForKey:@"name"];
										temp_objCatSales.m_strBrandName=[[dataDict objectForKey:@"merchant"] objectForKey:@"name"];
										temp_objCatSales.m_strDescription=[dataDict objectForKey:@"announcementTitle"];
										
										temp_objCatSales.m_strId=[dataDict objectForKey:@"id"];
										
										NSLog(@"%@",temp_objCatSales.m_strId);
																
										temp_objCatSales.m_strImageUrl=[dataDict objectForKey:@"mediumImageUrl"];
										temp_objCatSales.m_strImageUrl2=[dataDict objectForKey:@"largeImageUrl"];

										NSLog(@"%@->loc[%@,%@]",temp_objCatSales.m_strProductName, temp_objCatSales.m_strLatitude, temp_objCatSales.m_strLongitude);
										
										
										[l_appDelegate.m_arrCategorySalesData addObject:temp_objCatSales];
										
										[temp_objCatSales release];
										temp_objCatSales=nil;

									}
								}
							}
							
						}
					}
				}
			}
		} else {
			NSArray *temp_arrResponse = [[NSArray alloc]initWithArray:[temp_objSBJson objectWithString:temp_string]];
			
			CategoryModelClass *temp_objCatSales;
			
			NSDictionary *temp_dictCat;
			
			for ( int i = 0; i < [temp_arrResponse count]; i++ ) {
				temp_dictCat = [temp_arrResponse objectAtIndex:i];
				//NSLog(@"%@",temp_dictCat);
				temp_objCatSales = [[CategoryModelClass alloc]init];
				temp_objCatSales.m_strLatitude = [[temp_dictCat objectForKey:@"location"] objectForKey:@"latitude"];
				temp_objCatSales.m_strLongitude = [[temp_dictCat objectForKey:@"location"] objectForKey:@"longitude"];
				temp_objCatSales.m_strMallImageUrl = [NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictCat objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]];
				
				//reuse distance for street name to be shown on category rows : address1 will be used
				temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"distance"];
				//temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"];
				
				temp_objCatSales.m_strStoreHours=[[temp_dictCat objectForKey:@"location"] objectForKey:@"storeHours"];
				temp_objCatSales.m_strPhoneNo=[[temp_dictCat objectForKey:@"location"] objectForKey:@"phoneNumber"];
				
				temp_strAdd2=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address2"];
				
				if ( temp_strAdd2.length > 0 )
					temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"],temp_strAdd2];
				else
					temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"]];
				
					
				temp_strCity=[[temp_dictCat objectForKey:@"location"] objectForKey:@"city"];
				temp_strState=[[temp_dictCat objectForKey:@"location"] objectForKey:@"state"];
				temp_strZip=[[temp_dictCat objectForKey:@"location"] objectForKey:@"zip"];
				
				if ( temp_strCity.length > 0 )
					temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strCity];
				
				if ( temp_strState.length > 0 )
					temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strState];
				
				if ( temp_strZip.length > 0 )
					temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strZip];
												
				temp_objCatSales.m_strTitle=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productTagLine"];
				temp_objCatSales.m_strDiscount=[[temp_dictCat objectForKey:@"product"] objectForKey:@"discountInfo"];
				temp_objCatSales.m_strOverview=[[temp_dictCat objectForKey:@"product"] objectForKey:@"termsAndConditions"];
				temp_objCatSales.m_strProductName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productName"];
				temp_objCatSales.m_strBrandName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"brandName"];
				temp_objCatSales.m_strDescription=[[temp_dictCat objectForKey:@"product"] objectForKey:@"description"];
				
				temp_objCatSales.m_strId=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"product"] objectForKey:@"id"]];
				
				NSLog(@"%@",temp_objCatSales.m_strId);
										
				NSArray *temp_arrImgAdUrls=[[temp_dictCat objectForKey:@"product"] objectForKey:@"imageUrls"];
				NSString *temp_str=@"";
				if ( temp_arrImgAdUrls.count > 0 ) {
					temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:0]];
					temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
					NSLog(@"%@",temp_str);
					temp_objCatSales.m_strImageUrl=temp_str;
					
					if ( temp_arrImgAdUrls.count >= 2 ) {
						NSString * tStrTest = [temp_arrImgAdUrls objectAtIndex:1];
						if ((tStrTest != nil) && ([tStrTest length] > 1)) {
							temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:1]];
							temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
							NSLog(@"Cat sale image url2: %@",temp_str);
							temp_objCatSales.m_strImageUrl2=temp_str;
						} else {
							temp_objCatSales.m_strImageUrl2=nil;
						}
					}

					if ( temp_arrImgAdUrls.count >= 3 ) {
						NSString * tStrTest = [temp_arrImgAdUrls objectAtIndex:2];
						if ((tStrTest != nil) && ([tStrTest length] > 1)) {
							temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:2]];
							temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
							NSLog(@"Cat sale image url3: %@",temp_str);
							temp_objCatSales.m_strImageUrl3=temp_str;
						} else {
							temp_objCatSales.m_strImageUrl3=nil;
						}
					}
				}
				
				[l_appDelegate.m_arrCategorySalesData addObject:temp_objCatSales];
				
				[temp_objCatSales release];
				temp_objCatSales=nil;
			}
			
			[temp_arrResponse release];
		//NSLog(@"cat Index: %d, count: %d",l_appDelegate.m_intCategoryIndex,l_appDelegate.m_arrCategorySalesData.count);
		}
		
		[temp_objSBJson release];
		[temp_string release];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		[self.view setUserInteractionEnabled:TRUE];
		m_isConnectionActive = FALSE;
		
		[self addLogosButtonViews];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[m_activityIndicator stopAnimating];
	m_isConnectionActive=FALSE;
	m_isMallMapConnectionActive=FALSE;
	[l_catLoadingView stopLoadingView];
	
	self.view.userInteractionEnabled=TRUE;
	[m_tableView reloadData];
	
	UIAlertView *networkDownAlert=[[UIAlertView alloc]initWithTitle:kNetworkDownErrorTitle message:kNetworkDownErrorMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[networkDownAlert show];
	[networkDownAlert release];
	networkDownAlert=nil;
	
	//inform the user
    //NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
	//NSLog(@"Exit :didFailWithError");
}

#pragma mark -
#pragma mark Add Logos views

- (void)addLogosButtonViews {
	
	//[m_logosScrollView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"green_back.png"]]];
	
	NSLog(@"CategoriesViewController:addLogosButtonViews called");

	for ( UIView *tempView in [m_logosScrollView subviews] )
		[tempView removeFromSuperview];
	
	if ((l_appDelegate.m_arrCategorySalesData == nil) || (l_appDelegate.m_arrCategorySalesData.count <= 0)) {
		
		if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) {  
			[m_exceptionPage setImage:[UIImage imageNamed:[exceptionImageNamesArray objectAtIndex:2]]];
		} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) {  
			[m_exceptionPage setImage:[UIImage imageNamed:[exceptionImageNamesArray objectAtIndex:4]]];
		} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) {  
			[m_exceptionPage setImage:[UIImage imageNamed:[exceptionImageNamesArray objectAtIndex:1]]];
		} else if ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kDining] == NSOrderedSame) {  
			[m_exceptionPage setImage:[UIImage imageNamed:[exceptionImageNamesArray objectAtIndex:3]]];
		} else {
			[m_exceptionPage setImage:[UIImage imageNamed:[exceptionImageNamesArray objectAtIndex:0]]];
		}
		
		m_exceptionPage.hidden = NO;
		NSLog(@"CategoriesViewController:addLogosButtonViews SHOWING EXCEPTION PAGE");
		return;
	}

	CGFloat xShiftSpace = 4.0f;
	CGFloat yShiftSpace = 0.0f;
	int numberOfButtonsInARow = 4;
	CGFloat height = 73.0f, width=73.0f;
	CGFloat divideGap = 0.0f;

	if ((l_appDelegate.m_strCategoryName != nil) && 
			(([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) || 
			 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) || 
			 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) || 
			 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kDining] == NSOrderedSame)  
			 ) ) {
		xShiftSpace = 2.0f;
		numberOfButtonsInARow = 2;
		height = 112.0f; width = 145.0f;
		divideGap = ((320.0 - xShiftSpace*2) - (width*numberOfButtonsInARow))/(numberOfButtonsInARow+1);
	} else {
		xShiftSpace = 2.0f;
		numberOfButtonsInARow = 2;
		height = 106.0f; width = 145.0f;
		divideGap = ((320.0 - xShiftSpace*2) - (width*numberOfButtonsInARow))/(numberOfButtonsInARow+1);
	}
	
	CGFloat x=xShiftSpace+divideGap, y=yShiftSpace+divideGap;
	m_logosScrollView.contentSize = CGSizeMake(m_logosScrollView.contentSize.width,100);
	m_exceptionPage.hidden = YES;
	m_logosScrollView.hidden = NO;
	if ( m_logosScrollView ) {
		for ( int i = 0; i < l_appDelegate.m_arrCategorySalesData.count; i++ ) {		
            if ( i%numberOfButtonsInARow == 0 && i != 0 ) {
                x=xShiftSpace+divideGap;
                y=y+height+divideGap;
                m_logosScrollView.contentSize = CGSizeMake(m_logosScrollView.contentSize.width,m_logosScrollView.contentSize.height+(height+2*divideGap));
            }
            else if ( i != 0 )
                x = x+width+divideGap;
            
            CategoryModelClass *tempCatObj = [l_appDelegate.m_arrCategorySalesData objectAtIndex:i];
            
            NSURL *temp_loadingUrl = [NSURL URLWithString:tempCatObj.m_strImageUrl];
            
			if ((l_appDelegate.m_strCategoryName != nil) && 
					(([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kHealthBeauty] == NSOrderedSame) || 
					 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kSports] == NSOrderedSame) || 
					 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kMovies] == NSOrderedSame) || 
					 ([l_appDelegate.m_strCategoryName caseInsensitiveCompare:kDining] == NSOrderedSame)  
					 ) ) {
            	GrouponAsyncButtonView * asyncButton = [[[GrouponAsyncButtonView alloc]
                                             initWithFrame:CGRectMake(x,y,width,height)] autorelease];
                asyncButton.tag = i;
                asyncButton.isCatView = TRUE;
                
                [asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(btnCategoryAction:) btnText:tempCatObj.m_strProductName];
                [m_logosScrollView  addSubview:asyncButton];
			} else {
            	ShopbeeAsyncButtonView * asyncButton = [[[ShopbeeAsyncButtonView alloc]
                                             initWithFrame:CGRectMake(x,y,width,height)] autorelease];
			
				asyncButton.tag = i;
				asyncButton.isCatView = TRUE;
				
				[asyncButton loadImageFromURL:temp_loadingUrl target:self action:@selector(btnCategoryAction:) btnText:tempCatObj.m_strDiscount];
				[m_logosScrollView  addSubview:asyncButton];
            }
		}	
	}
}

- (void)btnCategoryAction:(id)sender {
	if (sender != nil)
		[self whereBtnPressed:[sender tag] prev_image:(UIImage *)[sender backgroundImageForState:UIControlStateNormal]];
}



#pragma mark -
#pragma mark Check Mall Map Existence

-(void)checkForMallMapExistence {
	if ( m_isMallMapConnectionActive ) {
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		m_isMallMapConnectionActive = FALSE;
		[m_mallMapConnection cancel];
	}
	
	if ( l_appDelegate.m_intCurrentView == 3 ) { //represents other category sales view i.e. shoes, bags etc.
		if ( l_appDelegate.m_arrCategorySalesData.count > 0 && l_appDelegate.m_intCategorySalesIndex != -1 && isCurrentListingView == NO ) {
			CategoryModelClass *temp_objCatSales = [l_appDelegate.m_arrCategorySalesData objectAtIndex:l_appDelegate.m_intCategorySalesIndex];
			[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];			
		}
		else if ( l_arrSearchResults.count > 0 && l_searchResultIndex >= 0 && isCurrentListingView == TRUE ) {
			CategoryModelClass *temp_objCatSales=[l_arrSearchResults objectAtIndex:l_searchResultIndex];
			[self sendRequestToCheckMallMap:temp_objCatSales.m_strMallImageUrl];
		}
    }
}

-(void)sendRequestToCheckMallMap:(NSString *)imageUrl
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		[m_activityIndicator startAnimating];
		
		NSLog(@"%@",imageUrl);
		
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:imageUrl]
																  cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/text; charset=utf-8", @"Content-Type", nil];
		
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"GET"];
		
		if (m_isMallMapConnectionActive==TRUE)
		{
			[m_mallMapConnection cancel];
		}
		
		m_mallMapConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
				
		[m_mallMapConnection start];
		m_isMallMapConnectionActive=TRUE;
		
		if(m_mallMapConnection)
		{
			NSLog(@"Request sent to get data");
		}	
	}
	else
	{
		m_isMallMapConnectionActive=FALSE;
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		[m_activityIndicator stopAnimating];
		
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
		
	}
}

#pragma mark -
#pragma mark Table View Methods



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	return 85.0;
	
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
	if(pageNumber < totalPages)
	{
		return l_arrSearchResults.count+1;
	}
	else 
	{
		return l_arrSearchResults.count;
	}


}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	UITableViewCell *newCell;
	
	NSLog(@"cellforRowAtIndexPath for row: %d",indexPath.row);
	
	NSString *strIdentifier= [NSString stringWithFormat:@"Cell %@",indexPath];
		
	if (indexPath.row < l_arrSearchResults.count)
	{
		l_objCatModel= [l_arrSearchResults objectAtIndex:indexPath.row];
		
		newCell=[tableView dequeueReusableCellWithIdentifier:strIdentifier];
		//add objects only if cell is nil (no need to add in case of resuable cell)
		if(newCell==nil)
		{
			newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strIdentifier]autorelease];
			newCell.selectionStyle = UITableViewCellSelectionStyleNone;	
			
			AsyncImageView *temp_imageView=[[[AsyncImageView alloc]initWithFrame:CGRectMake(5, 4, 70, 70)] autorelease];
			[temp_imageView setTag:50];
			[newCell.contentView addSubview:temp_imageView];

			//making Main Title for TableViewCell
			UILabel *temp_lblTitle=[[UILabel alloc]init];
			temp_lblTitle.frame=CGRectMake(82,6,200,25);
			temp_lblTitle.font=[UIFont fontWithName:kFontName size:16];
			temp_lblTitle.backgroundColor=[UIColor clearColor];
			//temp_lblTitle.text=l_objCatModel.m_strTitle;
			temp_lblTitle.tag=51;
			temp_lblTitle.textColor=[UIColor colorWithRed:0.33f green:0.33f blue:0.33f alpha:1.0];
			[newCell.contentView addSubview:temp_lblTitle];
			[temp_lblTitle release];
			
			//making Discount Title for TableViewCell
			UILabel *temp_lblDiscount=[[UILabel alloc]init];
			temp_lblDiscount.frame=CGRectMake(82,30,200,25);
			temp_lblDiscount.font=[UIFont fontWithName:kFontName size:16];
			temp_lblDiscount.backgroundColor=[UIColor clearColor];
			//temp_lblDiscount.text=l_objCatModel.m_strDiscount;
			temp_lblDiscount.tag=52;
			temp_lblDiscount.textColor=[UIColor redColor];
			[newCell.contentView addSubview:temp_lblDiscount];
			[temp_lblDiscount release];
			
			
			//making distance parameter for TableViewCell
			UILabel *temp_lblDistance=[[UILabel alloc]init];
			temp_lblDistance.frame=CGRectMake(82,50,138,25);
			temp_lblDistance.tag=53;
			temp_lblDistance.font=[UIFont fontWithName:kFontName size:13]; //kGillSansFont size:13];
			temp_lblDistance.backgroundColor=[UIColor clearColor];
			temp_lblDistance.textColor=[UIColor colorWithRed:0.62f green:0.62f blue:0.62f alpha:1.0];
			//temp_lblDistance.text=l_objCatModel.m_strDistance;
			[newCell.contentView addSubview:temp_lblDistance];
			[temp_lblDistance release];
						
		}
		else
		{
			AsyncImageView *asyncImage = (AsyncImageView *)[newCell.contentView viewWithTag:50];
			UIImageView *tempImageview=(UIImageView *)[asyncImage viewWithTag:101];
			[tempImageview setImage:nil];
			
			[(UILabel *)[newCell.contentView viewWithTag:51] setText:@""];
			[(UILabel *)[newCell.contentView viewWithTag:52] setText:@""];
		}
		
		//l_objCatModel= [l_arrSearchResults objectAtIndex:indexPath.row];
		
		//-------calculate distance-------
		l_cllocation=[[CLLocation alloc]initWithLatitude:[l_objCatModel.m_strLatitude floatValue] longitude:[l_objCatModel.m_strLongitude floatValue]];
		CLLocation *tempCurrentCL=[[CLLocation alloc] initWithLatitude:[l_appDelegate.m_objGetCurrentLocation m_latitude] longitude:[l_appDelegate.m_objGetCurrentLocation m_longitude]];
		
		float tempDistance=[l_cllocation distanceFromLocation:tempCurrentCL];
		
		if (tempDistance > 400) 
		{
			((UILabel *)[newCell.contentView viewWithTag:53]).text=[NSString stringWithFormat:@"%.2f miles away",tempDistance * 0.000622f];
		}
		else {
			((UILabel *)[newCell.contentView viewWithTag:53]).text=[NSString stringWithFormat:@"%.0f steps away",tempDistance];
		}
		
		[l_cllocation release];
		[tempCurrentCL release];
		
		//		//-----------------------------------------------------------------
		
	
		[(AsyncImageView *)[newCell.contentView viewWithTag:50] loadImageFromURL:[NSURL URLWithString:l_objCatModel.m_strImageUrl]];
		[(UILabel *)[newCell.contentView viewWithTag:51] setText:l_objCatModel.m_strTitle];
		[(UILabel *)[newCell.contentView viewWithTag:52] setText:l_objCatModel.m_strDiscount];
		
			
		newCell.backgroundView =[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"moreInfo.png"]]autorelease];
		
		
	}
	else if(indexPath.row == l_arrSearchResults.count)
	{
		newCell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LoadMoreCell"]autorelease];
		newCell.selectionStyle = UITableViewCellSelectionStyleNone;	
        UIImageView  *temp=[[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"main_bg_old.png"]]autorelease];
		[temp setFrame:newCell.frame];
        
		newCell.backgroundView =temp;		//making load more label for last cell
		UILabel *temp_lblLoadMore=[[UILabel alloc]init];
		temp_lblLoadMore.frame=CGRectMake(85,20,200,25);
		temp_lblLoadMore.font=[UIFont fontWithName:kFontName size:16];
		temp_lblLoadMore.backgroundColor=[UIColor clearColor];
		temp_lblLoadMore.text=@"Load more items..";
		[newCell.contentView addSubview:temp_lblLoadMore];
		[temp_lblLoadMore release];		
		
	}
	
	return newCell;
	
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if ( indexPath.row == l_appDelegate.m_arrCategorySalesData.count && ++pageNumber <= totalPages ) {
		[l_catLoadingView startLoadingView:self];
		
		ShopbeeAPIs	*tmp_request=[[ShopbeeAPIs alloc] init];
		[tmp_request getSearchResults:@selector(requestCallBackMethod:responseData:) tempTarget:self searchText:m_searchBar.text pageNo:[NSString stringWithFormat:@"%d",pageNumber]];
		[tmp_request release];
	}
	else if ( indexPath.row < l_appDelegate.m_arrCategorySalesData.count ) {
		WhereBox *whereBox=[[WhereBox alloc] init];
		whereBox.m_productImage= ((UIImageView *)[(AsyncImageView *)[[tableView cellForRowAtIndexPath:indexPath] viewWithTag:50] viewWithTag:101]).image ;
		
		whereBox.tagg=indexPath.row;
		[self.navigationController pushViewController:whereBox animated:YES];
		
		[CATransaction begin];
		CATransition *animation = [CATransition animation];
		animation.type = kCATransitionFromLeft;
		animation.duration = 0.3;
		//animation.delegate=self;
		[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
		[CATransaction commit];
		
		[whereBox release];
		whereBox=nil;
	}
	
//		UIImage *tempImage=((UIImageView *)[(AsyncImageView *)[[tableView cellForRowAtIndexPath:indexPath].contentView viewWithTag:50] viewWithTag:101]).image;
//		if (!tempImage) {
//			tempImage=[UIImage imageNamed:@"no-image"];
//		}
//		l_searchResultIndex=indexPath.row;
//		[self whereBtnPressed:indexPath.row prev_image:tempImage];
//		
//	}	
}

#pragma mark -
#pragma mark Memory Management Methods

- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

// GDL: Changed following two methods.

#pragma mark - memory management

- (void)viewDidUnload  {
    [super viewDidUnload];
    
    // GDL: Cancel the reloadData timer.
    [reloadTimer invalidate];
    [reloadTimer release];
    reloadTimer = nil;
    
    // Release retained IBOutlets.
    self.m_exceptionPage = nil;
    self.m_searchBar = nil;
    self.m_logosScrollView = nil;
    self.nav = nil;
    self.m_tableView = nil;
    self.m_lblCaption = nil;
    self.m_activityIndicator = nil;
}

- (void)dealloc  {
    
    // GDL: Cancel the reloadData timer.
    [reloadTimer invalidate];
    [reloadTimer release];
    reloadTimer = nil;
    
    [m_tableView release];
    [m_moreDetailView release];
    [m_lblCaption release];
    [m_strCaption release];
    [m_activityIndicator release];
    [m_mutCatResponseData release];
    
    // This was already released.
    //[m_theConnection release];
    
    // I expect this was already released.
    //[m_mallMapConnection release];
    
    [m_locationManager release];
    [m_scrollView release];
    [nav release];
    
    // Releasing this caused an error. I don't know why. It is an outlet.
    //[m_logosScrollView release];
    
    [m_searchBar release];
    [m_exceptionPage release];
    
	[super dealloc];
}

#pragma mark -
#pragma mark Search bar delegate methods

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
	isEditing=YES;
	/* search bar background setting */
	//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];
	UITextField *searchField;
	NSUInteger numViews = [m_searchBar.subviews count];
	for(int i = 0; i < numViews; i++) 
	{
		
		//[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
		
		if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
		{
			searchField = [m_searchBar.subviews objectAtIndex:i];
		}
	}
	
	if(!(searchField == nil)) 
	{
		[searchField setBackground: [UIImage imageNamed:@"gray_searchbox.png"] ];
		[searchField setBorderStyle:UITextBorderStyleNone];
	}
	/***********************************/
	
	[UIView beginAnimations:@"previousAd" context:nil];
	[UIView setAnimationDuration:0.4f];
	[UIView setAnimationBeginsFromCurrentState:YES];
	[m_searchBar setContentInset:UIEdgeInsetsMake(5, 5, 5, 5)];
	[m_searchBar setShowsCancelButton:YES];
	[m_lblCaption setHidden:YES];
	
	[m_moreDetailView removeFromSuperview];
	
	//[UIView set forView:m_searchBar cache:YES];
	//[UIView setAnimationTransition:UIViewAnimatio forView:m_imgViewSaleAd cache:YES];
	[UIView commitAnimations];
	return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{   
   /* {//search bar and exception page settings
    [searchBar setHidden:YES];//as we want to remove the search bar while searching
    [m_exceptionPage setFrame:CGRectMake(m_tableView.frame.origin.x, m_tableView.frame.origin.y-searchBar.frame.size.height, m_tableView.frame.size.width, m_tableView.frame.size.height+searchBar.frame.size.height)];
	
    }*/
    
    
    
    
    [l_arrSearchResults removeAllObjects];
	[m_tableView setHidden:YES];//changed to YES by z
    [m_exceptionPage setHighlighted:YES];//added by z
    [m_exceptionPage setHidden:NO];
	[m_logosScrollView setHidden:YES];
	isCurrentListingView=YES;
    [searchBar setShowsCancelButton:YES];
	
	pageNumber=1;
	
		
	[searchBar resignFirstResponder];
	
	//[l_catLoadingView startLoadingView:self];
	
	ShopbeeAPIs	*tmp_request=[[ShopbeeAPIs alloc] init];
	[tmp_request getSearchResults:@selector(requestCallBackMethod:responseData:) tempTarget:self searchText:searchBar.text pageNo:@"1"];
  
	[tmp_request release];
    [self enableCancelButton:searchBar];
	isEditing=NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{   
    
	//[m_tableView setHidden:YES];
	//[m_logosScrollView setHidden:NO];
	
	/* search bar background setting */
	//[m_searchBar setFrame:CGRectMake(0, 78, 320, 32)];

	[searchBar resignFirstResponder];
	if (!isEditing) {
        UITextField *searchField;
        NSUInteger numViews = [m_searchBar.subviews count];
        for(int i = 0; i < numViews; i++) 
        {
            
            //[[m_searchBar.subviews objectAtIndex:0] setHidden:YES];
            
            if([[m_searchBar.subviews objectAtIndex:i] isKindOfClass:[UITextField class]])
            {
                searchField = [m_searchBar.subviews objectAtIndex:i];
            }
        }
        
        if(!(searchField == nil)) 
        {
            [searchField setBackground: [UIImage imageNamed:@"search_bar_small.png"] ];
            [searchField setBorderStyle:UITextBorderStyleNone];
        }
        /***********************************/
        
        
        [m_searchBar setContentInset:UIEdgeInsetsMake(5, 220, 5, 5)];
        [m_searchBar setText:@""];
        
        [m_searchBar setShowsCancelButton:NO];
        [m_lblCaption setHidden:NO];
        [self viewWillAppear:YES];
    }
    isEditing=NO;
    [self enableCancelButton:searchBar];
	
    
}

-(void)requestCallBackMethod:(NSNumber *)responseCode responseData:(NSData *)responseData {
    
    
   /* {//search bar and exception page settings
        [m_searchBar setHidden:NO];//search bar should be visible in any case of result
        [m_exceptionPage setFrame:[m_tableView frame]];
    }*/
    [m_exceptionPage setHighlighted:NO];//added by z
    [m_exceptionPage setHidden:YES];
    [m_tableView setHidden:NO];//added by z
	[l_catLoadingView stopLoadingView];
	
	if ([responseCode intValue]==401)
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
		
		
	}
	else if(responseData!=nil && [responseCode intValue]==200)
	{	
		[m_tableView setHidden:NO];
		isCurrentListingView=YES;
		[m_logosScrollView setHidden:YES];
		 
		NSString *temp_string=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
		
		
		NSMutableArray *temp_arrResponse =[temp_string JSONValue];
		if (temp_arrResponse.count>0)
		{
			[m_exceptionPage setHidden:YES];
			[m_tableView setHidden:NO];
			
			totalPages=[[[temp_arrResponse objectAtIndex:0] valueForKey:@"noOfPages"] intValue];
			temp_arrResponse =[[temp_arrResponse objectAtIndex:0] valueForKey:@"searchData"];
		}
		else 
		{
			[m_exceptionPage setHidden:NO];
			[m_tableView setHidden:YES];
			//UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"No data found. Please try with different search text." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//			[tempAlert show];
//			[tempAlert release];
//			tempAlert=nil;
//							
			totalPages=0;
			if ([temp_arrResponse count]>0)
			{
				[temp_arrResponse removeAllObjects];
			}
					
		}

		CategoryModelClass *temp_objCatSales;
		
		NSDictionary *temp_dictCat;
		
		for (int i=0;i<[temp_arrResponse count]; i++)
		{
			temp_dictCat=[temp_arrResponse objectAtIndex:i];
			//NSLog(@"%@",temp_dictCat);
			temp_objCatSales=[[CategoryModelClass alloc]init];
			temp_objCatSales.m_strLatitude=[[temp_dictCat objectForKey:@"location"] objectForKey:@"latitude"];
			temp_objCatSales.m_strLongitude=[[temp_dictCat objectForKey:@"location"] objectForKey:@"longitude"];
			temp_objCatSales.m_strMallImageUrl=[NSString stringWithFormat:@"%@%@",kServerUrl,[[temp_dictCat objectForKey:@"location"] objectForKey:@"mallMapImageUrl"]];
			
			//reuse distance for street name to be shown on category rows : address1 will be used
			//temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"distance"];
			temp_objCatSales.m_strDistance=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"];
			
			temp_objCatSales.m_strStoreHours=[[temp_dictCat objectForKey:@"location"] objectForKey:@"storeHours"];
			temp_objCatSales.m_strPhoneNo=[[temp_dictCat objectForKey:@"location"] objectForKey:@"phoneNumber"];
			
			temp_strAdd2=[[temp_dictCat objectForKey:@"location"] objectForKey:@"address2"];
			
			if(temp_strAdd2.length>0)
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"],temp_strAdd2];
			else
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"location"] objectForKey:@"address1"]];
			
			
			temp_strCity=[[temp_dictCat objectForKey:@"location"] objectForKey:@"city"];
			temp_strState=[[temp_dictCat objectForKey:@"location"] objectForKey:@"state"];
			temp_strZip=[[temp_dictCat objectForKey:@"location"] objectForKey:@"zip"];
			
			if(temp_strCity.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strCity];
			}
			
			if(temp_strState.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strState];
			}
			
			if(temp_strZip.length>0)
			{
				temp_objCatSales.m_strAddress=[NSString stringWithFormat:@"%@, %@",temp_objCatSales.m_strAddress,temp_strZip];
			}
			
			temp_objCatSales.m_strTitle=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productTagLine"];
			temp_objCatSales.m_strDiscount=[[temp_dictCat objectForKey:@"product"] objectForKey:@"discountInfo"];
			temp_objCatSales.m_strOverview=[[temp_dictCat objectForKey:@"product"] objectForKey:@"termsAndConditions"];
			temp_objCatSales.m_strProductName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"productName"];
			temp_objCatSales.m_strBrandName=[[temp_dictCat objectForKey:@"product"] objectForKey:@"brandName"];
			temp_objCatSales.m_strDescription=[[temp_dictCat objectForKey:@"product"] objectForKey:@"description"];

											 //@"brandName"];
			
			temp_objCatSales.m_strId=[temp_dictCat valueForKey:@"advtlocid"];//[NSString stringWithFormat:@"%@",[[temp_dictCat objectForKey:@"product"] objectForKey:@"id"]];
			
			NSLog(@"%@",temp_objCatSales.m_strId);
			
			NSArray *temp_arrImgAdUrls=[[temp_dictCat objectForKey:@"product"] objectForKey:@"imageUrls"];
			NSString *temp_str=@"";
			if (temp_arrImgAdUrls.count>0)
			{
				temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:0]];
				temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				NSLog(@"%@",temp_str);
				temp_objCatSales.m_strImageUrl=temp_str;
				
				if(temp_arrImgAdUrls.count>=2)
				{
					NSString * tStrTest = [temp_arrImgAdUrls objectAtIndex:1];
					if ((tStrTest != nil) && ([tStrTest length] > 1)) {
						temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:1]];
						temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						NSLog(@"Cat sale image url2: %@",temp_str);
						temp_objCatSales.m_strImageUrl2=temp_str;
					} else {
						temp_objCatSales.m_strImageUrl2=nil;
					}
				}

				if(temp_arrImgAdUrls.count>=3)
				{
					NSString * tStrTest = [temp_arrImgAdUrls objectAtIndex:2];
					if ((tStrTest != nil) && ([tStrTest length] > 1)) {
						temp_str=[NSString stringWithFormat:@"%@%@",kServerUrl,[temp_arrImgAdUrls objectAtIndex:2]];
						temp_str=[temp_str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
						NSLog(@"Cat sale image url3: %@",temp_str);
						temp_objCatSales.m_strImageUrl3=temp_str;
						temp_objCatSales.m_strImageUrl3=temp_str;
					} else {
						temp_objCatSales.m_strImageUrl3=nil;
					}
				}
				
			}
			
			
			[l_arrSearchResults addObject:temp_objCatSales];
			
			[temp_objCatSales release];
			temp_objCatSales=nil;
			
		}
		
		//logic to interchange results data with category data because category array is used on many views
		//so better to replace category sales array content
		//replace only after first request else wrong data will be replaced on second request
		if ( pageNumber == 1 )
			l_arrBackupCategory = [NSMutableArray arrayWithArray:l_appDelegate.m_arrCategorySalesData];
		
		l_appDelegate.m_arrCategorySalesData = [l_arrSearchResults retain];
		
		//[temp_objCatSales release];
//		temp_objCatSales=nil;
		
		
		[m_tableView reloadData];
		
		
	}
	else if([responseCode intValue]!=-1)
	{
		UIAlertView *tempAlert=[[UIAlertView alloc]initWithTitle:@"Server error!" message:@"Internal error occurred. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[tempAlert show];
		[tempAlert release];
		tempAlert=nil;
	}
	
}



@end

