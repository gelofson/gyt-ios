//
//  GrouponBuyNowWebViewController.m
//  QNavigator
//
//  Created by Bharat Biswal on 12/5/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GrouponBuyNowWebViewController.h"


@implementation GrouponBuyNowWebViewController

@synthesize grouponWebView;
@synthesize pageTitleLabel;
@synthesize urlString;
@synthesize titleString;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

	[grouponWebView  loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
	pageTitleLabel.text = titleString;

    /*
     Bharat: COMMENTED OUT ON 12Dec2011 with feedback from Greg
	UIAlertView * popupMesgAlert=[[UIAlertView alloc] initWithTitle:@"Note" 
		message:@"FashionGram happily suggests that you can access your Groupon purchases, anytime, by logging into your account on the Groupon mobile website. Selecting the print option from your mobile phone allows the barcode and number to be viewed onscreen." 
		delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[ popupMesgAlert show];
	[ popupMesgAlert release];
	*/

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction) backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction) grouponButtonClicked: (id) sender {
	NSLog(@"GrouponBuyNowWebViewController::grouponButtonClicked");
	NSString * gouponUrl = @"http://www.groupon.com";
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[gouponUrl 
        stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]]; 
}

@end
