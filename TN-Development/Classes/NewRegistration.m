//
//  NewRegistration.m
//  QNavigator
//
//  Created by softprodigy on 15/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import"NewRegistration.h"
#import"Constants.h"
#import"LoginViewController.h"
#import"QNavigatorAppDelegate.h"
#import"SBJSON.h"
#import"TOSViewController.h"
#import"JSON.h"
#import"LoadingIndicatorView.h"
#import"GLImage.h"
#import "PhotoUseCaseViewController.h"
#import "MessageBoardManager.h"
#import "NewsDataManager.h"

@implementation NewRegistration
@synthesize m_txtFirstName;
@synthesize m_txtLastName;
@synthesize m_txtEmail;
@synthesize m_txtPassword;
@synthesize m_MsButton;
@synthesize m_MrButton;
@synthesize m_intResponseCode;
@synthesize m_pickerView;
@synthesize m_toolbarWithDoneButton;
@synthesize m_ageGroupBtn;
@synthesize m_TakePictureBtn;
@synthesize m_homeViewObj;
@synthesize m_personData;
@synthesize m_nextView;
NSURLConnection *l_theConnection;
QNavigatorAppDelegate *l_appDelegate;
LoadingIndicatorView *l_indicatorView;
#pragma mark Custom Methods

-(IBAction) MrButtonSelected{
	m_MsButton.selected=NO;
	[m_MrButton setSelected:YES];	
	m_gender=@"M";
}
-(IBAction) MsButtonSelected{
	m_MrButton.selected=NO;
	[m_MsButton setSelected:YES];
	m_gender=@"F";
}



-(IBAction) btnCancelAction:(id)sender
{
	[self dismissViewControllerAnimated:YES completion:nil];
	
}

-(IBAction) btnSaveAction:(id)sender
{
	[self resignAllResponders];
	[self animateViewDownward];
	
	m_txtFirstName.text=[m_txtFirstName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	m_txtLastName.text=[m_txtLastName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	m_txtEmail.text=[m_txtEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	m_txtPassword.text=[m_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

	//NSLog(@"m_txtFirstName=[%@]",m_txtFirstName.text);
	//NSLog(@"m_txtLastName=[%@]",m_txtLastName.text);
	//NSLog(@"m_txtEmail=[%@]",m_txtEmail.text);
	//NSLog(@"m_txtPassword=[%@]",m_txtPassword.text);

	if(m_txtFirstName.text.length==0 || m_txtLastName.text.length==0 || m_txtEmail.text.length==0 || m_txtPassword.text.length==0 )
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input required fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}
	else if(m_txtPassword.text.length<3)
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Minimum 3 characters required for password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}
	else if([m_txtEmail.text rangeOfString:@"@"].location==NSNotFound || [m_txtEmail.text rangeOfString:@"."].location==NSNotFound)
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please input valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}
	/*else if(!([m_MrButton isSelected]||[m_MsButton isSelected]))
	{
		UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:@"Please select your gender." message:@"" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alertView show];
		[alertView release];
	}*/
	/*else if ([m_ageGroupBtn titleForState:UIControlStateNormal].length<3) 
	{
		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select your age group." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[temp_alertView show];
		[temp_alertView release];
		temp_alertView=nil;
	}*/
	//else if([m_personData length]==0)
//	{
//		UIAlertView *temp_alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"Please select your profile picture." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[temp_alertView show];
//		[temp_alertView release];
//		temp_alertView=nil;
//		
//	}

	else
	{
		//[l_indicatorView startLoadingView:self];
		[l_indicatorView performSelectorInBackground:@selector(startLoadingView:) withObject:self];
		[self sendRequestForRegistration:0];
	}
}
-(IBAction) btnTakePictureAction
{
	[m_txtPassword resignFirstResponder];
	[m_txtFirstName resignFirstResponder];
	[m_txtEmail resignFirstResponder];
	[m_txtLastName resignFirstResponder];
	
	[self animateViewDownward];
	
	
	PhotoUseCaseViewController *PhotoUseCaseObj=[[PhotoUseCaseViewController alloc] initWithNibName:@"PhotoUseCaseViewController" bundle:nil];
	[PhotoUseCaseObj setHidesBottomBarWhenPushed:YES];
	PhotoUseCaseObj.m_newRegistration=self;
    [self presentViewController:PhotoUseCaseObj animated:NO completion:nil];
	//[self.navigationController pushViewController:PhotoUseCaseObj animated:YES];
	//[self.navigationController presentModalViewController:PhotoUseCaseObj animated:YES];
	//[self.view addSubview:PhotoUseCaseObj.view];
	[PhotoUseCaseObj release];
	PhotoUseCaseObj=nil;
	
	
	//UIActionSheet *tmp_sheet=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Skip, I will do it later" 
//										  destructiveButtonTitle:nil otherButtonTitles:@"Take Profile Picture",@"Choose Existing Photo",nil];
//	[tmp_sheet showInView:self.view];
//	[tmp_sheet release];
}

-(IBAction) btnPrivacyStatementAction:(id)sender
{
	TOSViewController *temp_tosViewCtrl=[[TOSViewController alloc]init];
	temp_tosViewCtrl.m_strTypeOfData=@"1";
    [self presentViewController:temp_tosViewCtrl animated:YES completion:nil];
	[temp_tosViewCtrl release];
	temp_tosViewCtrl=nil;
}
-(IBAction) btnTOSAction:(id)sender
{
	TOSViewController *temp_tosViewCtrl=[[TOSViewController alloc]init];
	temp_tosViewCtrl.m_strTypeOfData=@"0";
    [self presentViewController:temp_tosViewCtrl animated:YES completion:nil];
	[temp_tosViewCtrl release];
	temp_tosViewCtrl=nil;
}
-(void)resignAllResponders
{
	[m_txtFirstName resignFirstResponder];
	[m_txtLastName resignFirstResponder];
	[m_txtEmail resignFirstResponder];
	[m_txtPassword resignFirstResponder];
}
-(IBAction) btnAgeGroupAction{
	[m_txtFirstName resignFirstResponder];
	[m_txtLastName resignFirstResponder];
	[m_txtEmail resignFirstResponder];
	[m_txtPassword resignFirstResponder];
	[self animateViewDownward];
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
	[m_pickerView setFrame:CGRectMake(0, 300, 320, 216)];
	[m_toolbarWithDoneButton setFrame:CGRectMake(0, 260, 320, 44)];
	[self.view addSubview:m_pickerView];
	[UIView commitAnimations];	
}
-(IBAction) btnDoneAction{
	
	[m_ageGroupBtn setTitle:[m_agesArray objectAtIndex:[m_pickerView selectedRowInComponent:0]] forState:UIControlStateNormal];
    
	m_ageGroup=[m_agesArray objectAtIndex:[m_pickerView selectedRowInComponent:0]];
	
	if ([m_ageGroup isEqualToString:@"35+"]) 
	{
		m_ageGroup=@"35%2B";
	}
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.3];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
	[m_pickerView setFrame:CGRectMake(0, screenRect.size.height - 20, 320, 216)];
	[m_toolbarWithDoneButton setFrame:CGRectMake(0, screenRect.size.height, 320, 44)];
	[self.view addSubview:m_pickerView];
	[UIView commitAnimations];
}
-(IBAction) m_ShowActivityIndicator{
	
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    
	m_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, screenRect.size.height-20)];
	m_view.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
	
	m_indicatorView=[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(140,200 , 30, 30)];
	
	[m_view addSubview:m_indicatorView];
	[self.view addSubview:m_view];
	[m_indicatorView startAnimating];
	
}
-(IBAction)btnGoAction:(id)sender
{
    [m_nextView setHidden:NO];
    [self.view addSubview:m_nextView];
    [m_txtEmail resignFirstResponder];
    [m_txtPassword  resignFirstResponder];
}
-(IBAction)btnNewCancleAction:(id)sender
{
   //[m_nextView setHidden:YES]; 
    [m_nextView removeFromSuperview];
}

	

#pragma mark picker view methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
	return 1;	
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
	
	return [m_agesArray count];
}
- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	return [m_agesArray objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	//m_ageGroupBtn.titleLabel.text=[m_agesArray objectAtIndex:row];
	[m_ageGroupBtn setTitle: [m_agesArray objectAtIndex:row] forState:UIControlStateNormal];
	m_ageGroup=[m_agesArray objectAtIndex:row];
	
	if ([m_ageGroup isEqualToString:@"35+"]) 
	{
		m_ageGroup=@"35%2B";
	}
	
	//NSLog(@"the label is %@",[m_agesArray objectAtIndex:row]);
	//[[NSUserDefaults standardUserDefaults]setInteger:row+1 forKey:@"level"];
	//[[NSUserDefaults standardUserDefaults]synchronize];
	
}
	
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
		
		return 300.0;
}
	
#pragma mark actionsheet methods
-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex==0) {
		if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			UIImagePickerController *tmp_picker=[[UIImagePickerController alloc]init];
			tmp_picker.sourceType=UIImagePickerControllerSourceTypeCamera ;
			tmp_picker.allowsEditing=NO;
			tmp_picker.delegate=self;
			[self presentViewController:tmp_picker animated:YES completion:nil];
			[tmp_picker release];
			NSLog(@"button index is %i",buttonIndex);
		}
		else {
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No camera device found." message:nil delegate:self 
											   cancelButtonTitle:@"OK" otherButtonTitles:nil ];
			[alert show];
			[alert release];
		}
	}
	
	else if(buttonIndex==1){
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
			
			
			NSLog(@"button index is %i",buttonIndex);
			UIImagePickerController *picker=[[UIImagePickerController  alloc] init];
			picker.delegate=self;
			picker.sourceType=UIImagePickerControllerSourceTypeSavedPhotosAlbum;
			[self presentViewController:picker animated:YES completion:nil];
			[picker release];
		}
	}
	else if(buttonIndex==3)
	{
	
	}
}
#pragma mark Text field delegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if(textField==m_txtFirstName || textField==m_txtLastName || textField==m_txtEmail)
	{
		if (textField.text.length>=50 && range.length==0)
			return NO;
	}
	else if(textField==m_txtPassword)
	{
		if (textField.text.length>=20 && range.length==0)
			return NO;
		
	}
	
	
	return YES;
	
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
	
//	if( textField==m_txtPassword)
//	{
//		[self animateViewUpward];
//	}
	

    
	
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	//pass the control to next text field when Next button is tapped
	if (textField==m_txtFirstName)
    {
		[m_txtLastName becomeFirstResponder];
        [self animateViewUpward];
    }
	else if(textField==m_txtLastName)
    {
		[textField resignFirstResponder];
        [self animateViewDownward];
    }
	else if(textField==m_txtEmail){
		[m_txtPassword becomeFirstResponder];
		[self animateViewUpward];
	}
	else
	{
		[textField resignFirstResponder];
		[self animateViewDownward];
	}
    
    
	
	return YES;
}
//#pragma mark image picker methods
//-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
//{
//	[self dismissModalViewControllerAnimated:YES];
//}
//
//-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//{
//	UIImage *tempImage=[image scaledDownImageWithMaximumSize:CGSizeMake(320,320)];
//	
//	if (m_personData) {
//		[m_personData release];
//		m_personData=nil;
//	}
//	m_personData=UIImageJPEGRepresentation(tempImage,0.0);
//	[m_personData retain];
//	
//	tempImage=[UIImage imageWithData:m_personData];	
//		
//	[m_TakePictureBtn setBackgroundImage:tempImage forState:UIControlStateNormal];
//	
//	m_personImage=tempImage;
//		
//	[self dismissModalViewControllerAnimated:YES];
//}

#pragma mark sendRequestForRegistration

-(void)sendRequestForRegistration:(int )ignoreSaleData //0: no, 1: yes
{
	[l_appDelegate CheckInternetConnection];
	if(l_appDelegate.m_internetWorking==0)//0: internet working
	{
		self.view.userInteractionEnabled=FALSE;
		[m_activityIndicator startAnimating];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		NSMutableString *temp_url;
		//temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/customer.htm?action=registerCustomer",kServerUrl];
        
        temp_url=[NSMutableString stringWithFormat:@"%@/salebynow/json.htm?action=registerCustomer",kServerUrl];
		temp_url=(NSMutableString *)[temp_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		NSLog(@"%@",temp_url);
		
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSMutableString *pathDoc=[NSString stringWithFormat:@"%@",documentsDirectory];
		NSMutableString *tempEmail=[NSMutableString stringWithFormat:@"%@/%@.jpg",pathDoc,m_txtEmail.text];
		NSLog(@"Doc Directory: %@",tempEmail);
		
        if (m_personData == nil) {
            int r = arc4random()%6 + 1;
            NSString *avatarStr = [NSString stringWithFormat:@"avatar%d.jpg", r];
            UIImage *image = [UIImage imageNamed:avatarStr];
            self.m_personData=UIImageJPEGRepresentation(image,0.0);
            
        }
		[self.m_personData writeToFile:tempEmail atomically:YES];
		
		NSMutableDictionary *temp_dictRegData;
		
		NSUInteger len = [self.m_personData length];
		
		const char *raw=[self.m_personData bytes];
		
		
		//creates the byte array from image
		NSMutableArray *tempByteArray=[[NSMutableArray alloc]init];
		for (int i = 0; i < len; i++)
		{
			[tempByteArray addObject:[NSNumber numberWithInt:raw[i]]];
		} 
		
		//Bharat: DE43 : 12/06/11 : Change location 
		//temp_dictRegData=[NSMutableDictionary dictionaryWithObjectsAndKeys:[tempByteArray JSONFragment] ,@"photo", m_ageGroup,@"ageGroup",m_txtFirstName.text,@"firstname",@"-122.3888752",@"longitude",@"37.7796428",@"latitude",m_txtLastName.text,@"lastname",[m_txtEmail.text lowercaseString],@"email",m_txtPassword.text,@"password",m_gender,@"gender",nil];
		NSString * latString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_latitude]];
		NSString * lngString = [NSString stringWithFormat:@"%f", [l_appDelegate.m_objGetCurrentLocation m_longitude]];
		NSLog(@"NewRegistration:lat=[%@], lng=[%@]",latString, lngString);
		
        temp_dictRegData=[NSMutableDictionary dictionaryWithObjectsAndKeys:[tempByteArray JSONFragment] ,@"photo",m_txtFirstName.text,@"firstname",lngString,@"longitude",latString,@"latitude",m_txtLastName.text,@"lastname",[m_txtEmail.text lowercaseString],@"email",m_txtPassword.text,@"password",nil];
        
        //release the temporary byte array
		[tempByteArray release];
		tempByteArray=nil;
		
		NSMutableString *temp_strJson =[[NSMutableString alloc]initWithFormat:@"customer=%@",[temp_dictRegData JSONRepresentation]];
        NSLog(@"JSON str:%@", temp_strJson);
		NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:temp_url]
																  cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];	
		
		NSDictionary *headerFieldsDict = [NSDictionary dictionaryWithObjectsAndKeys:@"application/x-www-form-urlencoded", @"Content-Type", nil];
		
		[theRequest setHTTPBody:[temp_strJson dataUsingEncoding:NSUTF8StringEncoding]];
		[theRequest setAllHTTPHeaderFields:headerFieldsDict];
		[theRequest setHTTPMethod:@"POST"];
		
		l_theConnection = [[[NSURLConnection alloc] initWithRequest:theRequest delegate:self]autorelease];
		
		[l_theConnection start];
		
		if(l_theConnection)
		{
			NSLog(@"Request sent to get data");
		}
		
		[temp_strJson release];
	}
	else
	{
		self.view.userInteractionEnabled=TRUE;
		[l_indicatorView stopLoadingView];
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		UIAlertView *netCheckAlert=[[UIAlertView alloc]initWithTitle:kNetNotAvailableAlertTitle message:kNetNotAvailableAlertMsg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[netCheckAlert show];
		[netCheckAlert release];
	}	
}

#pragma mark -
#pragma mark Connection response methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
	m_intResponseCode = [httpResponse statusCode];
	NSLog(@"the response  is %d",m_intResponseCode);
	
	[m_mutResponseData setLength:0];	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	//NSLog(@"%@",[[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]autorelease]);
	
	[m_mutResponseData appendData:data];
	//NSLog(@"the data in the registration class is %@",m_mutResponseData);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	[l_indicatorView stopLoadingView];
	
	self.view.userInteractionEnabled=TRUE;
	
	NSLog(@"response from server: %@",[[[NSString alloc]initWithData:m_mutResponseData encoding:NSUTF8StringEncoding]autorelease]);
		
		
	
	if(m_intResponseCode==500 || m_intResponseCode==501)
	{
        [[NewsDataManager sharedManager] showErrorByCode:m_intResponseCode fromSource:NSStringFromClass([self class])];
//		UIAlertView *signupAlert=[[UIAlertView alloc]initWithTitle:@"Signup Failed!" message:@"Internal error occured. Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//		[signupAlert show];
//		[signupAlert release];
//		signupAlert=nil;
	}
	else if(m_intResponseCode==412 || m_intResponseCode == 401)
	{
        [[NewsDataManager sharedManager] showErrorByCode:401 fromSource:NSStringFromClass([self class])];
		[self dismissViewControllerAnimated:NO completion:nil];
		
	}
	else if(m_intResponseCode==409)
	{
		UIAlertView *alreadyExistAlert=[[UIAlertView alloc]initWithTitle:@"" message:@"User already exists, please signup with different email id." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alreadyExistAlert show];
		[alreadyExistAlert release];
		alreadyExistAlert=nil;
	}
	else if(m_mutResponseData!=nil && m_intResponseCode==200)
	{
		NSLog(@"data downloaded");
				
		UIAlertView *successfulRegisteredAlert=[[UIAlertView alloc]initWithTitle:@"Congratulations!!" message:@"You are registered successfully, please login with your email id and password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[successfulRegisteredAlert show];
		[successfulRegisteredAlert release];
		successfulRegisteredAlert=nil;
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		
		[prefs setValue:@"" forKey:@"remember"];
		
		
		//NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		[prefs setValue:m_txtEmail.text forKey:@"userName"];
		[prefs setValue:m_txtPassword.text forKey:@"password"];
		
		[self dismissViewControllerAnimated:NO completion:^{
            LoginViewController *temp_loginViewCtrl=[[[LoginViewController alloc]init]autorelease];
            temp_loginViewCtrl.m_fromRegistration=YES;
            temp_loginViewCtrl.m_isLoggedOut=TRUE;
            temp_loginViewCtrl.m_objLoginHome = m_homeViewObj;
            [(LoginHomeViewController *)m_homeViewObj presentViewController:temp_loginViewCtrl animated:NO completion:nil];
        }];
		
    }	
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	self.view.userInteractionEnabled=TRUE;
	[l_indicatorView stopLoadingView];
    
    NSLog(@"error on registration %@", [error localizedDescription]);
	//--------code for server down alert-----------------
	UIView *temp_dtView=[[UIView alloc]initWithFrame:CGRectMake(0,0,320,460)]; //65,180,197,122)];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height > 480)
    {
        [temp_dtView setFrame:CGRectMake(0,0,320,screenRect.size.height - 20)];
    }
    
	temp_dtView.tag=200;
	temp_dtView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
	
	
	UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(65,140,198,123)];
	temp_imgView.image=[UIImage imageNamed:@"oops_popup.png"];
	[temp_dtView addSubview:temp_imgView];
	[temp_imgView release];
	
	UIButton *temp_btnClose=[UIButton buttonWithType:UIButtonTypeCustom];
	[temp_btnClose addTarget:self action:@selector(btnCloseAction:) forControlEvents:UIControlEventTouchUpInside];
	[temp_btnClose setFrame:CGRectMake(235,140,30,30)];
	[temp_dtView addSubview:temp_btnClose];
	
	[self.view addSubview:temp_dtView];
	[temp_dtView release];
	//--------------------------------------------------
	
}
#pragma mark Animation methods


-(void)btnCloseAction:(id)sender
{
	[[self.view viewWithTag:200] removeFromSuperview];
    [[NewsDataManager sharedManager] showErrorByCode:5 fromSource:NSStringFromClass([self class])];
    [self dismissModalViewControllerAnimated:NO];
}

-(void)animateViewUpward
{
	[UIView beginAnimations: @"upView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.view.frame = CGRectMake(0,-20, 320,screenRect.size.height - 20);
    
	[UIView commitAnimations];
}

-(void)animateViewDownward
{
	[UIView beginAnimations: @"downView" context: nil];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDuration: 0.4];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];

    CGRect screenRect = [[UIScreen mainScreen] bounds];
    self.view.frame = CGRectMake(0,20, 320,screenRect.size.height - 20);

	[UIView commitAnimations];
}


#pragma mark -----------------






// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

-(void) viewDidLoad
{
	[super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
	l_appDelegate.m_checkForRegAndMypage=@"1";
	// Bharat: 11/10/2011: Changed 13-16 age group to "Under 16", as suggested by Ramesh
	//m_agesArray=[[NSArray alloc]initWithObjects:@"13-16",@"16-21",@"22-27",@"28-34",@"35+",nil];
	m_agesArray=[[NSArray alloc]initWithObjects:@"Under 16",@"16-21",@"22-27",@"28-34",@"35+",nil];
	m_mutResponseData=[[NSMutableData alloc] init];
	//[UIView beginAnimations:nil context:nil];
//	[UIView setAnimationDuration:0.5];
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    
	[m_pickerView setFrame:CGRectMake(0, screenRect.size.height - 20, 320, 216)];
	[m_toolbarWithDoneButton setFrame:CGRectMake(0, screenRect.size.height, 320, 44)];
	l_indicatorView=[LoadingIndicatorView SharedInstance];
    
    [self.m_imageHeader setImage:[UIImage imageNamed:@"navbar"]];
    [self.m_imageHeader setContentMode:UIViewContentModeScaleToFill];
    [self.m_imageHeader setFrame:CGRectMake(0, 0, 320, 52)];

    
	//[self.view addSubview:m_pickerView];
//	[UIView commitAnimations];
		
	// Bharat: 11/11/2011: Add the silhoutte image
//	[m_TakePictureBtn setImage:[UIImage imageNamed:@"silhouette"] forState:UIControlStateNormal];
    m_TakePictureBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    m_TakePictureBtn.layer.borderWidth = 1.0;
    [m_nextView setHidden:YES];
	
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
   
}

-(void) viewDidAppear:(BOOL)animated
{
    [self.m_imageHeader setFrame:CGRectMake(0, 0, 320, 52)];

    NSLog(@"The header view frame is : %10.2f", self.m_imageHeader.frame.size.height);
}

// GDL: Changed everything below.

#pragma mark - memory management

-(void)viewDidUnload {
    [self setM_imageHeader:nil];
    [super viewDidUnload];

    // Release retained IBOutlets.
    [m_pickerView release]; m_pickerView = nil;
    [m_toolbarWithDoneButton release]; m_toolbarWithDoneButton = nil;
    self.m_txtFirstName = nil;
    self.m_txtLastName = nil;
    self.m_txtEmail = nil;
    self.m_txtPassword = nil;
    self.m_MsButton = nil;
    self.m_MrButton = nil;
    self.m_ageGroupBtn = nil;
    self.m_TakePictureBtn = nil;
    self.m_nextView=nil;
}

-(void)dealloc  {
    [m_txtFirstName release];
    [m_txtLastName release];
    [m_txtEmail release];
    [m_txtPassword release];
    [m_MrButton release];
    [m_MsButton release];
    [m_activityIndicator release];
    [m_agePicker release];
    [m_mutResponseData release];
    [m_agesArray release];
    [m_gender release];
    [m_pickerView release];
    [m_toolbarWithDoneButton release];
    [m_ageGroupBtn release];
    [m_personImage release];
    [m_personData release];
    [m_ageGroup release];
    [m_TakePictureBtn release];
    [m_view release];
    [m_indicatorView release];
    [m_homeViewObj release];
    [m_nextView release];

   // [_m_imageHeader release];
	[super dealloc];
}

@end
