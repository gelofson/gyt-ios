//
//  CustomTopBarView.m
//  QNavigator
//
//  Created by Soft Prodigy on 25/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CustomTopBarView.h"
#import "CategoriesViewController.h"
#import "QNavigatorAppDelegate.h"
#import "Constants.h"
#import"BuzzButtonUseCase.h"
#include<QuartzCore/QuartzCore.h>

@implementation CustomTopBarView

@synthesize m_imgViewLogo;
@synthesize m_imgViewBlueBar;
@synthesize m_arrCategories;
@synthesize m_intSelectedCategory;
@synthesize categoriesDelegate;
@synthesize m_scrollView;
@synthesize m_pageControl;

QNavigatorAppDelegate *l_appDelegate;
int l_page;

#pragma mark -
#pragma mark Initialization

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        //Initialization code
		[self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"header_bg.jpg"]]];
		m_pageControl.hidden=YES;
		
		m_imgViewBlueBar=[[UIImageView alloc]initWithFrame:CGRectMake(0,87+6,320,6)]; // 12
		m_imgViewBlueBar.image=[UIImage imageNamed:@"top-strip.png"];
		
		[self addSubview:m_imgViewBlueBar];
		
		m_imgViewLogo=[[UIImageView alloc]initWithFrame:CGRectMake(118,14,84,27)];
		m_imgViewLogo.image=[UIImage imageNamed:@"new_logo"];
		[self addSubview:m_imgViewLogo];
		
		l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	}
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

#pragma mark -
#pragma mark Custom Methods
-(IBAction)buzzUseCase:(id)sender
{
	BuzzButtonUseCase *BuzzBtnUseCase=[[BuzzButtonUseCase alloc] init];
	BuzzBtnUseCase.m_intBackType=2;
	[[l_appDelegate.m_SelectedMainPageClassObj navigationController] pushViewController:BuzzBtnUseCase animated:YES];
	[CATransaction begin];
	CATransition *animation = [CATransition animation];
	animation.type = kCATransitionFromLeft;
	animation.duration = 0.3;
	//animation.delegate=self;
	[animation setValue:@"Slide" forKey:@"SlideViewAnimation"];
	[CATransaction commit];
	
	
	[BuzzBtnUseCase release];
	BuzzBtnUseCase=nil;
	
}

//creates the top category buttons bar with scroll view and page control
- (void)addCategoryBarButtons {
	m_pageControl.hidden=YES;
	
	//UIButton *tmp_BuzzButton=[UIButton  buttonWithType:UIButtonTypeCustom];// alloc] initWithFrame:CGRectMake(3,3 , 49, 41)];
	//tmp_BuzzButton.frame=CGRectMake(3,3,49, 41);
	//[tmp_BuzzButton setBackgroundImage:[UIImage imageNamed:@"btn_gray_buzz.png"] forState:UIControlStateNormal];
	//[l_appDelegate.m_customView addSubview:tmp_BuzzButton];
	//[tmp_BuzzButton addTarget:self action:@selector(buzzUseCase:) forControlEvents:UIControlEventTouchUpInside];
	numberOfBarButtons=m_arrCategories.count;
   
	for( int i = 0; i < m_arrCategories.count; i++ ) {
		UIButton *temp_btnCategory = [UIButton buttonWithType:UIButtonTypeCustom];
		[temp_btnCategory setFrame:CGRectMake(i*45, 8, 52, 53)];//(i*45,14,39,40)]; //
		
		[temp_btnCategory setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",[m_arrCategories objectAtIndex:i]]] forState:UIControlStateNormal];
		[temp_btnCategory setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_selected.png",[m_arrCategories objectAtIndex:i]]] forState:UIControlStateSelected];
		[temp_btnCategory addTarget:self action:@selector(btnCategoryAction:) forControlEvents:UIControlEventTouchUpInside];
		[temp_btnCategory setTag:[[m_arrCategories objectAtIndex:i] intValue]];
		
		//logic to add right arrow button at the right of the list
		if ( m_arrCategories.count > 7 && i == 6 ) {
			UIButton *temp_btnRightArrow=[UIButton buttonWithType:UIButtonTypeCustom];
			
			// Bharat: 12/04/11: US150: Replacing with more arrow image
			//[temp_btnRightArrow setFrame:CGRectMake((i*45)-5,43+6, 52, 53)];//((i*45)-5,35,39,40)]; //
			//[temp_btnRightArrow setImage:[UIImage imageNamed:@"right.png"] forState:UIControlStateNormal];
			[temp_btnRightArrow setFrame:CGRectMake((i*45)-5,43+6, 52, 53)];//((i*45)-5,35,39,40)]; //
			[temp_btnRightArrow setImage:[UIImage imageNamed:@"red arrow"] forState:UIControlStateNormal];
			
			[temp_btnRightArrow addTarget:self action:@selector(btnRightArrowAction:) forControlEvents:UIControlEventTouchUpInside];
			[temp_btnRightArrow setTag:111];
			
		}
		
		
		//set selected button
		if ( m_intSelectedCategory == [[m_arrCategories objectAtIndex:i] integerValue] )
			[temp_btnCategory setSelected:TRUE];
		
		[m_scrollView addSubview:temp_btnCategory];
		//[m_scrollView setBackgroundColor:[UIColor grayColor]];
		m_scrollView.contentSize = CGSizeMake(m_scrollView.contentSize.width+45, m_scrollView.frame.size.height);
		
		
		//[tmp_BuzzButton release];
		//tmp_BuzzButton=nil;	
		//[self addSubview:temp_btnCategory];
	}
}

-(void)btnRightArrowAction:(id)sender
{
	l_page = m_pageControl.currentPage+1;//int temp_page
	//update the scroll view to the appropriate page   		 
	CGRect frame = m_scrollView.frame;
	frame.origin.x = frame.size.width * l_page;//temp_page;
	frame.origin.y = 0;
	NSLog(@"%f",frame.origin.x);
	[m_scrollView scrollRectToVisible:frame animated:YES];
	[sender removeFromSuperview];
	
}
//navigate back to big sale view
-(void)scrollToBigSaleIcon
{
	l_page = 0;//m_pageControl.currentPage+1;//int temp_page
	//update the scroll view to the appropriate page   		 
	CGRect frame = m_scrollView.frame;
	frame.origin.x = frame.size.width * l_page;//temp_page;
	frame.origin.y = 0;
	NSLog(@"%f",frame.origin.x);
	[m_scrollView scrollRectToVisible:frame animated:YES];
	
}

-(void)btnCategoryAction:(id)sender
{
	l_appDelegate.m_tagIdentifier=[sender tag];
	if(m_intSelectedCategory!=[sender tag])
	{
		[self performSelector:@selector(deselectCustomBarButtons)];
		[sender setSelected:YES];
		m_intSelectedCategory=[sender tag];
		NSLog(@"%d",[sender tag]);
		
		switch ([sender tag])
		{
			case 1:
				l_appDelegate.m_strCategoryName=@"Indy shop";
				break;
			case 2:
				l_appDelegate.m_strCategoryName=@"";
				break;
			case 3:
				l_appDelegate.m_strCategoryName=kShoes;
				break;
			case 4:
				l_appDelegate.m_strCategoryName=kBags;
				break;
			case 5:
				l_appDelegate.m_strCategoryName=kApparel;
				break;
			case 6:
				l_appDelegate.m_strCategoryName=kAccessories;
				break;
			case 7:
				l_appDelegate.m_strCategoryName=kHealthBeauty;
				break;
			case 8:
				l_appDelegate.m_strCategoryName=kSports;
				break;
			case 9:
				l_appDelegate.m_strCategoryName=kElectronics;
				break;
			case 10:
				l_appDelegate.m_strCategoryName=kHomeFashions;
				break;
			case 11:
				l_appDelegate.m_strCategoryName=kMovies;
				break;
			case 12:
				l_appDelegate.m_strCategoryName=kDining;
				break;
			case 13:
				l_appDelegate.m_strCategoryName=kKids;
				break;
			case 14:
				l_appDelegate.m_strCategoryName=kMen;
				break;
			case 15:
				l_appDelegate.m_strCategoryName=kCharity;
				break;
			default:
				l_appDelegate.m_strCategoryName=@"";
				break;
		}
			
		[categoriesDelegate categoriesButtonAction:sender];
		
	}
	else if(m_intSelectedCategory==1)
	{
		//l_appDelegate.tabBarController.selectedIndex=0;
		
		[categoriesDelegate switchIndyShopListingView];
		
	}
    
        
	else if(m_intSelectedCategory>=3)
	{
		//l_appDelegate.tabBarController.selectedIndex=0;
		
		//check if Categories view controller already exists
		//[[LoadingIndicatorView SharedInstance] stopLoadingView];
			
		//switch between search listing view and logos view	
		
		[categoriesDelegate switchLogosListingView];
		
	}

}

//deselects all the top bar buttons before selecting a new category item
-(void)deselectCustomBarButtons
{
	for(int i=0;i<m_arrCategories.count;i++)
	{
		UIButton *temp_btnCategory=(UIButton *)[self viewWithTag:[[m_arrCategories objectAtIndex:i]intValue]];
		[temp_btnCategory setSelected:FALSE];
	}
}

-(void)selectCategory:(int)catTag
{
	[self deselectCustomBarButtons];
	for (UIButton *temp_btnCat in self.m_scrollView.subviews )
	{
		if ([temp_btnCat viewWithTag:catTag]) 
		{
			[temp_btnCat setSelected:YES];
			m_intSelectedCategory=catTag;
		}	
	}
}

- (void)createScrollBar {
	if(m_arrCategories.count>7)
		m_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,35+6,280,53)];
	else
		m_scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,35+6,320,53)];

	m_scrollView.delegate = self;
	m_scrollView.pagingEnabled = NO;
	m_scrollView.userInteractionEnabled = YES;
	m_scrollView.showsVerticalScrollIndicator = NO;
	m_scrollView.showsHorizontalScrollIndicator = NO;

	//m_scrollView.delaysContentTouches=NO;
	m_scrollView.contentSize = CGSizeMake(52,53);
	m_scrollView.scrollsToTop = NO;
	m_scrollView.contentOffset = CGPointMake(0,0);
	m_scrollView.backgroundColor = [UIColor clearColor];
	
	[self addSubview:m_scrollView];
	
	m_pageControl=[[UIPageControl alloc]initWithFrame:CGRectMake(0,83,320,20)];
	[m_pageControl setNumberOfPages:2];
	[m_pageControl setBackgroundColor:[UIColor clearColor]];
	[m_pageControl addTarget:self action:@selector(changePageAction:) forControlEvents:UIControlEventTouchUpInside];
	[self addSubview:m_pageControl];
	
	
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
	// Switch the indicator when more than 50% of the previous/next page is visible
	
	m_scrollView.frame=CGRectMake(0,35+6,320,53);
	m_scrollView.delegate=nil;
		
}

- (void)changePageAction:(id)sender 
{
	l_page = m_pageControl.currentPage;//int temp_page
	//update the scroll view to the appropriate page   		 
	CGRect frame = m_scrollView.frame;
	frame.origin.x = frame.size.width * l_page;//temp_page;
	frame.origin.y = 0;
	NSLog(@"%f",frame.origin.x);
	[m_scrollView scrollRectToVisible:frame animated:YES];	 
}

#pragma mark -
#pragma mark Overridden Setter Methods

-(void)setM_arrCategories:(NSArray *)array
{
	if(m_arrCategories)
	{
		[m_arrCategories release];
		m_arrCategories=nil;
	}
	m_arrCategories=[[NSArray alloc]initWithArray:array];
	
}


// GDL: Changed everything below.

#pragma mark -
#pragma mark Memory Management Methods

- (void)dealloc {
    
    [m_imgViewLogo release];
    [m_imgViewBlueBar release];
    [m_arrCategories release];
    
    // This should be assigned, not retained.
    //[categoriesDelegate release];
    
    [m_scrollView release];
    [m_pageControl release];
    
    [super dealloc];
}

@end
