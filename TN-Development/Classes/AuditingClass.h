//
//  AuditingClass.h
//  QNavigator
//
//  Created by Soft Prodigy on 22/09/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AuditingClass : NSObject 
{
	NSMutableArray *m_arrAuditData;
	NSMutableData *m_responseData;
	
	int m_intResponseCode;
}

@property (nonatomic, retain) NSMutableArray *m_arrAuditData;
@property (nonatomic, retain) NSMutableData *m_responseData;
@property int m_intResponseCode;


+ (id) SharedInstance;
- (void)initializeMembers;
-(void)sendRequestToSubmitAuditData;


@end
