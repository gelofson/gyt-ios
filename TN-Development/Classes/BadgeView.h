//
//  BadgeView.h
//  QNavigator
//
//  Created by Mac on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BadgeView : UIView {
    UILabel *badgeLabel;
}
@property(retain,nonatomic)IBOutlet UILabel *badgeLabel;
@end
