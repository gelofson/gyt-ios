//
//  PushNotificationViewController.h
//  QNavigator
//
//  Created by Mac on 9/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationCell.h"
#import "AcceptRejectNotificationCell.h"

enum {
    NONE_NOTIFICATION = 0,
    LIKE_TYPE_NOTIFICATION,
    FOLLOWING_NOTIFICATION,
    ACCEPT_REJECT_NOTIFICATION
};

@interface PushNotificationViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,NotificationCellDelegate,AcceptRejectNotificationDelegate> {
    int                 m_currentPage;
    int                 m_totalRecords;
    int                 m_itemsPerPage;
    BOOL                m_isLoading;
    NSMutableArray*     m_notificationsArray;
    IBOutlet UIButton *doneButton;
    IBOutlet UIButton *backButton;
    BOOL doneButtonVisible;
    IBOutlet UITableView *o_tableView;
    NSString *          m_type;
    NSString          * _notificationID;
    NSString          * _userEmail;
    NSString          * _userName;
    BOOL                goToProfile;
    NSIndexPath       * _selectedIndexPath;
    NSInteger           currentType;
}

//-(void)iconClickedAction:(int)messageId;
- (id) initWithDoneButton:(BOOL) button;
//-(void)btnBoughtItPicturesAction;
//-(void)btnPicturesAction;


@property (nonatomic, retain) NSString    * notificationID;
@property (nonatomic, retain) NSString    * userEmail;
@property (nonatomic, retain) NSString    * userName;
@property (nonatomic, retain) NSIndexPath * selectedIndexPath;
@property NSInteger currentType;

@end
