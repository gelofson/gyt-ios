//
//  NewsMoreViewController.h
//  TinyNews
//
//  Created by Nava Carmon on 12/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGridView.h"
#import "UIGridViewDelegate.h"

@class CategoryData;
@class LoadingIndicatorView;
@class DetailPageViewController;
@class VerticalScrollViewController;

@interface NewsMoreViewController : UIViewController<UIGridViewDelegate> {
    int filter;
    int curPage;
    int numPages;
    int totalRecords;
    BOOL fetchingData;
	UIViewController *m_CallBackViewController;
    UILabel *m_headerLabel;
    IBOutlet UIButton *moreButton;
    LoadingIndicatorView *l_buyItIndicatorView;
    CategoryData *m_data;
    UIView *footerView;
    DetailPageViewController *detailController;
    VerticalScrollViewController *verticalScrollController;
}

@property (nonatomic, retain) IBOutlet UIGridView *table;
@property(nonatomic,retain) NSString *m_Type;
@property(nonatomic,retain) IBOutlet UILabel *m_headerLabel;
@property (nonatomic, retain) IBOutlet UIButton *moreButton;
@property(nonatomic,retain) UIViewController *m_CallBackViewController;
@property(nonatomic,retain) CategoryData *m_data;
@property(nonatomic,retain) UIView *footerView;
@property(nonatomic,retain) DetailPageViewController *detailController;
@property(nonatomic,retain) VerticalScrollViewController *verticalScrollController;
@property int filter;
@property int curPage;
@property BOOL fetchingData;

- (IBAction)getMoreRecords:(id)sender;
- (IBAction)backPressed:(id)sender;
- (void) updateData;
//- (void) updateMessageData:(NSArray *)dataArray;
- (void) updateCurrentPage:(NSNumber *)aPage;
- (void) enableMore;

@end
