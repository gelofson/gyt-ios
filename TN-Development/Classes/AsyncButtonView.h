//
//  AsyncButtonView.h
//  QNavigator
//
//  Created by softprodigy on 27/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MoreButton;

@interface AsyncButtonView : UIView 
{
	//could instead be a subclass of UIImageView instead of UIView, depending on what other features you want to 
	// to build into this class?
	id m_target;
	NSURLConnection* connection; //keep a reference to the connection so we can cancel download in dealloc
	NSMutableData* data; //keep reference to the data so we can collect it as it downloads
	int m_tagValue;
    NSString *urlString;
    NSString *typeString;
	MoreButton *m_btnIcon;
    UIActivityIndicatorView *indicator;
	BOOL isCatView;
    NSInteger messageTag;
    NSTimer *fetchingTimer;
    BOOL imagePresent;
    BOOL cropImage;
    id delegate;
    MoreButton *m_btnIconSelect;
    BOOL maskImage;
	//but where is the UIImage reference? We keep it in self.subviews - no need to re-code what we have in the parent class
}

@property BOOL isCatView;
@property BOOL cropImage;
@property BOOL imagePresent;
@property BOOL maskImage;
@property  NSInteger messageTag;
@property (nonatomic,retain) NSMutableData* data;
@property (nonatomic,retain) NSURLConnection* connection;
@property (nonatomic,retain) id m_target;
@property (nonatomic,retain) NSString *urlString;
@property (nonatomic,copy) NSString *typeString;
@property (nonatomic,retain) NSTimer *fetchingTimer;
@property (nonatomic,retain) UIActivityIndicatorView *indicator;
@property (nonatomic,retain) id delegate;

@property (nonatomic,retain) MoreButton *m_btnIcon;
@property (nonatomic,retain) MoreButton *m_btnIconSelect;

- (void)loadImageFromURL:(NSURL*)url target:(id)target action:(SEL)selector btnText:(NSString *)btnText;
- (void)loadImageFromURL:(NSURL*)url target:(id)target action:(SEL)selector btnText:(NSString *)btnText ignoreCaching:(BOOL)ignore;
//- (UIImage*) image;
- (void) cancelRequest;
- (void) setNewImage:(UIImage *)img;
- (void) setButtonData:(NSDictionary *)adata;
- (void)resetButton;
- (void) setNewImage:(UIImage *)img forName:(NSString *)name;
- (CGSize) getImageSize;
- (void) changeViewRect:(CGRect)newRect;
@end
