//
//  MyPagePhotoViewController.h
//  QNavigator
//
//  Created by softprodigy on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIGridView.h"
#import "UIGridViewDelegate.h"

@interface MyPagePhotoViewController:UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>{
	
//	NSArray *m_mainArray;
	UIView *m_view;
	UIActivityIndicatorView *m_indicatorView;
	NSMutableArray *m_arrGetMyPage;
	NSMutableArray *m_arrGetMypagePopularPhoto;
	UITableView *tableViewGetPhoto;
	BOOL isFromViewFriends;
	NSString *m_FriendMailString;
	UILabel *m_HeaderLabel;
	NSString *m_FriendNameString;
    int indexCount;
    int selectedIndex;
    int storyCount;
    UIView *footerView;

}
@property(nonatomic,retain)UIView *footerView;
@property (nonatomic, retain) IBOutlet UIGridView *table;
@property (nonatomic,retain)NSMutableArray *m_arrGetMypagePopularPhoto;
@property (nonatomic,retain)IBOutlet UITableView *tableViewGetPhoto;
@property (nonatomic,retain) NSMutableArray *m_arrGetMyPage;
@property (nonatomic,retain) UIView *m_view;
@property (nonatomic,retain) NSString *m_FriendMailString;
@property( nonatomic,retain) IBOutlet UILabel *m_HeaderLabel;
@property (nonatomic,retain) NSString *m_FriendNameString;
//@property(nonatomic,retain) NSArray *m_mainArray;
@property BOOL isFromViewFriends;
@property(nonatomic,retain)UIActivityIndicatorView *m_indicatorView;

@property(nonatomic,assign)int storyCount;
-(IBAction) m_goToBackView;

@end
