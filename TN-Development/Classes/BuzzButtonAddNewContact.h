//
//  BuzzButton AddNewContact.h
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CONTACT_ENTRY_SELECT_TAG 20011
#define CONTACT_ENTRY_NAME_TAG 20012
#define CONTACT_ENTRY_EMAIL_TAG 20013

@interface BuzzButtonAddNewContact : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate> {

	IBOutlet UIView *m_sectionHeaderView;
	IBOutlet UITableView *m_emailTableView;
	
	IBOutlet UITextField *m_nameTextField;
	IBOutlet UITextField *m_emailTextField;

	NSMutableDictionary *m_DictOfContactDicts;
	NSMutableDictionary *m_newlyAddedContactsDict;
}

@property (nonatomic,retain) IBOutlet UIView *m_sectionHeaderView;
@property (nonatomic,retain) IBOutlet UITableView *m_emailTableView;
@property (nonatomic,retain) IBOutlet UITextField * m_nameTextField;
@property (nonatomic,retain) IBOutlet UITextField * m_emailTextField;
@property (nonatomic,retain) NSMutableDictionary *m_DictOfContactDicts;
@property (nonatomic,retain) NSMutableDictionary *m_newlyAddedContactsDict;

-(IBAction)goToBackView;
-(IBAction) addNewEntryAction:(id) sender;
- (IBAction)removeRow:(id)sender;


@end


