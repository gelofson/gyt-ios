//
//  FittingRoomMoreViewController.h
//  QNavigator
//
//  Created by softprodigy on 21/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import"AsyncButtonView.h"



@interface FittingRoomMoreViewController : UIViewController <UITextViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UIScrollViewDelegate>{
	IBOutlet UIScrollView *m_scrollViewMoreInfo;
	UIButton *m_dontBuyBtn;
	UIButton *m_BuyItBtn;
	UIButton *m_IboughtItBtn;
	//UIButton *m_ChangeInMindBtn;
	UIButton *m_BuyBtn;
	NSMutableDictionary *m_ResponseDictionary;
	NSString *m_userId;
	NSString *m_Type;
	NSInteger m_messageId;
	NSArray *m_dataArray;
	NSString *m_CheckString;
	UIView *m_ThePhotoView;
	UIImageView *m_ImageView;
	BOOL IboughtItBtnClicked;// for checking if the iboughtit btn was clicked or not
	BOOL MineTrack;
	UILabel *m_headerLabel;
	UIViewController *m_CallBackViewController;
	UIView *m_TheContentView;
	UIImageView *m_ProductImageView;
	NSMutableArray *m_messageIDsArray;
	NSInteger m_currentMessageID;
	IBOutlet UITextView *m_textViewSubject;
	IBOutlet UILabel *m_originatorNameLabel;
	UILabel *m_elapsedTimelabel;
	UIImageView *m_likeImageView;
	UIImageView *m_buyItimageView;
	UIImageView *m_dontbuyImageView;
	UILabel *m_likeLabel;
	UILabel *m_dontBuyLabel;
	AsyncButtonView *asyncProductButton2, *asyncButtonTop;
	UILabel *m_FollowOrUnfollowLabel,*m_LabelWishlist;
	UIImageView *m_GreenImageView;
    BOOL firstTime, follow;
	IBOutlet	UIButton *m_likeBtn;
	IBOutlet UIButton *m_BtnComment,*m_BtnWishlist,*m_BtnFollowOrUnfollow;
    
    // Nava adding placeholders
    IBOutlet UILabel     *headerLabel;
    IBOutlet UITextView  *whoWhatTextView;
    IBOutlet UITextView *Whowhattitletext;
    UIButton *m_leftArrow;
	UIButton *m_rightArrow;
	BOOL GetPinnFlag;
    int filter;
    int curPage;
    int numPages;
    int totalRecords;
    BOOL fetchingData;
}
@property(nonatomic,assign)BOOL GetPinnFlag;
@property (nonatomic,retain) IBOutlet UIButton *m_BtnComment,*m_BtnWishlist,*m_BtnFollowOrUnfollow,*m_likeBtn;
@property (nonatomic,retain) IBOutlet UIScrollView *m_scrollViewMoreInfo;
@property(nonatomic,retain) IBOutlet	UIButton *m_dontBuyBtn;
@property(nonatomic,retain) IBOutlet	UIButton *m_IboughtItBtn;
//@property(nonatomic,retain) IBOutlet	UIButton *m_ChangeInMindBtn;
@property(nonatomic,retain) IBOutlet UIButton *m_BuyBtn;
@property(nonatomic,retain) IBOutlet	UIButton *m_leftArrow;
@property(nonatomic,retain) IBOutlet	UIButton *m_rightArrow;
@property(nonatomic,readwrite) NSInteger m_messageId;
@property(nonatomic,retain) NSArray *m_dataArray;
@property(nonatomic,retain) NSString *m_Type;
@property(nonatomic,retain) NSMutableDictionary *m_ResponseDictionary;
@property(nonatomic,retain) NSString *m_CheckString;
@property(nonatomic,retain) IBOutlet UIView *m_ThePhotoView;
@property(nonatomic,retain) IBOutlet UIImageView *m_ImageView;
@property(nonatomic,readwrite) BOOL MineTrack;
@property(nonatomic,retain) IBOutlet UILabel *m_headerLabel;
@property(nonatomic,retain) UIViewController *m_CallBackViewController;
@property(nonatomic,retain) NSMutableArray *m_messageIDsArray;
@property(nonatomic,retain) IBOutlet UILabel *m_FollowOrUnfollowLabel,*m_LabelWishlist;
@property(nonatomic,retain) IBOutlet UIImageView *m_GreenImageView;
@property(nonatomic, retain) IBOutlet UILabel *m_originatorNameLabel;
@property(nonatomic, retain) IBOutlet UITextView *m_textViewSubject;

@property(nonatomic, retain) IBOutlet UILabel    *headerLabel;
@property(nonatomic, retain) IBOutlet UITextView *whoWhatTextView;
@property(nonatomic, retain) IBOutlet UITextView *Whowhattitletext;
@property(nonatomic, retain)  AsyncButtonView *asyncProductButton2;


@property(nonatomic, assign) IBOutlet UIImageView *whiteLine1;
@property(nonatomic, assign) IBOutlet UIImageView *whiteLine2;
@property(nonatomic, assign) IBOutlet UILabel *m_likeLabel;
@property int filter;
@property int curPage;
@property NSInteger m_currentMessageID;
-(IBAction)btnMoreAction:(id)sender;
-(IBAction)btnBackAction:(id)sender;
-(IBAction) btnBuyAction:(id)sender;
-(IBAction) btnDontBuyAction:(id)sender;
-(IBAction) btnFollowAction:(id)sender;
-(IBAction)btnUnfollowAction:(id)sender;
-(IBAction) btnFlagItAction;
-(IBAction)btnIboughtItAction;
-(IBAction)ProductBtnClicked:(id)sender;
-(void)initialDelayEnded;
-(void)bounce1AnimationStopped;
-(void)bounce2AnimationStopped;
-(IBAction)CloseBtnAction;
-(void)showAlertView:(NSString *)alertTitle alertMessage:(NSString *)alertMessage tag:(NSInteger)Tagvalue cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSString*)otherButtonTitles;
-(IBAction)ChangeInMindAction;
-(IBAction)FollowOrUnfollowAction;
-(IBAction)FindtheCurrentIndexInTheArray;
-(IBAction)LeftMovement:(id)sender;
-(IBAction)RightMovement:(id)sender;
-(IBAction)UpdateScrollViewContent;
-(IBAction)CommentBtnAction;
-(IBAction)btnWishListAction;
- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;
-(NSString *) formatWhoWhatText:(NSDictionary *)dataDict;
-(NSString *)formatWhoWhattitle:(NSDictionary*)datatitle;
-(NSString*)dateReFormated:(NSString*)date;
-(void) showHideArrowButton;
- (void) commenterImagePressed:(id)sender;
- (IBAction)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;
-(void)ProductBtnClicked:(id)sender;
@end
