 //
//  BuzzButton AddFromAddressBook.m
//  QNavigator
//
//  Created by Bharat Biswal on 12/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/CALayer.h>
#import <AddressBook/ABPerson.h>
#import <AddressBook/ABMultiValue.h>

#import "BuzzButtonAddFromAddressBook.h"
#import "QNavigatorAppDelegate.h"

QNavigatorAppDelegate *l_appDelegate;

@implementation BuzzButtonAddFromAddressBook

@synthesize m_emailTableView;
@synthesize m_sectionHeaderView;
@synthesize m_addrBookEmailContacts;
@synthesize m_selectAllButton;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

-(void)fillContactsDictionaryFromAddressbook
{
	
	ABRecordRef tmp_recordObj;
	NSString	*tempFName;
	NSString	*tempLName;
	//UIImage		*tempImage;
	NSString	*tmp_mailID;
	int			tmp_count;
	__block CFArrayRef cfpeople = NULL;
    
	ABMultiValueRef *tmp_mail;
    
    CFErrorRef myError = NULL;
	
    ABAddressBookRef myAddressBook = ABAddressBookCreateWithOptions(NULL, &myError);
    
    while (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(myAddressBook,
                 ^(bool granted, CFErrorRef error) {
                     if (granted) {
                         cfpeople = ABAddressBookCopyArrayOfAllPeople(myAddressBook);
                     } else {
                         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Couldn't access the address book"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                         [alertView show];
                         [alertView release];
                         return;
                     }
                 });
    }
    //CFRelease(myAddressBook);
	int count = CFArrayGetCount(cfpeople);
	for (int i=0; i<count; i++)
	{
		tmp_recordObj = CFArrayGetValueAtIndex(cfpeople, i);
		
		tmp_mail=(ABMultiValueRef *)ABRecordCopyValue(tmp_recordObj,kABPersonEmailProperty);
		tmp_count=ABMultiValueGetCount(tmp_mail);
		
		if (tmp_count>0)
		{
			tmp_mailID=(NSString *)ABMultiValueCopyValueAtIndex(tmp_mail,0);
			if ((tmp_mailID == nil) || ([tmp_mailID length] <= 0)) {
				CFRelease(tmp_mail);
				continue;
			}
		}
		else {
			CFRelease(tmp_mail);
			continue;
		}
		
	 	tempFName = (NSString *)ABRecordCopyValue(tmp_recordObj,kABPersonFirstNameProperty);
	 	tempLName = (NSString *)ABRecordCopyValue(tmp_recordObj,kABPersonLastNameProperty);
		NSString * fullName = nil;
		if (((tempFName == nil) || ([tempFName length] <= 0)) &&
				((tempLName == nil) || ([tempLName length] <= 0)) ) {
			fullName = [NSString stringWithFormat:@"%@", tmp_mailID];
		} else if ((tempLName == nil) || ([tempLName length] <= 0)) {
			fullName = [NSString stringWithFormat:@"%@", tempFName];
		} else if ((tempFName == nil) || ([tempFName length] <= 0)) {
			fullName = [NSString stringWithFormat:@"%@", tempLName];
		} else {
			fullName = [NSString stringWithFormat:@"%@ %@", tempFName, tempLName];
		}
				
		/*
		if(ABPersonHasImageData(tmp_recordObj))
		{
			tempImage =[UIImage imageWithData:(NSData *)ABPersonCopyImageData(tmp_recordObj)] ;
		}
		else {
			tempImage =[UIImage imageNamed:@"no-image"];
		}
		*/
		
		
		NSMutableDictionary *tempDict=[[NSMutableDictionary alloc] initWithCapacity:3];
		[tempDict setObject:fullName forKey:@"nameValue"];
		[tempDict setObject:tmp_mailID forKey:@"emailValue"];
		[tempDict setObject:@"0" forKey:@"selectionValue"];
		[self.m_addrBookEmailContacts setObject:tempDict forKey:fullName];
		[tempDict release];
		CFRelease(tmp_mail);
		CFRelease(tmp_mailID);

	}

    CFRelease(myAddressBook);
    CFRelease(cfpeople);


}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{

	[super viewDidLoad];
	
	NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithCapacity:3];
	self.m_addrBookEmailContacts = dict;
	[dict release];


    CALayer * layer = self.m_emailTableView.layer;
    layer.borderWidth = 3.0f;
    layer.borderColor = [UIColor colorWithRed:104.0f/255.0f green:176.0f/255.0f blue:223.0f/255.0f alpha:1.0].CGColor;
    layer.cornerRadius = 5.0f;
	
	[self fillContactsDictionaryFromAddressbook];

}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}	
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
 
-(IBAction)goToBackView
{
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
}

- (void)dealloc {
    
	self.m_emailTableView = nil;
	self.m_sectionHeaderView = nil;
	self.m_addrBookEmailContacts = nil;
	self.m_selectAllButton = nil;

    [super dealloc];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView.frame.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	return self.m_sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [m_addrBookEmailContacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell * cell = nil;
	static NSString *cellIdentifier= @"AddFromAddressBook_CellIdentifier";
	
	cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if(cell==nil) {
		cell=[[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier]autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
		UIImageView *temp_imgView=[[UIImageView alloc]initWithFrame:CGRectMake(13,5,18,18)];
		//[temp_imgView setImage:l_objIndyShopModel.m_imgItemImage]; //@"clothes.png"]];
		[temp_imgView setImage:[UIImage imageNamed:@"sharenow_checkbox_disabled"]];
		temp_imgView.contentMode=UIViewContentModeScaleAspectFit;
		temp_imgView.tag = CONTACT_ENTRY_SELECT_TAG;
		[cell.contentView addSubview:temp_imgView];
		[temp_imgView release];


		UILabel *temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(38,2,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:@"GillSans" size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:32.0f/255.0f green:80.0f/255.0f blue:112.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"Greg Elofson";
		temp_lblTitle.tag = CONTACT_ENTRY_NAME_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
		temp_lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(38,24,258,22)];
		temp_lblTitle.numberOfLines = 1;
		temp_lblTitle.font=[UIFont fontWithName:@"GillSans" size:16];
		temp_lblTitle.textColor=[UIColor colorWithRed:112.0f/255.0f green:112.0f/255.0f blue:112.0f/255.0f alpha:1.0];
		temp_lblTitle.backgroundColor=[UIColor clearColor];
		temp_lblTitle.text=@"greg.elofson@gmail.com";
		temp_lblTitle.tag = CONTACT_ENTRY_EMAIL_TAG;
		[cell.contentView addSubview:temp_lblTitle];
		[temp_lblTitle release];
	
	}
		
	NSArray * sortedKeys = [[self.m_addrBookEmailContacts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_addrBookEmailContacts objectForKey:keyStr];
        

        
        UIImageView * iv = (UIImageView *) [cell.contentView viewWithTag:CONTACT_ENTRY_SELECT_TAG];

		NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
		if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
			[iv setImage:[UIImage imageNamed:@"sharenow_checkbox_disabled"]];
		}
		else {
			[iv setImage:[UIImage imageNamed:@"sharenow_checkbox_enabled"]];
		}
        
        UILabel * lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_NAME_TAG];
        NSString * nameValue = [contactDict objectForKey:@"nameValue"];
        lbl.text = nameValue;
        
        lbl=nil;
        lbl = (UILabel *) [cell.contentView viewWithTag:CONTACT_ENTRY_EMAIL_TAG];
        NSString * emailValue = [contactDict objectForKey:@"emailValue"];
        lbl.text = emailValue;

    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 55.0f;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		NSArray * sortedKeys = [[self.m_addrBookEmailContacts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
		if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        	NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
			[self.m_addrBookEmailContacts removeObjectForKey:keyStr];
			BOOL selectedAll = YES;
			if ([self.m_addrBookEmailContacts count] > 0) {
				for (NSString * keyStr in [self.m_addrBookEmailContacts allKeys]) {
					NSMutableDictionary * contactDict = [self.m_addrBookEmailContacts objectForKey:keyStr];
					NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
					if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
						selectedAll = NO;
						break;
					}
				}
			} else {
				selectedAll = NO;
			}

			self.m_selectAllButton.selected = selectedAll;
		}
        [self.m_emailTableView reloadData];
	} else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray * sortedKeys = [[self.m_addrBookEmailContacts allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    
    if ((sortedKeys != nil) && ([sortedKeys count] > [indexPath row])) {
        NSString * keyStr = [sortedKeys objectAtIndex:[indexPath row]];
        NSMutableDictionary * contactDict = [self.m_addrBookEmailContacts objectForKey:keyStr];
        

        
        NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
        if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"1"] == NSOrderedSame)) {
            selectionValue = @"0";
        	[contactDict setObject:selectionValue forKey:@"selectionValue"];
			if (self.m_selectAllButton.selected == YES) {
				self.m_selectAllButton.selected = NO;
			}
		} else {
            selectionValue = @"1";
        	[contactDict setObject:selectionValue forKey:@"selectionValue"];
			BOOL selectedAll = YES;
			if ([self.m_addrBookEmailContacts count] > 0) {
				for (NSString * keyStr in [self.m_addrBookEmailContacts allKeys]) {
					NSMutableDictionary * contactDict = [self.m_addrBookEmailContacts objectForKey:keyStr];
					NSString * selectionValue = [contactDict objectForKey:@"selectionValue"];
					if ((selectionValue == nil) || ([selectionValue caseInsensitiveCompare:@"0"] == NSOrderedSame)) {
						selectedAll = NO;
						break;
					}
				}
			} else {
				selectedAll = NO;
			}

			self.m_selectAllButton.selected = selectedAll;
		}
        
        
        [tableView reloadData];
    }


}

-(IBAction) contactsSelectAllToggleAction:(id) sender {
	UIButton* button = (UIButton*)sender;
    button.selected = !button.selected;
	
	if ([self.m_addrBookEmailContacts count] > 0) {
		for (NSString * keyStr in [self.m_addrBookEmailContacts allKeys]) {
			NSMutableDictionary * contactDict = [self.m_addrBookEmailContacts objectForKey:keyStr];

			if (button.selected == YES)
				[contactDict setObject:@"1" forKey:@"selectionValue"];
			else
				[contactDict setObject:@"0" forKey:@"selectionValue"];
		}
	} 

	[self.m_emailTableView reloadData];
}


@end
