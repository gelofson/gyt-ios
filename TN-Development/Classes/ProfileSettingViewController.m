//
//  ProfileSettingViewController.m
//  QNavigator
//
//  Created by softprodigy on 31/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProfileSettingViewController.h"
#import"QNavigatorAppDelegate.h"


@implementation ProfileSettingViewController
@synthesize m_passwordLabel;

@synthesize m_mailLabel;

@synthesize m_firstNameLabel;

@synthesize m_lastNameLabel;

@synthesize m_MrButton;

@synthesize m_MsButton;

@synthesize m_ageGroupLabel;

QNavigatorAppDelegate *l_appDelegate;

#pragma mark -
#pragma mark custom

-(IBAction) m_goToBackView{
	[self.navigationController popViewControllerAnimated:YES];
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
#pragma mark -----------
-(void) viewDidLoad{
	//lastName=[[NSString alloc] init];
    
   
    
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	m_mailLabel.text= [prefs objectForKey:@"userName"];
	m_passwordLabel.text=[prefs objectForKey:@"password"];
   // m_ageGroupLabel.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"ageGroup"]];
	m_firstNameLabel.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"firstName"] ];

	m_lastNameLabel.text=[NSString stringWithFormat:@"%@",[l_appDelegate.userInfoDic valueForKey:@"lastName"]];
	if ([[l_appDelegate.userInfoDic valueForKey:@"gender"]isEqualToString:@"M"]) {
		[m_MrButton setSelected:YES];
	}
else if([[l_appDelegate.userInfoDic valueForKey:@"gender"]isEqualToString:@"F" ])
{
	[m_MsButton setSelected:YES];
}
}
-(IBAction)resetButtonClick:(id)sender
{


}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated{
	
	
}

// GDL: Changed everything below.

#pragma mark - memory management

- (void)viewDidUnload {
    [super viewDidUnload];
    
    // Release retained IBOutlets.
    self.m_mailLabel = nil;
    self.m_passwordLabel = nil;
    self.m_firstNameLabel = nil;
    self.m_lastNameLabel = nil;
    self.m_MrButton = nil;
    self.m_MsButton = nil;
    self.m_ageGroupLabel = nil;
}


- (void)dealloc {
	[m_mailLabel release];
    [m_passwordLabel release];
	[m_firstNameLabel release];	
	[m_lastNameLabel release];
	[m_MrButton release];
	[m_MsButton release];
	[m_ageGroupLabel release];
	
	[super dealloc];
}

@end
