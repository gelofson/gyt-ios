//
//  ShopbeeAPIs.h
//  QNavigator
//
//  Created by softprodigy on 20/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSON.h"

@interface ShopbeeAPIs : NSObject 
{
	NSURLConnection *l_theConnection;
	NSMutableData *m_mutResponseData;
	int m_intResponseCode;
}


-(void)getWishListDetails:(SEL)tempSelector tempTarget:(id)tempTarget wishlistid:(NSInteger)wishlistid;

-(void)sendBuyItOrNotRequest:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary;

-(void)getRegisteredUsers:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID listids:(NSString *)uids;

-(void)addToWishList:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary;

-(void)sendIBoughtItRequest:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary;

// GDL: We implement this, so we should declare it.
-(void)getRegisteredFaceBookUsers:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID listids:(NSString *)uids;

-(void)getRegisteredTwitterUsers:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID listids:(NSString *)uids;

-(void) mapTwitterID:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID twitterid:(NSString *)TwitterUid;

-(void) mapFaceBookID:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID facebookid:(NSString *)facebookID;

-(void)sendRequestToAddFriend:(SEL)tempSelector tempTarget:(id)tempTarget  custId:(NSString *)custId  tmpUids:(NSString *)tmpUids;

-(void) getFriendsList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid loginId:(NSString*)loginId;

-(void) getPhonebookList:(SEL)tempSelector tempTarget:(id)tempTarget emailIds:(NSArray*)Mail_IDs ;

-(void) RateShopbeeApp:(SEL)tempSelector tempTarget:(id)tempTarget tmp_dictionary:(NSDictionary *)dictionary;

-(void) getUserInfo:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)custid loginId:(NSString*)loginId;

-(void) getNotificationsCount:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)custid;

-(void) updateNotificationTray:(SEL)tempSelector tempTarget:(id)tempTarget notificationid:(NSString *)notificationid;

-(void) updateUserInfo:(SEL)tempSelector tempTarget:(id)tempTarget tmp_dictionary:(NSDictionary *)updateDictionary;

-(void) getFollowerList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid loginid:(NSString*)loginid;

-(void) getFollowingList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid loginid:(NSString*)loginid;

-(void) addFollowing:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerid followers:(NSArray *)followers;

-(void) deletefriend:(SEL)tempSelector tempTarget:(id)tempTarget  custId:(NSString *)custId friendid:(NSString *)friendid;

-(void)deleteWishList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID wishlistid:(NSString *)wishlistid;

-(void)getItemsOnWishList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID type:(NSString *)type;

-(void)updateWishList:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary;

-(void)addWishListWithoutProduct:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary;

-(void)getUnreadRequestsForMine:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;

-(void)getUnreadRequestsForFriends:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;

-(void)getUnreadRequestsForFollowing:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;

-(void)getUnreadRequestsForPopular:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;

-(void)getUnreadRequestsForNearby:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid type:(NSString *)type number:(NSInteger)number pageno:(NSInteger)pageNo;

-(void)getDataForParticularRequest:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId;

-(void)addResponseForFittingRoomRequest:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*) userid msgresponse:(NSMutableDictionary*)msgresponse;

-(void)unfollowAFriend:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*) userid following:(NSString*)personFollowing;

-(void)answerFriendRequest:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*) userid friendid:(NSString*)friendid acceptString:(NSString*)acceptString;

-(void)getSearchResults:(SEL)tempSelector tempTarget:(id)tempTarget searchText:(NSString *)searchText pageNo:(NSString *)pageNo;

-(void)iboughtitforAparticularItem:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId;

-(void)announceAChangeInMind:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId;

-(void)TurnPingsOnOrOff:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString*)userid friendid:(NSString*)friendid pingStatus:(NSString*)pingStatus;

-(void)setPrivacySettings:(SEL)tempSelector tempTarget:(id)tempTarget settingDict:(NSMutableDictionary*)settingDict;

-(void)getPrivacySettings:(SEL)tempSelector tempTarget:(id)tempTarget custid:(NSString*)custid;

-(void)FlagAMessage:(SEL)tempSelector tempTarget:(id)tempTarget userid:(NSString *)userid messageId:(int)messageId flagType:(NSString*)flagType;

-(void)forgotPassword:(SEL)tempSelector tempTarget:(id)tempTarget email:(NSString *)email;

-(void)getMyPageRecentPhotos:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID loginid:(NSString *) loginid;

-(void)getMyPagePopularPhotos:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID num:(int)num loginid:(NSString *)loginid;

-(void)getItemsOnFriendWishList:(SEL)tempSelector tempTarget:(id)tempTarget customerid:(NSString *)customerID friendId:(NSString *)friendId;

-(void)addToWishListWithFittingRoomData:(SEL)tempSelector tempTarget:(id)tempTarget tmpDict:(NSDictionary*)m_dictionary;

-(void)getNotificationsWithCallbackTarget:(id)tar withCallbackSelector:(SEL)selector forUserId:(NSString*)userId pageNumber:(int)pageNumber numberOfRecords:(int)numberOfRecords;

-(void)getNearStories:(SEL)tempSelector tempTarget:(id)tempTarget Lattitude:(double) lattitude Longitude:(double) longitude Count:(int) count;


@end
