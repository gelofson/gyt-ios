//
//  UIView+Badge.h
//  QNavigator
//
//  Created by Mac on 9/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIView (badge)//**

-(void)setBadge:(NSString *)badgeString;
-(void) removeBadge;

@end
