//  Copyright __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ATConnection : NSURLConnection {

	@private
	
	NSString	*tag;
}

@property (nonatomic, retain) NSString	*tag;
- (id)initWithRequest:(NSURLRequest *)request delegate:(id)delegate startImmediately:(BOOL)startImmediately withTag:(NSString*)tag_;
@end
