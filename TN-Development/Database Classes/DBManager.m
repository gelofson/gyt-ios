//
//  DBManager.m
//  
//
//  Created by Soft Prodigy
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DBManager.h"

#define DATABASE_FILENAME @"fashiongram_dbQNavigator.sqlite"
//#define DATABASE_FILENAME @"Registrationdb.sqlite"

@implementation DBManager

@synthesize arrSearchHistory;
@synthesize arrFavorites;
@synthesize arrInsee;
//@synthesize m_objCategory;

QNavigatorAppDelegate *l_appDelegate;

#pragma mark Database Initialization code

- (void)createEditableCopyOfDatabaseIfNeeded 
{
	//m_objCategory=[[CategoryModelClass alloc]init];
	
	NSLog(@"Creating editable copy of database");
	// First, test for existence.
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DATABASE_FILENAME];
		
	success = [fileManager fileExistsAtPath:writableDBPath];
	
	NSLog(@"%@",writableDBPath);
	
	if (success) return;
	// The writable database does not exist, so copy the default to the appropriate location.
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_FILENAME];
	
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	if (!success) 
	{
		NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
	}
}

-(BOOL) InitializeDB
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *DBpath = [documentsDirectory stringByAppendingPathComponent:DATABASE_FILENAME];
	
	l_appDelegate=(QNavigatorAppDelegate *)[[UIApplication sharedApplication]delegate];
	
	arrSearchHistory=[[NSMutableArray alloc]init];
	arrFavorites=[[NSMutableArray alloc]init];
	
	if(sqlite3_open([DBpath UTF8String],&database)!=SQLITE_OK)
	{
		return NO;
	}
	return YES;
}


-(BOOL) CloseDatabase 
{
	sqlite3_close(database);
	return YES;
}


#pragma mark Methods manipulate the categories data

//inserts a record for search history in tblSearchHistory
#if 0
-(NSInteger) insertCategoryRecord:(int)categoryNumber
{
	sqlite3_stmt * catStmt;
	
	char *catSql="insert into tblCategoriesData (categoryNumber, id, title, discount, address, overview, latitude, longitude, distance, mallMapUrl, imageUrl, imageUrl2, imageUrl3, storeHours, productName, brandName, phoneNo, dealUrl, storeWebsite, startRedemptionAt, endRedemptionAt) Values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	//
//	if (sqlite3_prepare_v2(database,catSql, -1, &catStmt, NULL)!=SQLITE_OK)
//	{
//		NSAssert1(0,@"Error while create inserting new record '%s'",sqlite3_errmsg(database));
//		return -1; //Denote Failure
//	}
	
	for(int i=0;i<l_appDelegate.m_arrCategorySalesData.count;i++)
	{
		if (sqlite3_prepare_v2(database,catSql, -1, &catStmt, NULL)!=SQLITE_OK)
		{
			NSAssert1(0,@"Error while create inserting new record '%s'",sqlite3_errmsg(database));
			return -1; //Denote Failure
		}
		
		m_objCategory=[l_appDelegate.m_arrCategorySalesData objectAtIndex:i];
			
		sqlite3_bind_int(catStmt,1, categoryNumber);
		sqlite3_bind_text(catStmt, 2, m_objCategory.m_strId.length==0?"":[m_objCategory.m_strId UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 3, m_objCategory.m_strTitle.length==0?"":[m_objCategory.m_strTitle UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 4, m_objCategory.m_strDiscount.length==0?"":[m_objCategory.m_strDiscount UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 5, m_objCategory.m_strAddress.length==0?"":[m_objCategory.m_strAddress UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 6, m_objCategory.m_strOverview.length==0?"":[m_objCategory.m_strOverview UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 7, m_objCategory.m_strLatitude.length==0?"":[m_objCategory.m_strLatitude UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 8, m_objCategory.m_strLongitude.length==0?"":[m_objCategory.m_strLongitude UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 9, m_objCategory.m_strDistance.length==0?"":[m_objCategory.m_strDistance UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 10, m_objCategory.m_strMallImageUrl.length==0?"":[m_objCategory.m_strMallImageUrl UTF8String] ,-1, SQLITE_TRANSIENT);
		
		sqlite3_bind_text(catStmt, 11, m_objCategory.m_strImageUrl.length==0?"":[m_objCategory.m_strImageUrl UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 12, m_objCategory.m_strImageUrl2.length==0?"":[m_objCategory.m_strImageUrl2 UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 13, m_objCategory.m_strImageUrl3.length==0?"":[m_objCategory.m_strImageUrl3 UTF8String], -1, SQLITE_TRANSIENT);
		
		sqlite3_bind_text(catStmt, 14, m_objCategory.m_strStoreHours.length==0?"":[m_objCategory.m_strStoreHours UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 15, m_objCategory.m_strProductName.length==0?"":[m_objCategory.m_strProductName UTF8String], -1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 16, m_objCategory.m_strBrandName.length==0?"":[m_objCategory.m_strBrandName UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 17, m_objCategory.m_strPhoneNo.length==0?"":[m_objCategory.m_strPhoneNo UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 18, m_objCategory.m_strDealUrl.length==0?"":[m_objCategory.m_strDealUrl UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 19, m_objCategory.m_strStoreWebsite.length==0?"":[m_objCategory.m_strStoreWebsite UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 20, m_objCategory.m_strStartRedemptionAt.length==0?"":[m_objCategory.m_strStartRedemptionAt UTF8String] ,-1, SQLITE_TRANSIENT);
		sqlite3_bind_text(catStmt, 21, m_objCategory.m_strEndRedemptionAt.length==0?"":[m_objCategory.m_strEndRedemptionAt UTF8String] ,-1, SQLITE_TRANSIENT);
		
			
		if (sqlite3_step(catStmt)==SQLITE_DONE)
		{
			NSLog(@"success");
		}
		
		
	}
	return 0;
	
}

//method to read the favorites data from database and return it in an array
-(NSMutableArray *)GetCategoriesData:(int)temp_catIndex  
{
	sqlite3_stmt *getCatStmt;
	
	NSMutableArray *temp_arrCatData=[[NSMutableArray alloc]init];
	
	const char *sql = "select categoryNumber, id, title, discount, address, overview, latitude, longitude, distance, mallMapUrl, imageUrl, imageUrl2, imageUrl3, storeHours, productName, brandName, phoneNo, dealUrl, storeWebsite, startRedemptionAt, endRedemptionAt from tblCategoriesData where categoryNumber=?";
	
	if(sqlite3_prepare_v2(database, sql, -1, &getCatStmt, NULL)!=SQLITE_OK)
	{
		NSAssert1(0,@"Error while Creating getSearchHistory Statement.'%s'",sqlite3_errmsg(database));
	}
	sqlite3_bind_int(getCatStmt, 1, temp_catIndex);
	
	CategoryModelClass *tempCatObj;
	while(sqlite3_step(getCatStmt)==SQLITE_ROW)
	{
		//@try
//		{
			tempCatObj=[[CategoryModelClass alloc]init];
			
			//tempCatObj.m_strId =[[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(getCatStmt, 0)];
			tempCatObj.m_strId =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt, 1)];
			tempCatObj.m_strTitle =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt, 2)];
			tempCatObj.m_strDiscount =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt, 3)];
			tempCatObj.m_strAddress =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,4)];
			tempCatObj.m_strOverview =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,5)];
			tempCatObj.m_strLatitude =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,6)];
			tempCatObj.m_strLongitude= [NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,7)];
			tempCatObj.m_strDistance =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,8)];
			tempCatObj.m_strMallImageUrl =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt, 9)];
			tempCatObj.m_strImageUrl =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,10)];
			
			tempCatObj.m_strImageUrl2=[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,11)];
		
			tempCatObj.m_strImageUrl3=[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,12)];
						
			tempCatObj.m_strStoreHours =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,13)];
			tempCatObj.m_strProductName =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt, 14)];
			tempCatObj.m_strBrandName =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,15)];
			tempCatObj.m_strPhoneNo =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,16)];
			tempCatObj.m_strDealUrl =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,17)];
			tempCatObj.m_strStoreWebsite =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,18)];
			tempCatObj.m_strStartRedemptionAt =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,19)];
			tempCatObj.m_strEndRedemptionAt =[NSString stringWithUTF8String:(char *)sqlite3_column_text(getCatStmt,20)];
				
		
			[temp_arrCatData addObject:tempCatObj];
			[tempCatObj release];
		}
		//@catch (NSException *e) 
//		{
//			NSLog(@"exception");
//		}
	//}
	return [temp_arrCatData autorelease];
}

//method to read the favorites data from database and return it in an array
-(BOOL)checkForAlreadyInsertedData:(int)temp_catIndex  
{
	sqlite3_stmt *getCatStmt;
	
	const char *sql = "select categoryNumber from tblCategoriesData where categoryNumber=?";
	
	if(sqlite3_prepare_v2(database, sql, -1, &getCatStmt, NULL)!=SQLITE_OK)
	{
		NSAssert1(0,@"Error while Creating getSearchHistory Statement.'%s'",sqlite3_errmsg(database));
	}
	sqlite3_bind_int(getCatStmt, 1, temp_catIndex);
	
	
	if(sqlite3_step(getCatStmt)==SQLITE_ROW)
	{
		return YES;
	}
	else {
		return NO;
	}
}



//deletes a record from database
-(void)deleteCategoryData:(int)temp_catIndex
{
	sqlite3_stmt *deleteCatStmt;
	const char *deleteSql = "delete from tblCategoriesData where categoryNumber=?";
	
	if(sqlite3_prepare_v2(database, deleteSql, -1, &deleteCatStmt, NULL)!=SQLITE_OK) 
	{
		NSAssert1(0,@"Error while  deleteCatStmt Statement.'%s'",sqlite3_errmsg(database));
	}
	
	sqlite3_bind_int(deleteCatStmt, 1,temp_catIndex);
	//sqlite3_bind_text(deleteFavoritesStmt, 1, (char *)[recordId UTF8String], -1, SQLITE_TRANSIENT);

	if (SQLITE_DONE != sqlite3_step(deleteCatStmt))
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
	
	sqlite3_reset(deleteCatStmt);
	
}

-(void)flushDB
{
	sqlite3_stmt *deleteCatStmt;
	const char *deleteSql = "delete from tblCategoriesData";
	
	if(sqlite3_prepare_v2(database, deleteSql, -1, &deleteCatStmt, NULL)!=SQLITE_OK) 
	{
		NSAssert1(0,@"Error while  deleteCatStmt Statement.'%s'",sqlite3_errmsg(database));
	}
	
	//sqlite3_bind_int(deleteCatStmt, 1,temp_catIndex);
	//sqlite3_bind_text(deleteFavoritesStmt, 1, (char *)[recordId UTF8String], -1, SQLITE_TRANSIENT);
	
	if (SQLITE_DONE != sqlite3_step(deleteCatStmt))
		NSAssert1(0, @"Error while deleting. '%s'", sqlite3_errmsg(database));
	
	sqlite3_reset(deleteCatStmt);
}
#endif
#pragma mark RegistrationClassDatabaseMethods


-(void)insertRegistrationData:(NSDictionary *)m_Databasedict
{
	sqlite3_stmt *insertStmt;
	const char *catSql="INSERT INTO registrationfields(zipcode,firstname,lastname,Mail,Password,Phone) values(?,?,?,?,?,?)";
	
	NSString *firstname= [NSString stringWithFormat:@"%@",[m_Databasedict objectForKey:@"FirstName"]];
	NSString *lastname=[NSString stringWithFormat:@"%@",[m_Databasedict objectForKey:@"LastName"]];
	NSString *mail=[NSString stringWithFormat:@"%@",[m_Databasedict objectForKey:@"EMail"]];
	NSString *password=[NSString stringWithFormat:@"%@",[m_Databasedict objectForKey:@"Password"]];
	NSString *phone=[NSString stringWithFormat:@"%@",[m_Databasedict objectForKey:@"Phone"]];
	NSString *zipcode=[NSString stringWithFormat:@"%@",[m_Databasedict objectForKey:@"Zipcode"]];
	
	if(sqlite3_prepare_v2(database, catSql, -1, &insertStmt, NULL)!=SQLITE_OK) 
	{
		NSAssert1(0,@"Error while  insertStmt Statement.'%s'",sqlite3_errmsg(database));
	}
	
	sqlite3_bind_text(insertStmt, 1,(char *)[zipcode UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 2,(char *)[firstname UTF8String] ,-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 3,(char *)[lastname UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 4,(char *)[mail UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 5,(char *)[password UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 6,(char *)[phone UTF8String],-1,SQLITE_TRANSIENT);
	
	
	

	if (sqlite3_step(insertStmt)==SQLITE_DONE)
	{
		NSLog(@"Data saved for message no.");
	}
	else
	{
		NSLog(@"Data NOT saved for message no.");
	}
	
}
-(NSDictionary *) getRegistrationData:(NSDictionary *) m_getdatabaseData
{
	sqlite3_stmt *getData;
	
	//m_getdatabaseData=[[NSDictionary alloc] init];
//
	
	
	const char *sql = "select Mail,Password,Phone from registrationfields ";
	
	if(sqlite3_prepare_v2(database, sql, -1, &getData, NULL)!=SQLITE_OK)
	{
		NSAssert1(0,@"Error while Creating getSearchHistory Statement.'%s'",sqlite3_errmsg(database));
	}
	while (sqlite3_step(getData)==SQLITE_ROW) {
	NSString *tmp_mail=[[NSString alloc]initWithUTF8String:(char*) sqlite3_column_text(getData,0)];
	NSString *tmp_password=[[NSString alloc]initWithUTF8String:(char*) sqlite3_column_text(getData,1)];
		NSString *tmp_phone=[[NSString alloc]initWithUTF8String:(char*) sqlite3_column_text(getData,2)];
	
	//return [m_getDatabaseData autorelease];

	m_getdatabaseData=[NSDictionary dictionaryWithObjectsAndKeys:tmp_mail,@"EMail",tmp_phone,@"Phone",tmp_password,@"PassWord",nil];
		[tmp_mail release];
		tmp_mail=nil;
		[tmp_phone release];
		tmp_phone=nil;
		[tmp_password release];
		tmp_password=nil;

	}
	return m_getdatabaseData;
	//if (m_getdatabaseData) {
//		
//	
//	[m_getdatabaseData release];
//	m_getdatabaseData=nil;
//	}
}
#pragma mark save profile info data  

-(void) insertProfileData:(NSDictionary *)m_ProfileInfoDict{
	sqlite3_stmt *insertStmt;
	const char *catSql="INSERT INTO ProfileInfoSettings(AboutMe,Interest,FavouriteBrand,FavouriteQuotation) values(?,?,?,?)";
	
	NSString *aboutme= [NSString stringWithFormat:@"%@",[m_ProfileInfoDict objectForKey:@"AboutMe"]];
	NSString *interest=[NSString stringWithFormat:@"%@",[m_ProfileInfoDict objectForKey:@"Interest"]];
	NSString *fvbrand=[NSString stringWithFormat:@"%@",[m_ProfileInfoDict objectForKey:@"FavouriteBrand"]];
	NSString *fvquotation=[NSString stringWithFormat:@"%@",[m_ProfileInfoDict objectForKey:@"FavouriteQuotation"]];
	
	
	if(sqlite3_prepare_v2(database, catSql, -1, &insertStmt, NULL)!=SQLITE_OK) 
	{
		NSAssert1(0,@"Error while  insertStmt Statement.'%s'",sqlite3_errmsg(database)); 
	}
	
	sqlite3_bind_text(insertStmt, 1,(char *)[aboutme UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 2,(char *)[interest UTF8String] ,-1, SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 3,(char *)[fvbrand UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(insertStmt, 4,(char *)[fvquotation UTF8String],-1,SQLITE_TRANSIENT);
	
	
	
	
	
	if (sqlite3_step(insertStmt)==SQLITE_DONE)
	{
		NSLog(@"Data saved for message no.");
	}
else
	{
		NSLog(@"Data NOT saved for message no.");
	}
	
}



-(NSDictionary *) getProfileInfo
	{
		sqlite3_stmt *getData;
		
		NSDictionary *m_getdatabaseData;
		//
		
		
		const char *sql = "select AboutMe,Interest,FavouriteBrand,FavouriteQuotation from ProfileInfoSettings ";
		
		if(sqlite3_prepare_v2(database, sql, -1, &getData, NULL)!=SQLITE_OK)
		{
			NSAssert1(0,@"Error while Creating getSearchHistory Statement.'%s'",sqlite3_errmsg(database));
		}
		while (sqlite3_step(getData)==SQLITE_ROW) {
			NSString *aboutme=[[NSString alloc]initWithUTF8String:(char*) sqlite3_column_text(getData,0)];
			NSString *interest=[[NSString alloc]initWithUTF8String:(char*) sqlite3_column_text(getData,1)];
			NSString *fvbrand=[[NSString alloc]initWithUTF8String:(char*) sqlite3_column_text(getData,2)];
			NSString *fvquotation=[[NSString alloc]initWithUTF8String:(char*)sqlite3_column_text(getData,3)];
			
			//return [m_getDatabaseData autorelease];
		
			m_getdatabaseData=[NSDictionary dictionaryWithObjectsAndKeys:aboutme,@"AboutMe",interest,@"Interest",fvbrand,@"FavouriteBrand",fvquotation,@"FavouriteQuotation",nil];
			
		[aboutme release];
		aboutme=nil;
		
		[interest release];
		interest=nil;
		
		[fvbrand release];
		fvbrand=nil;
		
		[fvquotation release];
		fvquotation=nil;
		return m_getdatabaseData;	
		}
		return nil;
	}


#pragma mark Singleton Instance function

+ (id) SharedInstance {
	static id sharedManager = nil;
	
    if (sharedManager == nil) {
        sharedManager = [[self alloc] init];
    }
	
    return sharedManager;
}

#pragma mark - memory management

- (void) dealloc {
    [arrSearchHistory release];
    [arrFavorites release];
    //[m_objCategory release];
    [arrInsee release];
    
    [super dealloc];
}

@end
